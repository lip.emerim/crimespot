#!/bin/sh
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="$@"

retries=10

until PGPASSWORD=$POSTGRES_PASSWORD pg_isready -h "$host" -U crimespot > /dev/null 2>&1; do
  retries=$((retries-1))

  if [ $retries -eq 0 ]
  then
    echo "max attempts reached"
    exit 1;
  fi

  echo "Waiting for postgres server, $retries remaining attempts..."
  sleep 10
done




echo "Postgres is up - executing command"
exec $cmd
