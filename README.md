# CrimeSpot

Collaborative crime occurrence register

[![pipeline status](https://gitlab.com/lip.emerim/crimespot/badges/master/pipeline.svg)](https://gitlab.com/lip.emerim/crimespot/-/commits/master)
[![coverage report](https://gitlab.com/lip.emerim/crimespot/badges/master/coverage.svg)](https://gitlab.com/lip.emerim/crimespot/-/commits/master)


# starting the application

Run `docker-compose up`

# Checking dependencies

Display outdated packages with the command

    mvn versions:display-dependency-upgrades

To reflect changes you must tell your IDE to build the project.

# FluentLenium E2E tests

Each config property can be overridden by system property.

# Running web browser tests on Selenium Grid

`mvn clean test -DbrowserName=ie -DuseHub=true -DisHeadless=true -DgridUrl=$BROWSERSTACK_GRID_URL  -DpageUrl=http://$IP`

`mvn clean test -DbrowserName=edge -DuseHub=true -DisHeadless=true -DgridUrl=$BROWSERSTACK_GRID_URL  -DpageUrl=http://$IP`

`mvn clean test -DbrowserName=firefox -DuseHub=true -DisHeadless=true -DgridUrl=$BROWSERSTACK_GRID_URL  -DpageUrl=http://$IP`

`mvn clean test -DbrowserName=chrome -DuseHub=true -DisHeadless=true -DgridUrl=$BROWSERSTACK_GRID_URL  -DpageUrl=http://$IP`

`mvn clean test -DbrowserName=safari -DuseHub=true -DisHeadless=true -DgridUrl=$BROWSERSTACK_GRID_URL  -DpageUrl=http://$IP`

The option `browserName` indicates the browser we will run the tests into.   
The option `useHub` indicates if the tests will be run remote or not
The option `isHeadless` indicates if the tests will be run headless or not    
The option `gridUrl` is used when running remote tests to point to the selenium
grid instance.    
The option `pageUrl` points the browser to the URL where the application
to be tested is running.   

By default, the tests will run locally, on firefox and the application is
also expected to be running locally. To change that behavior, just
override the options above.

### Generate jar file

```bash
docker-compose run --rm app mvn clean package -DskipTests spring-boot:repackage -DskipTests
```