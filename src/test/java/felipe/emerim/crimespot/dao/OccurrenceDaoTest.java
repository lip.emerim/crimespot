package felipe.emerim.crimespot.dao;

import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.PaginatedEntity;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.OccurrenceSortEnum;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import felipe.emerim.crimespot.form.OccurrenceSearchForm;
import felipe.emerim.crimespot.repository.OccurrenceRepository;
import felipe.emerim.crimespot.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class OccurrenceDaoTest extends BaseDaoTest {

    @Autowired
    private OccurrenceDao occurrenceDao;
    @Autowired
    private OccurrenceRepository occurrenceRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFullSearch() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm, 8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testFullSearchPagination() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        PaginatedEntity<Occurrence> page1 = occurrenceDao.fullSearch(user, occurrenceSearchForm, 8);

        occurrenceSearchForm.setPage(2);

        PaginatedEntity<Occurrence> page2 = occurrenceDao.fullSearch(user, occurrenceSearchForm, 8);

        List<Long> idsPage1 = page1.getData().stream().map(Occurrence::getId).collect(Collectors.toList());
        List<Long> idsPage2 = page2.getData().stream().map(Occurrence::getId).collect(Collectors.toList());

        assertThat(idsPage2.stream().noneMatch(idsPage1::contains)).isTrue();

        Occurrence occurrence = occurrenceRepository.findById(1016L).get();
        assertThat(page2.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

    }

    @Test
    public void testFullSearchNoSortSpecified() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setOccurrenceSortEnum(null);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm, 8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testFullSearchHugePageLength() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                80000000);

        assertThat(occurrences.getPageLength()).isEqualTo(10);
        assertThat(occurrences.getData()).hasSize(10);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testFullSearchNearest() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setOccurrenceSortEnum(OccurrenceSortEnum.NEAREST);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1057L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testFullSearchLeastRecent() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setOccurrenceSortEnum(OccurrenceSortEnum.LEAST_RECENT);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1004L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testFullSearchMostReported() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setOccurrenceSortEnum(OccurrenceSortEnum.MOST_REPORTED);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1012L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testSearchWithSearchTerm() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setSearchTerm("roubo");
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(11);

        Occurrence occurrence = occurrenceRepository.findById(1065L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testSearchWithSearchTermSizeLessThanThree() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        //Search term should be ignored
        occurrenceSearchForm.setSearchTerm("a");
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(62);

        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testSearchWithStatus() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setOccurrenceStatusEnum(OccurrenceStatusEnum.REJECTED);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(1);
        assertThat(occurrences.getTotalResults()).isEqualTo(1);

        Occurrence occurrence = occurrenceRepository.findById(1003L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();
    }

    @Test
    public void testSearchWithTypes() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        ArrayList<OccurrenceTypeEnum> types = new ArrayList<>();
        types.add(OccurrenceTypeEnum.DRUGS_VIOLATION);
        types.add(OccurrenceTypeEnum.HOMICIDE);

        occurrenceSearchForm.setOccurrenceTypeEnums(types);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(10);

        Occurrence drugsOccurrence = occurrenceRepository.findById(1014L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId()
                .equals(drugsOccurrence.getId()))).isTrue();

        Occurrence homicideOccurrence = occurrenceRepository.findById(1015L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId()
                .equals(homicideOccurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceType().getDescription().equals(OccurrenceTypeEnum.HOMICIDE) ||
                                e.getOccurrenceType().getDescription().equals(OccurrenceTypeEnum.DRUGS_VIOLATION)
                )
        ).isTrue();
    }

    @Test
    public void testSearchGetOnlyRelatedToMe() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1000L).get();

        occurrenceSearchForm.setOnlyRelatedToMe(true);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(8);
        assertThat(occurrences.getTotalResults()).isEqualTo(39);

        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getUser().getId().equals(1000L) || e.getModerator().getId().equals(1000L)
                )
        ).isTrue();
    }

    @Test
    public void testSearchForRoleUser() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1002L).get();

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(4);
        assertThat(occurrences.getTotalResults()).isEqualTo(4);

        Occurrence occurrence = occurrenceRepository.findById(1065L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getUser().getId().equals(1002L)
                )
        ).isTrue();
    }

    /**
     * For RoleUser every parameter should be ignored, except the search term which we test
     * in another test case.
     */
    @Test
    public void testSearchForRoleUserWithParameters() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1022L).get();

        occurrenceSearchForm.setOnlyRelatedToMe(true);
        // This filter is ignored because the user can get his own occurrences only
        occurrenceSearchForm.setOccurrenceSortEnum(OccurrenceSortEnum.NEAREST);
        occurrenceSearchForm.setOccurrenceStatusEnum(OccurrenceStatusEnum.APPROVED);
        ArrayList<OccurrenceTypeEnum> types = new ArrayList<>();
        types.add(OccurrenceTypeEnum.ANIMAL_ABUSE);


        occurrenceSearchForm.setOccurrenceTypeEnums(types);
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(2);
        assertThat(occurrences.getTotalResults()).isEqualTo(2);

        assertThat(occurrences.getData().get(0).getId().equals(1052L)).isTrue();
        assertThat(occurrences.getData().get(1).getId().equals(1063L)).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getUser().getId().equals(1022L)
                )
        ).isTrue();
    }

    @Test
    public void testSearchForRoleUserWithSearchTerm() {
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        User user = userRepository.findById(1002L).get();

        occurrenceSearchForm.setSearchTerm("Renner");
        PaginatedEntity<Occurrence> occurrences = occurrenceDao.fullSearch(user, occurrenceSearchForm,
                8);

        assertThat(occurrences.getPageLength()).isEqualTo(8);
        assertThat(occurrences.getData()).hasSize(1);
        assertThat(occurrences.getTotalResults()).isEqualTo(1);

        Occurrence occurrence = occurrenceRepository.findById(1010L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getUser().getId().equals(1002L)
                )
        ).isTrue();
    }

    @Test
    public void testMapFullSearch() {
        OccurrenceMapSearchForm form = this.getMapSearchForm();

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.mapFullSearch(form);

        assertThat(occurrences.getPageLength()).isEqualTo(200);
        assertThat(occurrences.getData()).hasSize(50);
        assertThat(occurrences.getTotalResults()).isEqualTo(50);

        Occurrence occurrence = occurrenceRepository.findById(1010L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceStatus().getDescription().equals(OccurrenceStatusEnum.APPROVED)
                )
        ).isTrue();
    }

    @Test
    public void testMapFullSearchWithSearchTerm() {
        OccurrenceMapSearchForm form = this.getMapSearchForm();
        form.setSearchTerm("roubo");

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.mapFullSearch(form);

        assertThat(occurrences.getPageLength()).isEqualTo(200);
        assertThat(occurrences.getData()).hasSize(11);
        assertThat(occurrences.getTotalResults()).isEqualTo(11);

        Occurrence occurrence = occurrenceRepository.findById(1010L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceStatus().getDescription().equals(OccurrenceStatusEnum.APPROVED)
                )
        ).isTrue();
    }

    @Test
    public void testMapFullSearchWithTypes() {
        OccurrenceMapSearchForm form = this.getMapSearchForm();

        ArrayList<OccurrenceTypeEnum> types = new ArrayList<>();
        types.add(OccurrenceTypeEnum.ROBBERY);
        types.add(OccurrenceTypeEnum.THEFT);

        form.setOccurrenceTypeEnums(types);

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.mapFullSearch(form);

        assertThat(occurrences.getPageLength()).isEqualTo(200);
        assertThat(occurrences.getData()).hasSize(10);
        assertThat(occurrences.getTotalResults()).isEqualTo(10);

        Occurrence occurrence = occurrenceRepository.findById(1010L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceStatus().getDescription().equals(OccurrenceStatusEnum.APPROVED)
                )
        ).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceType().getDescription().equals(OccurrenceTypeEnum.ROBBERY) ||
                                e.getOccurrenceType().getDescription().equals(OccurrenceTypeEnum.THEFT)
                )
        ).isTrue();
    }

    @Test
    public void testMapFullSearchWithInterval() {
        OccurrenceMapSearchForm form = this.getMapSearchForm();
        LocalDate start = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now();
        form.setStartDate(start);
        form.setEndDate(end);

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.mapFullSearch(form);

        assertThat(occurrences.getPageLength()).isEqualTo(200);
        assertThat(occurrences.getData()).hasSize(30);
        assertThat(occurrences.getTotalResults()).isEqualTo(30);

        Occurrence occurrence = occurrenceRepository.findById(1010L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceStatus().getDescription().equals(OccurrenceStatusEnum.APPROVED)
                )
        ).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> {
                            LocalDate date = e.getOccurrenceDate();
                            boolean afterStart = date.isEqual(start) || date.isAfter(start);
                            boolean beforeEnd = date.isEqual(end) || date.isBefore(end);

                            return afterStart && beforeEnd;
                        }
                )
        ).isTrue();
    }

    @Test
    public void testMapFullSearchOnlyVerified() {
        OccurrenceMapSearchForm form = this.getMapSearchForm();

        form.setOnlyVerified(true);

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.mapFullSearch(form);

        assertThat(occurrences.getPageLength()).isEqualTo(200);
        assertThat(occurrences.getData()).hasSize(26);
        assertThat(occurrences.getTotalResults()).isEqualTo(26);

        Occurrence occurrence = occurrenceRepository.findById(1010L).get();
        assertThat(occurrences.getData().stream().anyMatch((e) -> e.getId().equals(occurrence.getId()))).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        (e) -> e.getOccurrenceStatus().getDescription().equals(OccurrenceStatusEnum.APPROVED)
                )
        ).isTrue();

        assertThat(
                occurrences.getData().stream().allMatch(
                        Occurrence::isVerified
                )
        ).isTrue();
    }

    @Test
    public void testOccurrencesFarAwayAreNotLoaded() {
        OccurrenceMapSearchForm form = this.getMapSearchForm();

        // We do not have any occurrences near Madagascar, so none should be loaded
        form.setLatitude(-18.7792678);
        form.setLongitude(46.8344597);

        PaginatedEntity<Occurrence> occurrences = occurrenceDao.mapFullSearch(form);

        assertThat(occurrences.getPageLength()).isEqualTo(200);
        assertThat(occurrences.getData()).hasSize(0);
        assertThat(occurrences.getTotalResults()).isEqualTo(0);
    }

    @Test
    public void testCountByStatus() {
        Long countRejected = occurrenceDao.countByStatus(OccurrenceStatusEnum.REJECTED);
        assertThat(countRejected).isEqualTo(1);

        Long countApproved = occurrenceDao.countByStatus(OccurrenceStatusEnum.APPROVED);
        assertThat(countApproved).isEqualTo(57);

    }

    @Test
    public void countInAnalysisByUser() {
        User admin = userRepository.findById(1000L).get();
        User moderator = userRepository.findById(1002L).get();

        Long countAdmin = occurrenceDao.countInAnalysisByUser(admin);
        Long countModerator = occurrenceDao.countInAnalysisByUser(moderator);

        assertThat(countAdmin).isEqualTo(1);
        assertThat(countModerator).isEqualTo(0);
    }

    protected OccurrenceMapSearchForm getMapSearchForm() {
        OccurrenceMapSearchForm form = new OccurrenceMapSearchForm();
        form.setLatitude(-29.90934829911911);
        form.setLongitude(-51.14336807889483);
        form.setSearchRadius(3.1111870380138256);

        return form;
    }
}
