package felipe.emerim.crimespot.dao;

import felipe.emerim.crimespot.domain.Report;
import felipe.emerim.crimespot.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ReportDaoTest extends BaseDaoTest {
    @Autowired
    UserService userService;

    @Autowired
    ReportDao reportDao;

    @Test
    public void testGetActiveReportsByOccurrenceId() {
        List<Report> reports = reportDao.getActiveReportsByOccurrenceId(1012L);

        assertThat(
                reports.stream().allMatch((r) -> r.getOccurrence().getId().equals(1012L))
        ).isTrue();
        assertThat(reports).hasSize(3);
    }

    @Test
    public void testCountReportedOccurrences() {
        Long counter = reportDao.countReportedOccurrences();

        assertThat(counter).isEqualTo(3);
    }
}
