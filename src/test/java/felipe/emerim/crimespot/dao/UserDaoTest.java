package felipe.emerim.crimespot.dao;

import felipe.emerim.crimespot.domain.PaginatedEntity;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.form.UserSearchForm;
import felipe.emerim.crimespot.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class UserDaoTest extends BaseDaoTest {
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindNearestBySearchTermAndRole() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(8);
        assertThat(users.getTotalResults()).isEqualTo(24);
        assertThat(users.getPageLength()).isEqualTo(8);
    }

    @Test
    public void testFindNearestBySearchTermAndRolePagination() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();

        PaginatedEntity<User> page1 = userDao.findNearestBySearchTermAndRole(user, form);

        form.setPage(2);
        PaginatedEntity<User> page2 = userDao.findNearestBySearchTermAndRole(user, form);

        List<Long> idsPage1 = page1.getData().stream().map(User::getId).collect(Collectors.toList());
        List<Long> idsPage2 = page2.getData().stream().map(User::getId).collect(Collectors.toList());

        assertThat(idsPage2.stream().noneMatch(idsPage1::contains)).isTrue();

        User page2User = userRepository.findById(1019L).get();
        assertThat(page2.getData().stream().anyMatch((e) -> e.getId().equals(page2User.getId()))).isTrue();
    }

    @Test
    public void testFindNearestBySearchTermAndRoleHugePageLength() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();
        form.setPageLength(1888888);

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(15);
        assertThat(users.getTotalResults()).isEqualTo(24);
        assertThat(users.getPageLength()).isEqualTo(15);
    }

    @Test
    public void testFindNearestBySearchTermAndRoleWithSearchTerm() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();
        form.setSearchTerm("alucard");

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(1);
        assertThat(users.getTotalResults()).isEqualTo(1);
        assertThat(users.getPageLength()).isEqualTo(8);

        User alucard = userRepository.findById(1018L).get();

        assertThat(users.getData().stream().allMatch((e) -> e.equals(alucard))).isTrue();
    }

    @Test
    public void testFindNearestBySearchTermAndRoleWithRole() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();
        form.setRole(RoleEnum.ROLE_ADMIN);

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(1);
        assertThat(users.getTotalResults()).isEqualTo(1);

        assertThat(users.getData().stream().allMatch((e) -> e.equals(user))).isTrue();
    }

    @Test
    public void testFindNearestBySearchTermAndRoleWithActiveFalse() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();
        form.setActive(false);

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(1);
        assertThat(users.getTotalResults()).isEqualTo(1);

        User greenArcher = userRepository.findById(1023L).get();

        assertThat(users.getData().stream().allMatch((e) -> e.equals(greenArcher))).isTrue();
    }

    @Test
    public void testFindNearestBySearchTermAndRoleWithActiveTrue() {
        User user = userRepository.findById(1000L).get();
        UserSearchForm form = new UserSearchForm();
        form.setActive(true);

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getTotalResults()).isEqualTo(23);
        assertThat(users.getData()).hasSize(8);
        assertThat(users.getData().stream().allMatch(User::isActive)).isTrue();
    }

    @Test
    public void testFindNearestBySearchTermAndRoleUserIsModerator() {
        User user = userRepository.findById(1001L).get();
        UserSearchForm form = new UserSearchForm();

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(8);
        assertThat(users.getTotalResults()).isEqualTo(24);
        assertThat(users.getPageLength()).isEqualTo(8);

    }

    @Test
    public void testFindNearestBySearchTermAndRoleUserIsModeratorWithSearchTerm() {
        User user = userRepository.findById(1001L).get();
        UserSearchForm form = new UserSearchForm();

        form.setSearchTerm("alucard");
        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(1);
        assertThat(users.getTotalResults()).isEqualTo(1);
        assertThat(users.getPageLength()).isEqualTo(8);

        User alucard = userRepository.findById(1018L).get();

        assertThat(users.getData().stream().allMatch((e) -> e.equals(alucard))).isTrue();
    }

    @Test
    public void testFindNearestBySearchTermAndRoleUserIsModeratorWithActiveFalse() {
        User user = userRepository.findById(1001L).get();
        UserSearchForm form = new UserSearchForm();
        form.setActive(false);

        PaginatedEntity<User> users = userDao.findNearestBySearchTermAndRole(user, form);

        assertThat(users.getData()).hasSize(1);
        assertThat(users.getTotalResults()).isEqualTo(1);

        User greenArcher = userRepository.findById(1023L).get();

        assertThat(users.getData().stream().allMatch((e) -> e.equals(greenArcher))).isTrue();
    }
}
