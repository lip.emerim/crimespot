package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.helper.UserHelper;
import felipe.emerim.crimespot.validation.group.CreateUserGroup;
import felipe.emerim.crimespot.validation.group.EditUserGroup;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class UserTest extends BaseDomainTest {
    @Test
    public void testValidUser() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isEmpty();
    }

    @Test
    public void testUserUsernameBlank() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setUsername("");
        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserUsernameNull() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setUsername(null);
        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserUsernameMinLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setUsername(RandomStringUtils.randomAlphabetic(4));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.username.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "30");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserUsernameMaxLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setUsername(RandomStringUtils.randomAlphabetic(31));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.username.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "30");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserUsernameWrongPattern() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setUsername("A$%#*&__)(");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("user.username.pattern"));
    }

    @Test
    public void testUserPasswordBlank() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setPassword("");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserPasswordNull() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setPassword(null);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserPasswordMinLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setPassword(RandomStringUtils.randomAlphabetic(4));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.password.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "500");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserPasswordMaxLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setPassword(RandomStringUtils.randomAlphabetic(501));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.password.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "500");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserNullRoles() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setRoles(null);

        violations = this.validator.validate(user);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserNameBlank() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setName("");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserNameNull() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setName(null);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserNameMinLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setName(RandomStringUtils.randomAlphabetic(4));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.name.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "50");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserNameMaxLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setName(RandomStringUtils.randomAlphabetic(51));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.name.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "50");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserEmailBlank() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setEmail("");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserEmailNull() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setEmail(null);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserEmailMaxLength() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setEmail(RandomStringUtils.randomAlphabetic(99) + "@gmail.com");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.email.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "100");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testUserEmailInvalid() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setEmail("not_really_an_email");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.email"));
    }

    @Test
    public void testUserDateOfBirthNull() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setDateOfBirth(null);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserDateOfBirthFuture() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setDateOfBirth(LocalDate.now().plusDays(1));

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.past"));
    }

    @Test
    public void testUserCpfBlank() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setCpf("");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserCpfNull() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setCpf(null);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testUserCpfInvalid() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setCpf("12345678901");

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.cpf"));
    }

    @Test
    public void testUserSearchRadiusMin() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setSearchRadius(0.9D);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("user.searchRadius.min"));
    }

    @Test
    public void testUserSearchRadiusMax() {
        User user = UserHelper.createValidUser();
        Set<ConstraintViolation<User>> violations;

        user.setSearchRadius(10.1D);

        violations = this.validator.validate(user, CreateUserGroup.class, EditUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("user.searchRadius.max"));
    }

    @Test
    public void testUserIsModeratorTrue() {
        User user = UserHelper.createValidUser();
        assertThat(user.isModerator()).isTrue();
    }

    @Test
    public void testUserIsModeratorFalse() {
        User user = UserHelper.createValidUser();
        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_MODERATOR));
        assertThat(user.isModerator()).isFalse();
    }

    @Test
    public void testGetMainRoleAdmin() {
        User user = UserHelper.createValidUser();
        RoleEnum mainRole = user.getMainRole();

        assertThat(mainRole).isEqualTo(RoleEnum.ROLE_ADMIN);
    }

    @Test
    public void testGetMainRoleModerator() {
        User user = UserHelper.createValidUser();
        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        RoleEnum mainRole = user.getMainRole();

        assertThat(mainRole).isEqualTo(RoleEnum.ROLE_MODERATOR);
    }

    @Test
    public void testGetMainRoleUser() {
        User user = UserHelper.createValidUser();
        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_MODERATOR));
        RoleEnum mainRole = user.getMainRole();

        assertThat(mainRole).isEqualTo(RoleEnum.ROLE_USER);
    }

    @Test
    public void testHasRoleTrue() {
        User user = UserHelper.createValidUser();
        assertThat(user.hasRole(RoleEnum.ROLE_ADMIN)).isTrue();
        assertThat(user.hasRole(RoleEnum.ROLE_MODERATOR)).isTrue();
        assertThat(user.hasRole(RoleEnum.ROLE_USER)).isTrue();
    }

    @Test
    public void testHasRoleFalse() {
        User user = UserHelper.createValidUser();

        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        assertThat(user.hasRole(RoleEnum.ROLE_ADMIN)).isFalse();
    }

    @Test
    public void testCanToggleUserIsAdmin() {
        User user = UserHelper.createValidUser();
        User toggleUser = UserHelper.createValidUser();

        toggleUser.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.canToggleUser(toggleUser)).isTrue();
    }

    @Test
    public void testCanToggleUserToggledUserIsAdmin() {
        User user = UserHelper.createValidUser();
        User toggleUser = UserHelper.createValidUser();

        toggleUser.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.canToggleUser(toggleUser)).isTrue();
    }

    @Test
    public void testCanToggleUserIsModeratorAndToggleUserIsUser() {
        User user = UserHelper.createValidUser();
        User toggleUser = UserHelper.createValidUser();

        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        toggleUser.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        toggleUser.removeRole(UserHelper.createRole(RoleEnum.ROLE_MODERATOR));

        assertThat(user.canToggleUser(toggleUser)).isTrue();
    }

    @Test
    public void testCanToggleUserIsModeratorAndToggleUserIsModerator() {
        User user = UserHelper.createValidUser();
        User toggleUser = UserHelper.createValidUser();

        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        toggleUser.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.canToggleUser(toggleUser)).isFalse();
    }

    @Test
    public void testCanToggleUserIsModeratorAndToggleUserIsAdmin() {
        User user = UserHelper.createValidUser();
        User toggleUser = UserHelper.createValidUser();

        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.canToggleUser(toggleUser)).isFalse();
    }

    @Test
    public void testCanToggleUserUserIsActiveUser() {
        User user = UserHelper.createValidUser();
        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.canToggleUser(user)).isTrue();
    }

    /**
     * The ADMIN cannot be toggled, even by himself
     */
    @Test
    public void testCanToggleUserUserIsActiveUserButIsAdmin() {
        User user = UserHelper.createValidUser();

        assertThat(user.canToggleUser(user)).isFalse();
    }

    @Test
    public void testAddRole() {
        User user = UserHelper.createValidUser();
        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.hasRole(RoleEnum.ROLE_ADMIN)).isFalse();
        user.addRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.hasRole(RoleEnum.ROLE_ADMIN)).isTrue();
    }

    @Test
    public void testRemoveRole() {
        User user = UserHelper.createValidUser();

        assertThat(user.hasRole(RoleEnum.ROLE_ADMIN)).isTrue();

        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));

        assertThat(user.hasRole(RoleEnum.ROLE_ADMIN)).isFalse();
    }
}
