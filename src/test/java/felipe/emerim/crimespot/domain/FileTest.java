package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.helper.FileHelper;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class FileTest extends BaseDomainTest {
    @Test
    public void testValidFile() throws IOException {
        File file = FileHelper.createValidPDFFile();

        Set<ConstraintViolation<File>> violations = this.validator.validate(file);
        assertThat(violations).isEmpty();
    }

    @Test
    public void testFileBlankContentType() throws IOException {
        File file = FileHelper.createValidPDFFile();

        file.setContentType("");

        Set<ConstraintViolation<File>> violations = this.validator.validate(file);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFileNullContentType() throws IOException {
        File file = FileHelper.createValidPDFFile();

        file.setContentType(null);

        Set<ConstraintViolation<File>> violations = this.validator.validate(file);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFileFileContentEmpty() throws IOException {
        File file = FileHelper.createValidPDFFile();

        file.setFileContent(new byte[]{});

        Set<ConstraintViolation<File>> violations = this.validator.validate(file);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFileFileContentNull() throws IOException {
        File file = FileHelper.createValidPDFFile();

        file.setFileContent(null);

        Set<ConstraintViolation<File>> violations = this.validator.validate(file);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testSetContentFromMultipartFile() throws IOException {
        File file = FileHelper.createValidPDFFile();
        File updateFile = FileHelper.createJPGFile();

        assertThat(file.getFileContent()).isNotEqualTo(updateFile.getFileContent());

        MultipartFile result = new MockMultipartFile("jpg_file",
                "jpg_file", updateFile.getContentType(), updateFile.getDecodedContent());

        file.setContentFromMultipartFile(result);
        assertThat(file.getFileContent()).isEqualTo(updateFile.getFileContent());
    }

    @Test
    public void testGetDecodedContent() throws IOException {
        File file = FileHelper.createValidPDFFile();
        Resource fileResource = FileHelper.loadFile(FileHelper.PDF_PATH);
        byte[] fileBytes = FileUtils.readFileToByteArray(fileResource.getFile());

        assertThat(file.getDecodedContent()).isEqualTo(fileBytes);
    }

    @Test
    public void testUpdate() throws IOException {
        File file = FileHelper.createValidPDFFile();
        File updateFile = FileHelper.createJPGFile();

        assertThat(file.getFileContent()).isNotEqualTo(updateFile.getDecodedContent());
        assertThat(file.getContentType()).isNotEqualTo(updateFile.getContentType());

        file.update(updateFile);

        assertThat(file.getFileContent()).isEqualTo(updateFile.getFileContent());
        assertThat(file.getContentType()).isEqualTo(updateFile.getContentType());
    }
}
