package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.helper.OccurrenceHelper;
import felipe.emerim.crimespot.helper.UserHelper;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class OccurrenceTest extends BaseDomainTest {

    @Test
    public void testValidOccurrence() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        Set<ConstraintViolation<Occurrence>> violations = this.validator.validate(occurrence);
        assertThat(violations).isEmpty();
    }

    @Test
    public void testOccurrenceInvalidTitleBlank() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setTitle("");
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testOccurrenceInvalidTitleNull() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setTitle(null);
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testOccurrenceInvalidTitleMinLength() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setTitle(RandomStringUtils.randomAlphabetic(4));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.title");
        message = message.replace("{min}", "10");
        message = message.replace("{max}", "100");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testOccurrenceInvalidTitleMaxLength() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setTitle(RandomStringUtils.randomAlphabetic(101));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.title");
        message = message.replace("{min}", "10");
        message = message.replace("{max}", "100");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testOccurrenceInvalidDescriptionBlank() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setDescription("");
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testOccurrenceInvalidDescriptionNull() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setDescription(null);
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testOccurrenceInvalidDescriptionMinLength() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setDescription(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("occurrence.description.length");
        message = message.replace("{min}", "15");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testOccurrenceInvalidDescriptionMaxLength() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setDescription(RandomStringUtils.randomAlphabetic(14));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("occurrence.description.length");
        message = message.replace("{min}", "15");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testOccurrenceInvalidModeratorCommentsMinLength() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setModeratorComments(RandomStringUtils.randomAlphabetic(14));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("occurrence.moderatorComments.length");
        message = message.replace("{min}", "15");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testOccurrenceInvalidModeratorCommentsMaxLength() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setModeratorComments(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("occurrence.moderatorComments.length");
        message = message.replace("{min}", "15");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testOccurrenceInvalidOccurrenceDateNull() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setOccurrenceDate(null);
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testOccurrenceInvalidOccurrenceDateFuture() throws IOException {
        Set<ConstraintViolation<Occurrence>> violations;
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setOccurrenceDate(LocalDate.now().plusDays(1));
        violations = this.validator.validate(occurrence);
        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.pastOrPresent"));
    }

    @Test
    public void testIsReviewableValid() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.WAITING_ANALYSIS));
        assertThat(occurrence.isReviewable()).isTrue();
    }

    @Test
    public void testIsReviewableWrongStatus() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.IN_ANALYSIS));
        assertThat(occurrence.isReviewable()).isFalse();
    }

    @Test
    public void testIsReviewableByUserValid() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.WAITING_ANALYSIS));
        assertThat(occurrence.isReviewableByUser(user)).isTrue();
    }

    @Test
    public void testIsReviewableByUserWrongStatus() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.IN_ANALYSIS));
        assertThat(occurrence.isReviewableByUser(user)).isFalse();
    }

    @Test
    public void testIsReviewableByUserNotModerator() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();

        user.getRoles().remove(UserHelper.createRole(RoleEnum.ROLE_MODERATOR));
        assertThat(occurrence.isReviewableByUser(user)).isFalse();
    }

    @Test
    public void testIsInReviewByUserValid() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        user.setId(1L);

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.IN_ANALYSIS));
        assertThat(occurrence.isInReviewByUser(user)).isTrue();
    }

    @Test
    public void testIsInReviewByUserWrongStatus() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        user.setId(1L);

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.WAITING_ANALYSIS));
        assertThat(occurrence.isInReviewByUser(user)).isFalse();
    }

    @Test
    public void testIsInReviewByUserWrongUser() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        occurrence.getUser().setId(1L);
        User wrongUser = UserHelper.createUser();
        wrongUser.setId(2L);

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.IN_ANALYSIS));
        assertThat(occurrence.isInReviewByUser(wrongUser)).isFalse();
    }

    @Test
    public void testIsInReviewByUserNullModerator() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        user.setId(1L);

        occurrence.setModerator(null);
        assertThat(occurrence.isInReviewByUser(user)).isFalse();
    }

    @Test
    public void testCanDeleteOccurrenceAdminAndOwner() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        user.setId(1L);

        assertThat(occurrence.canDeleteOccurrence(user)).isTrue();
    }

    @Test
    public void testCanDeleteOccurrenceAdminAndOwnerInactive() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        user.setId(1L);

        occurrence.setActive(false);
        assertThat(occurrence.canDeleteOccurrence(user)).isFalse();
    }

    @Test
    public void testCanDeleteOccurrenceNotAdminButOwner() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        user.setId(1L);

        user.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        assertThat(occurrence.canDeleteOccurrence(user)).isTrue();
    }

    @Test
    public void testCanDeleteOccurrenceNotOwnerButAdmin() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        User admin = UserHelper.createUser();
        admin.setId(2L);
        user.setId(1L);

        assertThat(occurrence.canDeleteOccurrence(admin)).isTrue();
    }

    @Test
    public void testCanDeleteOccurrenceNeitherOwnerNorAdmin() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User user = occurrence.getModerator();
        User admin = UserHelper.createUser();
        admin.setId(2L);
        user.setId(1L);

        admin.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        assertThat(occurrence.canDeleteOccurrence(admin)).isFalse();
    }

    @Test
    public void testCanRevokeApprovalAdminAndApproved() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.setId(2L);

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.APPROVED));
        assertThat(occurrence.canDeleteOccurrence(admin)).isTrue();
    }

    @Test
    public void testCanRevokeApprovalAdminButNotApproved() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.setId(2L);

        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.REJECTED));
        assertThat(occurrence.canRevokeApproval(admin)).isFalse();
    }

    @Test
    public void testCanRevokeApprovalApprovedButNotAdmin() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.setId(2L);

        admin.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        assertThat(occurrence.canDeleteOccurrence(admin)).isFalse();
    }

    @Test
    public void testHasPendingReportByUser() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.setId(2L);

        occurrence.setReportsCount(3);

        assertThat(occurrence.hasPendingReportByUser(admin)).isTrue();
    }

    @Test
    public void testHasPendingReportByUserNotAdmin() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.removeRole(UserHelper.createRole(RoleEnum.ROLE_ADMIN));
        admin.setId(2L);

        occurrence.setReportsCount(3);

        assertThat(occurrence.hasPendingReportByUser(admin)).isFalse();
    }

    @Test
    public void testHasPendingReportByUserNoReports() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.setId(2L);

        occurrence.setReportsCount(0);

        assertThat(occurrence.hasPendingReportByUser(admin)).isFalse();
    }

    @Test
    public void testHasPendingReportByUserNotApproved() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        User admin = UserHelper.createUser();
        admin.setId(2L);

        occurrence.setReportsCount(3);
        occurrence.setOccurrenceStatus(OccurrenceHelper.createOccurrenceStatus(OccurrenceStatusEnum.REJECTED_BY_ADMIN));

        assertThat(occurrence.hasPendingReportByUser(admin)).isFalse();
    }
}
