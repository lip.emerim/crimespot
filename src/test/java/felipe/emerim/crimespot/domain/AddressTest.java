package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.helper.AddressHelper;
import felipe.emerim.crimespot.validation.group.AddressGroup;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class AddressTest extends BaseDomainTest {

    @Test
    public void testAddressValid() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isEmpty();
    }

    @Test
    public void testAddressCountryBlank() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setCountry("");
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressCountryNull() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setCountry(null);
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressCountryMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setCountry(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.max");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressStateBlank() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setState("");
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressStateNull() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setState(null);
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressStateMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setState(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.max");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressCityBlank() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setCity("");
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressCityNull() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setCity(null);
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressCityMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setCity(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.max");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressStreetBlank() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setStreet("");
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressStreetNull() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setStreet(null);
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressStreetMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setStreet(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.max");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressNumberMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setNumber(RandomStringUtils.randomNumeric(12));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.max");
        message = message.replace("{max}", "11");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressNumberInvalidPattern() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setNumber(RandomStringUtils.randomAlphabetic(5));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidNumber"));
    }

    @Test
    public void testAddressPostalCodeBlank() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setPostalCode("");
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressPostalCodeNull() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setPostalCode(null);
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testAddressPostalCodeMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setPostalCode(RandomStringUtils.randomNumeric(9));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.invalidPostalCode");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressPostalCodeMinLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setPostalCode(RandomStringUtils.randomNumeric(4));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.invalidPostalCode");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testAddressPostalCodeInvalidPattern() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setPostalCode(RandomStringUtils.randomAlphabetic(9));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidPostalCode"));
    }

    @Test
    public void testAddressComplementMaxLength() {
        Address address = AddressHelper.createValidUserAddress();
        Set<ConstraintViolation<Address>> violations;

        address.setComplement(RandomStringUtils.randomAlphabetic(1001));
        violations = this.validator.validate(address, AddressGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("field.max");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testGetAddressForGeocodeRequestWithNumber() {
        Address address = AddressHelper.createValidUserAddress();

        String requestAddress = address.getStreet() +
                ", " + address.getNumber() +
                " - " +
                address.getNeighborhood() +
                ", " +
                address.getCity() +
                " - " +
                address.getState() +
                ", " +
                address.getPostalCode() +
                ", " +
                address.getCountry();

        assertThat(address.getAddressForGeocodeRequest()).isEqualTo(requestAddress);

    }

    @Test
    public void testGetAddressForGeocodeRequestWithoutNumber() {
        Address address = AddressHelper.createValidUserAddress();
        address.setNumber(null);

        String requestAddress = address.getStreet() +
                " - " +
                address.getNeighborhood() +
                ", " +
                address.getCity() +
                " - " +
                address.getState() +
                ", " +
                address.getPostalCode() +
                ", " +
                address.getCountry();

        assertThat(address.getAddressForGeocodeRequest()).isEqualTo(requestAddress);
    }
}
