package felipe.emerim.crimespot.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PaginatedEntityTest extends BaseDomainTest {
    @Test
    public void testGetTotalPages() {
        PaginatedEntity<?> paginatedEntity = createPaginatedEntity();
        paginatedEntity.setPageLength(10);
        paginatedEntity.setTotalResults(100L);

        assertThat(paginatedEntity.getTotalPages()).isEqualTo(10D);
    }

    @Test
    public void testGetTotalPagesZeroTotalResults() {
        PaginatedEntity<?> paginatedEntity = createPaginatedEntity();
        paginatedEntity.setPageLength(10);
        paginatedEntity.setTotalResults(0L);

        assertThat(paginatedEntity.getTotalPages()).isEqualTo(0D);
    }

    @Test
    public void testGetTotalPagesZeroPageLength() {
        PaginatedEntity<?> paginatedEntity = createPaginatedEntity();
        paginatedEntity.setPageLength(0);
        paginatedEntity.setTotalResults(100L);

        assertThat(paginatedEntity.getTotalPages()).isEqualTo(0D);
    }

    protected PaginatedEntity<?> createPaginatedEntity() {

        return PaginatedEntity
                .builder()
                .build();
    }
}
