package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.config.Messages;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.Validator;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseDomainTest {
    @Autowired
    protected Validator validator;

    @Autowired
    protected Messages messages;
}
