package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.BaseDomainTest;
import felipe.emerim.crimespot.helper.AddressHelper;
import felipe.emerim.crimespot.helper.UserHelper;
import felipe.emerim.crimespot.validation.group.CreateUserGroup;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateEditUserFormTest extends BaseDomainTest {
    @Test
    public void testValidForm() {
        CreateEditUserForm form = this.createForm();
        Set<ConstraintViolation<CreateEditUserForm>> violations;

        form.setPasswordConfirm("123456");
        violations = this.validator.validate(form, CreateUserGroup.class);

        assertThat(violations).isEmpty();
    }

    /**
     * When the form is initialized a mismatch error should never appear
     */
    @Test
    public void testNoMismatchErrorWhenUserPasswordIsNull() {
        CreateEditUserForm form = this.createForm();
        Set<ConstraintViolation<CreateEditUserForm>> violations;

        form.getUser().setPassword(null);
        violations = this.validator.validate(form, CreateUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).doesNotContain(this.messages.get("user.passwordConfirmMismatch"));
    }

    @Test
    public void testFormBlankPasswordConfirm() {
        CreateEditUserForm form = this.createForm();
        Set<ConstraintViolation<CreateEditUserForm>> violations;

        form.setPasswordConfirm("");
        violations = this.validator.validate(form, CreateUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFormNullPasswordConfirm() {
        CreateEditUserForm form = this.createForm();
        Set<ConstraintViolation<CreateEditUserForm>> violations;

        form.setPasswordConfirm(null);
        violations = this.validator.validate(form, CreateUserGroup.class);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFormUserPasswordDifferentFromPasswordConfirm() {
        CreateEditUserForm form = this.createForm();
        Set<ConstraintViolation<CreateEditUserForm>> violations;

        form.setPasswordConfirm("1234567");
        violations = this.validator.validate(form, CreateUserGroup.class);

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("user.passwordConfirmMismatch"));
    }

    protected CreateEditUserForm createForm() {
        CreateEditUserForm form = new CreateEditUserForm();
        form.setAddress(AddressHelper.createValidUserAddress());
        form.setUser(UserHelper.createValidUser());
        form.getUser().setPassword("123456");

        return form;
    }
}
