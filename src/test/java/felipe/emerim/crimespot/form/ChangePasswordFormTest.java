package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.BaseDomainTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangePasswordFormTest extends BaseDomainTest {
    @Test
    public void testValidForm() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isEmpty();
    }

    /**
     * When the form is initialized a mismatch error should never appear
     */
    @Test
    public void testNoMismatchErrorWhenNewPasswordIsNull() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword(null);
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).doesNotContain(this.messages.get("user.passwordConfirmMismatch"));
    }

    @Test
    public void testFormBlankOriginalPassword() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("");
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFormNullOriginalPassword() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword(null);
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFormBlankNewPassword() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword("");
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFormNullNewPassword() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword(null);
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.required"));
    }

    @Test
    public void testFormNewPasswordMinLength() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword(RandomStringUtils.randomAlphabetic(4));
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.password.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "500");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testFormNewPasswordMaxLength() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword(RandomStringUtils.randomAlphabetic(501));
        form.setNewPasswordConfirm("1234567");

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("user.password.length");
        message = message.replace("{min}", "5");
        message = message.replace("{max}", "500");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testFormPasswordDifferentFromPasswordConfirm() {
        ChangePasswordForm form = new ChangePasswordForm();
        Set<ConstraintViolation<ChangePasswordForm>> violations;

        form.setOriginalPassword("123456");
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("12345678");

        violations = this.validator.validate(form);

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("user.passwordConfirmMismatch"));
    }
}
