package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.BaseDomainTest;
import felipe.emerim.crimespot.domain.File;
import felipe.emerim.crimespot.helper.AddressHelper;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.helper.OccurrenceHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateEditOccurrenceFormTest extends BaseDomainTest {

    @Test
    public void testValidFormWithoutFile() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;

        violations = this.validator.validate(form);

        assertThat(violations).isEmpty();
    }

    @Test
    public void testValidFormEmptyFile() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;

        MultipartFile result = new MockMultipartFile("filename",
                "fileName", MediaType.APPLICATION_PDF.toString(), new byte[]{});

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isEmpty();
    }

    @Test
    public void testValidFormWithFile() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;
        File file = FileHelper.createPDFFile();

        MultipartFile result = new MockMultipartFile("filename",
                "fileName", file.getContentType(), file.getDecodedContent());

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isEmpty();
    }

    @Test
    public void testFormDocumentNullContentType() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;
        File file = FileHelper.createPDFFile();

        MultipartFile result = new MockMultipartFile("filename",
                "fileName", null, file.getDecodedContent());

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidFile"));
    }

    @Test
    public void testFormDocumentInvalidContentType() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;
        File file = FileHelper.createPDFFile();

        MultipartFile result = new MockMultipartFile("filename",
                "fileName", "aaaa", file.getDecodedContent());

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidFile"));
    }

    @Test
    public void testFormDocumentNotAllowedContentType() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;
        File file = FileHelper.createPDFFile();

        MultipartFile result = new MockMultipartFile("filename",
                "fileName", MediaType.APPLICATION_JSON.toString(), file.getDecodedContent());

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidFile"));
    }

    @Test
    public void testFormDocumentNullFileName() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;
        File file = FileHelper.createPDFFile();

        MultipartFile result = new MockMultipartFile("filename",
                null, MediaType.APPLICATION_PDF.toString(), file.getDecodedContent());

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidFile"));
    }

    @Test
    public void testFormDocumentBlankFileName() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;
        File file = FileHelper.createPDFFile();

        MultipartFile result = new MockMultipartFile("filename",
                "", MediaType.APPLICATION_PDF.toString(), file.getDecodedContent());

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidFile"));
    }

    @Test
    public void testFormDocumentTooBig() throws IOException {
        CreateEditOccurrenceForm form = this.createForm();
        Set<ConstraintViolation<CreateEditOccurrenceForm>> violations;

        //6 MB
        byte[] bytes = new byte[6000000];

        MultipartFile result = new MockMultipartFile("filename",
                "filename", MediaType.APPLICATION_PDF.toString(), bytes);

        form.setOccurrenceDocument(result);

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        assertThat(messageList).contains(this.messages.get("field.invalidFile"));
    }

    protected CreateEditOccurrenceForm createForm() throws IOException {
        CreateEditOccurrenceForm form = new CreateEditOccurrenceForm();
        form.setAddress(AddressHelper.createValidOccurrenceAddress());
        form.setOccurrence(OccurrenceHelper.createValidOccurrence());

        return form;
    }
}
