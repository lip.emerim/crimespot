package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.BaseDomainTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class OccurrenceReviewFormTest extends BaseDomainTest {
    @Test
    public void testValidFormNullModeratorComments() {
        OccurrenceReviewForm form = new OccurrenceReviewForm();
        Set<ConstraintViolation<OccurrenceReviewForm>> violations;

        form.setModeratorComments(null);

        violations = this.validator.validate(form);

        assertThat(violations).isEmpty();
    }

    @Test
    public void testFormModeratorCommentsMaxLength() {
        OccurrenceReviewForm form = new OccurrenceReviewForm();
        Set<ConstraintViolation<OccurrenceReviewForm>> violations;

        form.setModeratorComments(RandomStringUtils.randomAlphabetic(1001));

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("occurrence.moderatorComments.length");
        message = message.replace("{min}", "15");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }

    @Test
    public void testFormModeratorCommentsMinLength() {
        OccurrenceReviewForm form = new OccurrenceReviewForm();
        Set<ConstraintViolation<OccurrenceReviewForm>> violations;

        form.setModeratorComments(RandomStringUtils.randomAlphabetic(14));

        violations = this.validator.validate(form);

        assertThat(violations).isNotEmpty();

        List<String> messageList = violations.stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.toList());

        String message = messages.get("occurrence.moderatorComments.length");
        message = message.replace("{min}", "15");
        message = message.replace("{max}", "1000");
        assertThat(messageList).contains(message);
    }
}
