package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.service.AddressService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * To test controller advices, the whole context has to be loaded.
 * WebMvcTest would not work here. We can use any controller to make
 * requests to.
 */
@WebMvcTest(controllers = {AddressController.class})
public class GlobalControllerExceptionHandlerTest extends BaseControllerTest {
    @MockBean
    AddressService addressService;

    @Test
    public void testNotFoundError() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        when(addressService.search(any(String.class))).thenThrow(NotFoundException.class);
        when(messages.get("error.notFound")).thenReturn("notFound");

        this.mvc.perform(get("/address/search")
                .with(csrf())
                .param("query", "sadas")
                .accept(MediaType.TEXT_HTML))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("path"))
                .andExpect(model().attributeExists("timestamp"))
                .andExpect(model().attributeExists("error"))
                .andExpect(model().attribute("message", is("notFound")))
                .andExpect(model().attribute("status", is(HttpStatus.NOT_FOUND)));

    }

    @Test
    public void testForbiddenError() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        when(addressService.search(any(String.class))).thenThrow(ForbiddenException.class);
        when(messages.get("error.forbidden")).thenReturn("forbidden");

        this.mvc.perform(get("/address/search")
                .with(csrf())
                .param("query", "sadas")
                .accept(MediaType.TEXT_HTML))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(status().isForbidden())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("path"))
                .andExpect(model().attributeExists("timestamp"))
                .andExpect(model().attributeExists("error"))
                .andExpect(model().attribute("message", is("forbidden")))
                .andExpect(model().attribute("status", is(HttpStatus.FORBIDDEN)));

    }

    @Test
    public void testInternalError() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        when(addressService.search(any(String.class))).thenThrow(RuntimeException.class);
        when(messages.get("error.internal")).thenReturn("internal");

        this.mvc.perform(get("/address/search")
                .with(csrf())
                .param("query", "sadas")
                .accept(MediaType.TEXT_HTML))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(status().isInternalServerError())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("path"))
                .andExpect(model().attributeExists("timestamp"))
                .andExpect(model().attributeExists("error"))
                .andExpect(model().attribute("message", is("internal")))
                .andExpect(model().attribute("status", is(HttpStatus.INTERNAL_SERVER_ERROR)));

    }
}
