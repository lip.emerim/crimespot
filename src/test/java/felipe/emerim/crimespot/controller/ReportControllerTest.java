package felipe.emerim.crimespot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import felipe.emerim.crimespot.domain.Report;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.exception.ReportAlreadyExistsException;
import felipe.emerim.crimespot.form.ReportCreateForm;
import felipe.emerim.crimespot.helper.ReportCreateFormHelper;
import felipe.emerim.crimespot.helper.ReportHelper;
import felipe.emerim.crimespot.service.ReportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.head;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReportController.class)
public class ReportControllerTest extends BaseControllerTest {
    @MockBean
    ReportService reportService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testExistsByOccurrenceIdAndUserIdAndActiveIsTrueOk() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.existsByOccurrenceIdAndUserIdAndActiveIsTrue(
                1000L, this.mockAuthContext.getUser().getId()
        )).thenReturn(true);

        this.mvc.perform(head("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().isNoContent());

        verify(reportService).existsByOccurrenceIdAndUserIdAndActiveIsTrue(
                1000L, this.mockAuthContext.getUser().getId());
    }

    @Test
    public void testExistsByOccurrenceIdAndUserIdAndActiveIsTrueNotOk() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.existsByOccurrenceIdAndUserIdAndActiveIsTrue(
                1000L, this.mockAuthContext.getUser().getId()
        )).thenReturn(false);

        this.mvc.perform(head("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().isNotFound());

        verify(reportService).existsByOccurrenceIdAndUserIdAndActiveIsTrue(
                1000L, this.mockAuthContext.getUser().getId());
    }

    @Test
    public void testCreateReport() throws Exception {
        ReportCreateForm form = ReportCreateFormHelper.createValidForm();
        Report report = ReportHelper.createValidReport();
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.createReport(
                this.mockAuthContext.getUser(), 1000L, form.getDescription()
        )).thenReturn(report);

        when(messages.get("report.create.success")).thenReturn("message");

        this.mvc.perform(post("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(form))
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is("message")))
                .andExpect(jsonPath("$.type", is("SUCCESS")));

        verify(reportService).createReport(this.mockAuthContext.getUser(), 1000L, form.getDescription());
    }

    @Test
    public void testCreateReportInvalidFields() throws Exception {
        ReportCreateForm form = ReportCreateFormHelper.createValidForm();
        Report report = ReportHelper.createValidReport();
        form.setDescription("sadas");
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.createReport(
                this.mockAuthContext.getUser(), 1000L, form.getDescription()
        )).thenReturn(report);

        when(messages.get("report.create.fieldErrors")).thenReturn("message");

        this.mvc.perform(post("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(form))
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is("message")))
                .andExpect(jsonPath("$.type", is("WARNING")));

        verifyNoInteractions(reportService);
    }

    @Test
    public void testCreateReportAlreadyReported() throws Exception {
        ReportCreateForm form = ReportCreateFormHelper.createValidForm();
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.createReport(
                this.mockAuthContext.getUser(), 1000L, form.getDescription()
        )).thenThrow(ReportAlreadyExistsException.class);

        when(messages.get("report.create.alreadyReported")).thenReturn("message");

        this.mvc.perform(post("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(form))
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is("message")))
                .andExpect(jsonPath("$.type", is("WARNING")));

        verify(reportService).createReport(this.mockAuthContext.getUser(), 1000L, form.getDescription());
    }

    @Test
    public void testCreateReportOccurrenceNotFound() throws Exception {
        ReportCreateForm form = ReportCreateFormHelper.createValidForm();
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.createReport(
                this.mockAuthContext.getUser(), 1000L, form.getDescription()
        )).thenThrow(NotFoundException.class);

        when(messages.get("report.create.occurrenceDeactivated")).thenReturn("message");

        this.mvc.perform(post("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(form))
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is("message")))
                .andExpect(jsonPath("$.type", is("WARNING")));

        verify(reportService).createReport(this.mockAuthContext.getUser(), 1000L, form.getDescription());
    }

    @Test
    public void testCreateReportUnexpectedError() throws Exception {
        ReportCreateForm form = ReportCreateFormHelper.createValidForm();
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.createReport(
                this.mockAuthContext.getUser(), 1000L, form.getDescription()
        )).thenThrow(RuntimeException.class);

        when(messages.get("report.create.unknownError")).thenReturn("message");

        this.mvc.perform(post("/occurrence/{occurrenceId}/report", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(form))
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is("message")))
                .andExpect(jsonPath("$.type", is("DANGER")));

        verify(reportService).createReport(this.mockAuthContext.getUser(), 1000L, form.getDescription());
    }
}
