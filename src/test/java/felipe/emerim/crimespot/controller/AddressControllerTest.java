package felipe.emerim.crimespot.controller;

import com.google.gson.Gson;
import felipe.emerim.crimespot.domain.GeocodingAddress;
import felipe.emerim.crimespot.service.AddressService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AddressController.class)
public class AddressControllerTest extends BaseControllerTest {
    @MockBean
    AddressService addressService;

    /**
     * Either admin or anonymous user can access this route.
     * We choose anonymous because it is simpler
     */
    @Test
    public void testSearch() throws Exception {
        Gson gson = new Gson();

        this.mockAuthContext.mockAuthAnonymous();

        GeocodingAddress address = new GeocodingAddress();

        when(addressService.search(any(String.class))).thenReturn(address);

        this.mvc.perform(get("/address/search")
                .with(csrf())
                .param("query", "sadas"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(gson.toJson(address)));
    }

    @Test
    public void testSearchTermTooShort() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        this.mvc.perform(get("/address/search")
                .with(csrf())
                .param("query", "sad"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void testSearchException() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        when(addressService.search(any(String.class))).thenThrow(IllegalArgumentException.class);

        this.mvc.perform(get("/address/search")
                .with(csrf())
                .param("query", "sadas"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }
}
