package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.ChangePasswordForm;
import felipe.emerim.crimespot.form.CreateEditUserForm;
import felipe.emerim.crimespot.form.UserSearchForm;
import felipe.emerim.crimespot.helper.AddressHelper;
import felipe.emerim.crimespot.helper.UserHelper;
import felipe.emerim.crimespot.service.AddressService;
import felipe.emerim.crimespot.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
public class UserControllerTest extends BaseControllerTest {
    @MockBean
    UserService userService;
    @MockBean
    AddressService addressService;

    @Test
    public void testCreateGet() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        this.mvc.perform(get("/user/create")
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("user/create"))
                .andExpect(model().attributeExists("form"))
                .andExpect(status().isOk());
    }

    //An authenticated user cannot access the create user page
    @Test
    public void testCreateGetAuthenticated() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        this.mvc.perform(get("/user/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testEditGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(userService.findById(any(Long.class))).thenReturn(Optional.of(this.mockAuthContext.getUser()));

        this.mvc.perform(get("/user/edit/{userId}", "1000")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("user/edit"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attribute("form", hasProperty("user",
                        is(this.mockAuthContext.getUser()))))
                .andExpect(status().isOk());

        verify(userService).findById(1000L);
    }

    @Test
    public void testEditGetNotActiveUser() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        this.mvc.perform(get("/user/edit/{userId}", "1001")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails())))
                .andExpect(status().isForbidden());

        verifyNoInteractions(userService);
    }

    @Test
    public void testEditGetNotFound() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(userService.findById(any(Long.class))).thenThrow(NotFoundException.class
        );

        this.mvc.perform(get("/user/edit/{userId}", "1000")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails())))
                .andExpect(status().isNotFound());

        verify(userService).findById(1000L);
    }

    @Test
    public void testListGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        PaginatedEntity<User> results = PaginatedEntity.<User>builder()
                .currentPage(1)
                .totalResults(100L)
                .pageLength(10)
                .data(new ArrayList<>())
                .build();

        UserSearchForm form = new UserSearchForm();
        form.setPage(1);

        when(userService.findNearestBySearchTermAndRole(any(User.class), any(UserSearchForm.class)))
                .thenReturn(results);

        this.mvc.perform(get("/user/list")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8")
                .flashAttr("searchForm", form))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("user/list"))
                .andExpect(model().attribute("users", is(results)))
                .andExpect(model().attribute("searchForm", is(form)))
                .andExpect(status().isOk());

        verify(userService).findNearestBySearchTermAndRole(this.mockAuthContext.getUser(), form);
    }

    @Test
    public void testChangePasswordGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        this.mvc.perform(get("/user/change-password")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("user/change-password"))
                .andExpect(model().attribute("form", instanceOf(ChangePasswordForm.class)))
                .andExpect(model().attribute("user", is(this.mockAuthContext.getUser())))
                .andExpect(status().isOk());

        verifyNoInteractions(userService);
    }

    @Test
    public void testCreatePost() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        CreateEditUserForm form = this.createForm();

        when(messages.get("user.saved")).thenReturn("saved");

        this.mvc.perform(post("/user/create")
                .with(csrf())
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/login"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "saved")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(userService).save(null, form.getUser());
    }

    @Test
    public void createPostLoggedInUser() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditUserForm form = this.createForm();

        when(messages.get("user.saved")).thenReturn("saved");

        this.mvc.perform(post("/user/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));

        verifyNoInteractions(addressService);
        verifyNoInteractions(userService);
    }

    @Test
    public void testCreateInvalidForm() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        CreateEditUserForm form = this.createForm();
        form.setAddress(null);

        this.mvc.perform(post("/user/create")
                .with(csrf())
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(status().is3xxRedirection());

        verifyNoInteractions(addressService);
        verifyNoInteractions(userService);
    }

    @Test
    public void testCreateInvalidAddress() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        CreateEditUserForm form = this.createForm();

        when(messages.get("address.notFound")).thenReturn("noAddress");

        doThrow(IllegalArgumentException.class).when(addressService).geocode(any(Address.class));

        this.mvc.perform(post("/user/create")
                .with(csrf())
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "noAddress")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verifyNoInteractions(userService);
    }

    @Test
    public void testCreateInvalidUser() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        CreateEditUserForm form = this.createForm();

        when(messages.get("user.exist")).thenReturn("alreadyExists");

        doThrow(IllegalArgumentException.class).when(userService).save(nullable(User.class), any(User.class));

        this.mvc.perform(post("/user/create")
                .with(csrf())
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "alreadyExists")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(userService).save(null, form.getUser());
    }

    @Test
    public void testEditPost() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditUserForm form = this.createForm();
        form.getUser().setId(1000L);

        when(messages.get("user.saved")).thenReturn("saved");

        this.mvc.perform(post("/user/edit")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "saved")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(userService).save(this.mockAuthContext.getUser(), form.getUser());
    }

    @Test
    public void testEditPostAnonymousUser() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        CreateEditUserForm form = this.createForm();

        this.mvc.perform(post("/user/edit")
                .with(csrf())
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrlPattern("**/login"))
                .andExpect(status().is3xxRedirection());

        verifyNoInteractions(addressService);
        verifyNoInteractions(userService);
    }

    @Test
    public void testEditPostInvalidForm() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditUserForm form = this.createForm();
        form.getUser().setId(1000L);
        form.setAddress(null);

        this.mvc.perform(post("/user/edit")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/edit/1000"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(status().is3xxRedirection());

        verifyNoInteractions(addressService);
        verifyNoInteractions(userService);
    }

    @Test
    public void testEditPostInvalidAddress() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditUserForm form = this.createForm();
        form.getUser().setId(1000L);

        when(messages.get("address.notFound")).thenReturn("noAddress");

        doThrow(IllegalArgumentException.class).when(addressService).geocode(any(Address.class));

        this.mvc.perform(post("/user/edit")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/edit/1000"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "noAddress")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verifyNoInteractions(userService);
    }

    @Test
    public void testEditPostInvalidUser() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditUserForm form = this.createForm();
        form.getUser().setId(1000L);

        when(messages.get("user.exist")).thenReturn("alreadyExists");

        doThrow(IllegalArgumentException.class).when(userService).save(nullable(User.class), any(User.class));

        this.mvc.perform(post("/user/edit")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("createEditUserForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/edit/1000"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "alreadyExists")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(userService).save(this.mockAuthContext.getUser(), form.getUser());
    }

    @Test
    public void testTogglePostActivate() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("user.activate.success", this.mockAuthContext.getUser().getUsername()))
                .thenReturn("activated");
        this.mockAuthContext.getUser().setActive(true);
        when(userService.toggle(any(User.class), anyLong())).thenReturn(this.mockAuthContext.getUser());

        this.mvc.perform(post("/user/toggle")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .param("userId", "1000")
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/list"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "activated")
                )))
                .andExpect(status().is3xxRedirection());

        verify(userService).toggle(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testTogglePostDeactivate() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("user.deactivate.success", this.mockAuthContext.getUser().getUsername()))
                .thenReturn("deactivated");
        this.mockAuthContext.getUser().setActive(false);
        when(userService.toggle(any(User.class), anyLong())).thenReturn(this.mockAuthContext.getUser());

        this.mvc.perform(post("/user/toggle")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .param("userId", "1000")
                .param("originUrl", "/")
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "deactivated")
                )))
                .andExpect(status().is3xxRedirection());

        verify(userService).toggle(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testToggleModeratorPostActivate() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("user.activate.moderator.success", this.mockAuthContext.getUser().getUsername()))
                .thenReturn("activated");
        when(userService.toggleModerator(any(User.class), anyLong())).thenReturn(this.mockAuthContext.getUser());

        this.mvc.perform(post("/user/toggle-moderator")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .param("userId", "1000")
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/list"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "activated")
                )))
                .andExpect(status().is3xxRedirection());

        verify(userService).toggleModerator(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testToggleModeratorPostDeactivate() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("user.deactivate.moderator.success", this.mockAuthContext.getUser().getUsername()))
                .thenReturn("deactivated");
        this.mockAuthContext.getUser().setRoles(this.mockAuthContext.getUser().getRoles().stream()
                .filter((e) -> e.getRole() != RoleEnum.ROLE_MODERATOR).collect(Collectors.toSet()));

        when(userService.toggleModerator(any(User.class), anyLong())).thenReturn(this.mockAuthContext.getUser());

        this.mvc.perform(post("/user/toggle-moderator")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .param("userId", "1000")
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/list"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "deactivated")
                )))
                .andExpect(status().is3xxRedirection());

        verify(userService).toggleModerator(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testChangePasswordPost() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        ChangePasswordForm form = new ChangePasswordForm();
        form.setOriginalPassword(this.mockAuthContext.getUser().getPassword());
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("1234567");

        when(messages.get("user.passwordChange.success")).thenReturn("changed");
        when(userService.changePassword(any(User.class), any(ChangePasswordForm.class)))
                .thenReturn(this.mockAuthContext.getUser());

        this.mvc.perform(post("/user/change-password")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("changePasswordForm", form)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/edit/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "changed")
                )))
                .andExpect(status().is3xxRedirection());

        verify(userService).changePassword(this.mockAuthContext.getUser(), form);
    }

    @Test
    public void testChangePasswordPostInvalidForm() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        ChangePasswordForm form = new ChangePasswordForm();
        form.setOriginalPassword(this.mockAuthContext.getUser().getPassword());
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("12345678");

        this.mvc.perform(post("/user/change-password")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("changePasswordForm", form)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/change-password"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(status().is3xxRedirection());

        verifyNoInteractions(userService);
    }

    @Test
    public void testChangePasswordPostMismatch() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        ChangePasswordForm form = new ChangePasswordForm();
        form.setOriginalPassword(this.mockAuthContext.getUser().getPassword());
        form.setNewPassword("1234567");
        form.setNewPasswordConfirm("1234567");

        when(messages.get("user.passwordChange.invalidOriginal")).thenReturn("mismatch");
        when(userService.changePassword(any(User.class), any(ChangePasswordForm.class)))
                .thenThrow(IllegalArgumentException.class);

        this.mvc.perform(post("/user/change-password")
                .with(csrf())
                .with(user(this.mockAuthContext.getUserDetails()))
                .flashAttr("changePasswordForm", form)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/user/change-password"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "mismatch")
                )))
                .andExpect(status().is3xxRedirection());

        verify(userService).changePassword(this.mockAuthContext.getUser(), form);
    }

    @Test
    public void testCannotAccessLoginWhenAuthenticated() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        this.mvc.perform(get("/login")
                .with(user(this.mockAuthContext.getUserDetails())))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

    public CreateEditUserForm createForm() {
        CreateEditUserForm form = new CreateEditUserForm();
        form.setUser(UserHelper.createUser());
        form.setAddress(AddressHelper.createAddress());
        form.setPasswordConfirm(form.getUser().getPassword());

        return form;
    }

}
