package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(HomeController.class)
public class HomeControllerTest extends BaseControllerTest {
    @Test
    public void testHomeAnonymous() throws Exception {
        this.mockAuthContext.mockAuthAnonymous();

        this.mvc.perform(get("/")
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attributeDoesNotExist("userLatitude"))
                .andExpect(model().attributeDoesNotExist("userLongitude"))
                .andExpect(view().name("index"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }

    @Test
    public void testHomeAuthenticated() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        User user = this.mockAuthContext.getUser();

        user.setAddress(new Address());
        user.getAddress().setLatitude(10);
        user.getAddress().setLongitude(20);

        this.mvc.perform(get("/")
                .with(user(this.mockAuthContext.getUserDetails()))
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attribute("userLatitude", is(10D)))
                .andExpect(model().attribute("userLongitude", is(20D)))
                .andExpect(view().name("index"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }
}
