package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.MockAuthContext;
import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.dao.OccurrenceDao;
import felipe.emerim.crimespot.dao.ReportDao;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.service.UserDetailsImplService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@Import(MockAuthContext.class)
public abstract class BaseControllerTest {
    @Autowired
    MockMvc mvc;
    @Autowired
    MockAuthContext mockAuthContext;
    @MockBean
    UserDetailsImplService implService;
    @MockBean
    Messages messages;
    @MockBean
    OccurrenceDao occurrenceDao;
    @MockBean
    ReportDao reportDao;

    /**
     * Mocking interceptors is not really good. You have to make sure preHandle returns true for all of them
     * otherwise your request will be aborted. Also thymeleaf templates would not work. So we did not mock them.
     */
    @BeforeEach
    public void setup() {
        Mockito.when(occurrenceDao.countByStatus(any(OccurrenceStatusEnum.class))).thenReturn(0L);
        Mockito.when(reportDao.countReportedOccurrences()).thenReturn(0L);
        Mockito.when(occurrenceDao.countInAnalysisByUser(any(User.class))).thenReturn(0L);
    }

    @AfterEach
    public void tearDown() {
        this.mockAuthContext.tearDown();
    }
}
