package felipe.emerim.crimespot.controller;

import com.google.gson.Gson;
import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.form.CreateEditOccurrenceForm;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import felipe.emerim.crimespot.form.OccurrenceReviewForm;
import felipe.emerim.crimespot.form.OccurrenceSearchForm;
import felipe.emerim.crimespot.helper.AddressHelper;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.helper.OccurrenceHelper;
import felipe.emerim.crimespot.service.AddressService;
import felipe.emerim.crimespot.service.OccurrenceService;
import felipe.emerim.crimespot.service.OccurrenceTypeService;
import felipe.emerim.crimespot.service.ReportService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(OccurrenceController.class)
public class OccurrenceControllerTest extends BaseControllerTest {
    @MockBean
    OccurrenceService occurrenceService;
    @MockBean
    OccurrenceTypeService occurrenceTypeService;
    @MockBean
    AddressService addressService;
    @MockBean
    ReportService reportService;

    @Test
    public void testListGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();
        OccurrenceSearchForm occurrenceSearchForm = new OccurrenceSearchForm();
        PaginatedEntity<Occurrence> occurrences = PaginatedEntity.<Occurrence>builder()
                .data(new ArrayList<>())
                .totalResults(0L)
                .pageLength(100)
                .currentPage(1)
                .build();

        when(occurrenceService.fullSearch(any(User.class), any(OccurrenceSearchForm.class), any(int.class)))
                .thenReturn(occurrences);

        this.mvc.perform(get("/occurrence/list")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .flashAttr("searchForm", occurrenceSearchForm))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("occurrence/list"))
                .andExpect(model().attributeExists("occurrences"))
                .andExpect(model().attribute("occurrences", hasProperty("pageLength", is(100))))
                .andExpect(model().attribute("user", is(this.mockAuthContext.getUser())))
                .andExpect(status().isOk());

        verify(occurrenceService).fullSearch(this.mockAuthContext.getUser(), occurrenceSearchForm, 8);
    }


    @Test
    public void testMapListGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();
        Gson gson = new Gson();
        OccurrenceMapSearchForm form = new OccurrenceMapSearchForm();
        PaginatedEntity<Occurrence> occurrences = PaginatedEntity.<Occurrence>builder()
                .data(new ArrayList<>())
                .totalResults(0L)
                .pageLength(100)
                .currentPage(1)
                .build();

        when(occurrenceService.mapFullSearch(any(OccurrenceMapSearchForm.class)))
                .thenReturn(occurrences);

        this.mvc.perform(post("/occurrence/map-list")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(form)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageLength", is(100)));

        verify(occurrenceService).mapFullSearch(form);
    }

    @Test
    public void testCreateGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        this.mvc.perform(get("/occurrence/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("occurrence/create"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attributeExists("occurrenceTypes"))
                .andExpect(status().isOk());

        verify(occurrenceTypeService, atLeastOnce()).findAll();
    }

    @Test
    public void testEditGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        when(occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(anyLong()))
                .thenReturn(Optional.of(OccurrenceHelper.createOccurrence()));

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        this.mvc.perform(get("/occurrence/edit/{id}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("occurrence/edit"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attribute("form",
                        hasProperty("occurrence",
                                hasProperty("title", is("Dummy occurrence")))))
                .andExpect(model().attributeExists("occurrenceTypes"))

                .andExpect(status().isOk());

        verify(occurrenceTypeService, atLeastOnce()).findAll();
        verify(occurrenceService).findByIdAndUserActiveIsTrueAndActiveIsTrue(1000L);
    }

    @Test
    public void testEditGetNotAllowed() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        when(occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(anyLong()))
                .thenReturn(Optional.of(OccurrenceHelper.createOccurrence()));

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(false);

        this.mvc.perform(get("/occurrence/edit/{id}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(status().isForbidden());

        verifyNoInteractions(occurrenceTypeService);
    }

    @Test
    public void testEditGetNotFound() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        when(occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(anyLong()))
                .thenReturn(Optional.empty());

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        this.mvc.perform(get("/occurrence/edit/{id}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(status().isNotFound());

        verifyNoInteractions(occurrenceTypeService);
    }

    @Test
    public void testViewGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        occurrence.getModerator().setId(1000L);
        occurrence.getUser().setId(1000L);

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        when(reportService.getActiveReportsByOccurrenceId(anyLong()))
                .thenReturn(new ArrayList<>());

        when(occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(anyLong()))
                .thenReturn(Optional.of(occurrence));

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        when(occurrenceService.canViewOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        this.mvc.perform(get("/occurrence/view/{id}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("occurrence/view"))
                .andExpect(model().attributeExists("occurrence"))
                .andExpect(model().attribute("occurrence", hasProperty("title", is("Dummy occurrence"))))
                .andExpect(model().attribute("isModerator", is(true)))
                .andExpect(model().attribute("isDeletableByUser", is(true)))
                .andExpect(model().attribute("isReviewableByUser", is(false)))
                .andExpect(model().attribute("canRevokeApproval", is(true)))
                .andExpect(model().attribute("isOwner", is(true)))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attributeExists("reports"))
                .andExpect(status().isOk());

        verify(occurrenceService).findByIdAndUserActiveIsTrueAndActiveIsTrue(1000L);
        verify(occurrenceService).canViewOccurrence(this.mockAuthContext.getUser(), occurrence);
        verify(occurrenceService).canEditOccurrence(this.mockAuthContext.getUser(), occurrence);
        verify(reportService).getActiveReportsByOccurrenceId(1000L);
    }

    @Test
    public void testViewGetNotExists() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        when(reportService.getActiveReportsByOccurrenceId(anyLong()))
                .thenReturn(new ArrayList<>());

        when(occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(anyLong()))
                .thenReturn(Optional.empty());

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        when(occurrenceService.canViewOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        this.mvc.perform(get("/occurrence/view/{id}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(status().isNotFound());

        verify(occurrenceService).findByIdAndUserActiveIsTrueAndActiveIsTrue(1000L);
    }

    @Test
    public void testViewGetNotAllowed() throws Exception {
        this.mockAuthContext.mockAuthAdmin();
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        occurrence.getModerator().setId(1000L);
        occurrence.getUser().setId(1000L);

        when(occurrenceTypeService.findAll())
                .thenReturn(new ArrayList<>());

        when(reportService.getActiveReportsByOccurrenceId(anyLong()))
                .thenReturn(new ArrayList<>());

        when(occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(anyLong()))
                .thenReturn(Optional.of(occurrence));

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(true);

        when(occurrenceService.canViewOccurrence(any(User.class), any(Occurrence.class)))
                .thenReturn(false);

        this.mvc.perform(get("/occurrence/view/{id}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(status().isForbidden());

        verify(occurrenceService).findByIdAndUserActiveIsTrueAndActiveIsTrue(1000L);
        verify(occurrenceService).canViewOccurrence(this.mockAuthContext.getUser(), occurrence);
    }

    @Test
    public void testDownloadDocumentGet() throws Exception {
        this.mockAuthContext.mockAuthAdmin();
        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        occurrence.getModerator().setId(1000L);
        occurrence.getUser().setId(1000L);

        when(occurrenceService.getDocument(any(User.class), any(Long.class), any(Long.class)))
                .thenReturn(occurrence.getOccurrenceDocument());

        this.mvc.perform(get("/occurrence/{occurrenceId}/document/download/{documentId}", "1000", "1001")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.APPLICATION_PDF))
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andExpect(status().isOk())
                .andExpect(content().bytes(occurrence.getOccurrenceDocument().getDecodedContent()));

        verify(occurrenceService).getDocument(this.mockAuthContext.getUser(), 1000L, 1001L);
    }

    @Test
    public void testStartReviewPost() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.getActiveReportsByOccurrenceId(anyLong()))
                .thenReturn(new ArrayList<>());

        when(occurrenceService.startReview(any(User.class), anyLong()))
                .thenReturn(new Occurrence());

        when(messages.get("occurrence.startReview.success")).thenReturn("success");

        this.mvc.perform(post("/occurrence/review/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "success")
                )))
                .andExpect(status().is3xxRedirection());

        verify(occurrenceService).startReview(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testStartReviewPostCannotReview() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(reportService.getActiveReportsByOccurrenceId(anyLong()))
                .thenReturn(new ArrayList<>());

        when(occurrenceService.startReview(any(User.class), anyLong()))
                .thenThrow(IllegalArgumentException.class);

        when(messages.get("occurrence.startReview.error")).thenReturn("error");

        this.mvc.perform(post("/occurrence/review/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/list"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "error")
                )))
                .andExpect(status().is3xxRedirection());

        verify(occurrenceService).startReview(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testCreatePost() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();

        when(messages.get("occurrence.saved")).thenReturn("saved");

        this.mvc.perform(post("/occurrence/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/list"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "saved")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(occurrenceService).setDocument(form.getOccurrence(), form.getOccurrenceDocument());
        verify(occurrenceService).save(this.mockAuthContext.getUser(), form.getOccurrence());
    }

    @Test
    public void testCreateInvalidForm() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();
        form.setAddress(null);

        this.mvc.perform(post("/occurrence/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(status().is3xxRedirection());

        verifyNoInteractions(addressService);
        verifyNoInteractions(occurrenceService);
        verifyNoInteractions(occurrenceService);
    }

    @Test
    public void testCreateInvalidAddress() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();

        when(messages.get("address.notFound")).thenReturn("noAddress");

        doThrow(IllegalArgumentException.class).when(addressService).geocode(any(Address.class));

        this.mvc.perform(post("/occurrence/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "noAddress")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verifyNoInteractions(occurrenceService);
    }

    @Test
    public void testCreateInvalidDocument() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();

        when(messages.get("occurrence.invalidFile")).thenReturn("invalidFile");

        doThrow(IOException.class).when(occurrenceService).setDocument(any(Occurrence.class), any(MultipartFile.class));

        this.mvc.perform(post("/occurrence/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "invalidFile")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(occurrenceService).setDocument(form.getOccurrence(), form.getOccurrenceDocument());
        verify(occurrenceService, never()).save(any(User.class), any(Occurrence.class));
    }

    @Test
    public void testCreateDidNotWaitForTimeout() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();

        when(messages.get("occurrence.timeout")).thenReturn("timeout");

        doThrow(IllegalArgumentException.class).when(occurrenceService).save(any(User.class), any(Occurrence.class));

        this.mvc.perform(post("/occurrence/create")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/create"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "timeout")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(occurrenceService).setDocument(form.getOccurrence(), form.getOccurrenceDocument());
        verify(occurrenceService).save(any(User.class), any(Occurrence.class));
    }

    @Test
    public void testEditPost() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();
        form.getOccurrence().setId(1000L);

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class))).thenReturn(true);
        when(messages.get("occurrence.saved")).thenReturn("saved");

        this.mvc.perform(post("/occurrence/edit")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "saved")
                )))
                .andExpect(status().is3xxRedirection());

        verify(addressService).geocode(form.getAddress());
        verify(occurrenceService).setDocument(form.getOccurrence(), form.getOccurrenceDocument());
        verify(occurrenceService).save(this.mockAuthContext.getUser(), form.getOccurrence());
        verify(occurrenceService).canEditOccurrence(this.mockAuthContext.getUser(), form.getOccurrence());
    }

    @Test
    public void testEditInvalidForm() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();
        form.getOccurrence().setId(1000L);
        form.setAddress(null);

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class))).thenReturn(true);

        this.mvc.perform(post("/occurrence/edit")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/edit/1000"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(status().is3xxRedirection());

        verify(occurrenceService).canEditOccurrence(this.mockAuthContext.getUser(), form.getOccurrence());
        verifyNoInteractions(addressService);
        verify(occurrenceService, never()).setDocument(any(Occurrence.class), any(MultipartFile.class));
        verify(occurrenceService, never()).save(any(User.class), any(Occurrence.class));
    }

    @Test
    public void testEditInvalidAddress() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();
        form.getOccurrence().setId(1000L);

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class))).thenReturn(true);
        doThrow(IllegalArgumentException.class).when(addressService).geocode(any(Address.class));
        when(messages.get("address.notFound")).thenReturn("noAddress");

        this.mvc.perform(post("/occurrence/edit")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/edit/1000"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "noAddress")
                )))
                .andExpect(status().is3xxRedirection());

        verify(occurrenceService).canEditOccurrence(this.mockAuthContext.getUser(), form.getOccurrence());
        verify(addressService).geocode(form.getAddress());
        verify(occurrenceService, never()).setDocument(any(Occurrence.class), any(MultipartFile.class));
        verify(occurrenceService, never()).save(any(User.class), any(Occurrence.class));
    }

    @Test
    public void testEditInvalidDocument() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();
        form.getOccurrence().setId(1000L);

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class))).thenReturn(true);
        when(messages.get("occurrence.invalidFile")).thenReturn("invalidFile");
        doThrow(IOException.class).when(occurrenceService).setDocument(any(Occurrence.class), any(MultipartFile.class));

        this.mvc.perform(post("/occurrence/edit")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(redirectedUrl("/occurrence/edit/1000"))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.DANGER, "invalidFile")
                )))
                .andExpect(status().is3xxRedirection());

        verify(occurrenceService).canEditOccurrence(this.mockAuthContext.getUser(), form.getOccurrence());
        verify(addressService).geocode(form.getAddress());
        verify(occurrenceService).setDocument(form.getOccurrence(), form.getOccurrenceDocument());
        verify(occurrenceService, never()).save(any(User.class), any(Occurrence.class));
    }

    @Test
    public void testEditNotAllowed() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        CreateEditOccurrenceForm form = this.createForm();
        form.getOccurrence().setId(1000L);

        when(messages.get("error.forbidden")).thenReturn("forbidden");

        when(occurrenceService.canEditOccurrence(any(User.class), any(Occurrence.class))).thenReturn(false);

        this.mvc.perform(post("/occurrence/edit")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("createEditOccurrenceForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(status().isForbidden());

        verify(occurrenceService).canEditOccurrence(this.mockAuthContext.getUser(), form.getOccurrence());
        verifyNoInteractions(addressService);
        verify(occurrenceService, never()).setDocument(any(Occurrence.class), any(MultipartFile.class));
        verify(occurrenceService, never()).save(any(User.class), any(Occurrence.class));
    }

    /*
     * Timeout error will never occur when editing an occurrence. Which is why it was not tested
     */


    @Test
    public void testApprove() throws Exception {

        this.mockAuthContext.mockAuthAdmin();

        OccurrenceReviewForm form = new OccurrenceReviewForm();
        form.setModeratorComments("Really cool moderator comments");

        when(messages.get("occurrence.approved")).thenReturn("approved");

        this.mvc.perform(post("/occurrence/approve/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("occurrenceReviewForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "approved")
                )))
                .andExpect(flash().attribute("form", nullValue()));

        verify(occurrenceService).finishReview(this.mockAuthContext.getUser(), 1000L,
                form.getModeratorComments(), OccurrenceStatusEnum.APPROVED);
    }

    @Test
    public void testApproveInvalidForm() throws Exception {

        this.mockAuthContext.mockAuthAdmin();

        OccurrenceReviewForm form = new OccurrenceReviewForm();
        form.setModeratorComments("R");

        this.mvc.perform(post("/occurrence/approve/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("occurrenceReviewForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", nullValue()))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)));

        verifyNoInteractions(occurrenceService);
    }

    @Test
    public void testReject() throws Exception {

        this.mockAuthContext.mockAuthAdmin();

        OccurrenceReviewForm form = new OccurrenceReviewForm();
        form.setModeratorComments("Really cool moderator comments");

        when(messages.get("occurrence.rejected")).thenReturn("rejected");

        this.mvc.perform(post("/occurrence/reject/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("occurrenceReviewForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "rejected")
                )))
                .andExpect(flash().attribute("form", nullValue()));

        verify(occurrenceService).finishReview(this.mockAuthContext.getUser(), 1000L,
                form.getModeratorComments(), OccurrenceStatusEnum.REJECTED);
    }

    @Test
    public void testRejectInvalidForm() throws Exception {

        this.mockAuthContext.mockAuthAdmin();

        OccurrenceReviewForm form = new OccurrenceReviewForm();
        form.setModeratorComments("R");

        this.mvc.perform(post("/occurrence/reject/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .flashAttr("occurrenceReviewForm", form)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", nullValue()))
                .andExpect(flash().attributeExists("org.springframework.validation.BindingResult.form"))
                .andExpect(flash().attribute("form", is(form)));

        verifyNoInteractions(occurrenceService);
    }

    @Test
    public void testRevokeApproval() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("occurrence.revokedApproval")).thenReturn("revokedApproval");

        this.mvc.perform(post("/occurrence/revokeApproval/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "revokedApproval")
                )));

        verify(occurrenceService).revokeApproval(this.mockAuthContext.getUser(), 1000L);

    }

    @Test
    public void testDeactivate() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("occurrence.deactivated")).thenReturn("deactivated");

        this.mvc.perform(post("/occurrence/deactivate/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/list"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "deactivated")
                )));

        verify(occurrenceService).deactivate(this.mockAuthContext.getUser(), 1000L);
    }

    @Test
    public void testRemoveDocument() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("occurrence.document.remove.success")).thenReturn("documentRemoved");

        this.mvc.perform(post("/occurrence/{occurrenceId}/document/{documentId}/remove", "1000", "1001")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "documentRemoved")
                )));

        verify(occurrenceService).removeDocument(this.mockAuthContext.getUser(), 1000L, 1001L);

    }

    @Test
    public void testAcceptReports() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("occurrence.reports.accept.success")).thenReturn("accepted");

        this.mvc.perform(post("/occurrence/reports/accept/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "accepted")
                )));

        verify(occurrenceService).acceptReports(1000L, this.mockAuthContext.getUser());
    }

    @Test
    public void testRejectReports() throws Exception {
        this.mockAuthContext.mockAuthAdmin();

        when(messages.get("occurrence.reports.reject.success")).thenReturn("rejected");

        this.mvc.perform(post("/occurrence/reports/reject/{occurrenceId}", "1000")
                .with(user(this.mockAuthContext.getUserDetails()))
                .with(csrf())
                .characterEncoding("UTF-8"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/occurrence/view/1000"))
                .andExpect(flash().attribute("messages", hasItem(
                        new Alert(AlertTypes.SUCCESS, "rejected")
                )));

        verify(occurrenceService).rejectReports(1000L, this.mockAuthContext.getUser());
    }

    protected CreateEditOccurrenceForm createForm() throws IOException {
        File file = FileHelper.createPDFFile();
        MultipartFile multipartFile = new MockMultipartFile("pdfFile", "pdfFile",
                file.getContentType(), file.getDecodedContent());
        CreateEditOccurrenceForm form = new CreateEditOccurrenceForm();
        form.setAddress(AddressHelper.createAddress());
        form.setOccurrenceDocument(multipartFile);
        form.setOccurrence(OccurrenceHelper.createOccurrence());
        form.getOccurrence().setOccurrenceDocument(file);

        return form;
    }
}
