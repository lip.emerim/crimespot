package felipe.emerim.crimespot;

import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.domain.Role;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.service.UserDetailsImplService;
import lombok.Getter;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;

@Component
@Getter
public class MockAuthContext {

    public static Long USER_ID = 1000L;

    protected UserImpl userDetails;
    protected User user;

    /**
     * Every class that uses this MUST mock this bean
     */
    @Autowired
    UserDetailsImplService implService;

    public void tearDown() {
        this.user = null;
        this.userDetails = null;
    }

    public void mockAuthAdmin() {
        final Long adminId = 1000L;
        final String adminName = "admin user";
        final String adminEmail = "admin@crimespot.com";
        final String adminUsername = "admin";
        final String adminPassword = "user";
        final String[] adminRoles = {
                RoleEnum.ROLE_ADMIN.name(),
                RoleEnum.ROLE_MODERATOR.name(),
                RoleEnum.ROLE_USER.name()
        };

        mockAuth(adminId, adminEmail, adminName, adminUsername, adminPassword, adminRoles);
    }

    public void mockAuthModerator() {
        final Long moderatorId = 1000L;
        final String moderatorName = "moderator user";
        final String moderatorEmail = "moderator@crimespot.com";
        final String moderatorUsername = "moderator";
        final String moderatorPassword = "user";
        final String[] moderatorRoles = {
                RoleEnum.ROLE_MODERATOR.name(),
                RoleEnum.ROLE_USER.name()
        };

        mockAuth(moderatorId, moderatorEmail, moderatorName, moderatorUsername, moderatorPassword, moderatorRoles);
    }

    public void mockAuthUser() {
        final Long userId = 1000L;
        final String userName = "authenticated user";
        final String userEmail = "user@crimespot.com";
        final String userUsername = "user";
        final String userPassword = "user";
        final String[] userRoles = {
                RoleEnum.ROLE_USER.name()
        };

        mockAuth(userId, userEmail, userName, userUsername, userPassword, userRoles);
    }

    public void mockAuthAnonymous() {
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(new AnonymousAuthenticationToken(
                "guest", "guest", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS")
        ));
        Mockito.when(implService.loadUserByUsername(any(String.class))).thenReturn(null);
        SecurityContextHolder.setContext(securityContext);
    }

    protected void mockAuth(Long userId, String userEmail, String name, String username, String password,
                            String[] roleList) {
        user = new User();
        user.setId(userId);
        user.setEmail(userEmail);
        user.setName(name);
        user.setPassword(password);
        user.setUsername(username);
        user.setRoles(Arrays.stream(roleList).map((e) -> Role.builder().role(RoleEnum.valueOf(e)).build())
                .collect(Collectors.toSet()));

        userDetails = new UserImpl(
                username,
                password,
                AuthorityUtils.createAuthorityList(roleList),
                user
        );

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(securityContext.getAuthentication().getPrincipal()).thenReturn(userDetails);
        Mockito.when(implService.loadUserByUsername(username)).thenReturn(userDetails);
        SecurityContextHolder.setContext(securityContext);
    }
}


