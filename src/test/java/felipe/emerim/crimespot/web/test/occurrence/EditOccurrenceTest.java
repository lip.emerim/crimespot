package felipe.emerim.crimespot.web.test.occurrence;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.ErrorPage;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.occurrence.EditOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ListOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ViewOccurrencePage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import java.io.IOException;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class EditOccurrenceTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @Page
    ListOccurrencePage listOccurrencePage;

    @Page
    EditOccurrencePage editOccurrencePage;

    @Page
    ViewOccurrencePage viewOccurrencePage;

    @Page
    ErrorPage errorPage;

    @RetryingTest(5)
    public void testEditOccurrence() {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        editOccurrencePage.getTitleField().fill().with("cool edited occurrence");
        editOccurrencePage.submitForm();

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isNotPresent();

        assertThat(viewOccurrencePage.getOccurrenceTitleField()).hasText("cool edited occurrence");
        // Since our user is a moderator, he has the link view available
        assertThat(viewOccurrencePage.getOccurrenceUserLinkText()).hasText("admin");
    }

    @RetryingTest(5)
    public void testEditOccurrenceInvalidData() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.TXT_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillForm(
                "c",
                "c",
                "20/13/5098",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).submitForm();

        editOccurrencePage.isAt("1000");
        editOccurrencePage.assertFormErrorCount(editOccurrencePage.getForm(), 4);

    }

    @RetryingTest(5)
    public void testEditOccurrenceMissingRequiredFields() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        editOccurrencePage.fillForm(
                "",
                "",
                "",
                "",
                null
        ).fillAddressForm(
                "92032-360",
                "",
                ""
        ).submitForm();

        editOccurrencePage.isAt("1000");
    }

    @RetryingTest(5)
    public void testEditOccurrenceInvalidAddress() {
        this.mockGeocodeRequestNotFound();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        editOccurrencePage.fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).fillAddressFormWithoutPostalCode(
                "Brasil",
                "dasasdasdas",
                "dasdasdsa",
                "sadasdfsadsa",
                "dsfsfdsfds",
                "599",
                "Cwerte"
        ).submitForm();

        editOccurrencePage.isAt("1000");

        editOccurrencePage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("address.notFound"));

        assertThat(editOccurrencePage.getDownloadDocumentButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testEditOccurrenceWithDocument() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.PNG_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillDocumentField(path);

        editOccurrencePage.submitForm();

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isPresent();
        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isDisplayed();

        viewOccurrencePage.getEditOccurrenceButton().click();

        assertThat(editOccurrencePage.getDownloadDocumentButton()).isDisplayed();
    }

    @RetryingTest(5)
    public void testCannotEditOtherUserOccurrence() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();

        editOccurrencePage.go("1000");

        errorPage.checkErrorStatus("403 FORBIDDEN");
    }

    @RetryingTest(5)
    public void testCannotEditNotPendingOccurrence() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1052");
        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();

        editOccurrencePage.go("1052");

        errorPage.checkErrorStatus("403 FORBIDDEN");
    }

    @RetryingTest(5)
    public void testNonLoggedInUserCannotEditOccurrence() {
        indexPage.go();
        indexPage.isAt();
        editOccurrencePage.go("1052");
        loginPage.isAt();
    }

    @RetryingTest(5)
    public void testRemoveDocument() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.PNG_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillDocumentField(path);

        editOccurrencePage.submitForm();

        viewOccurrencePage.getRemoveDocumentButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getRemoveDocumentModal());

        viewOccurrencePage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.document.remove.success"));

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isNotPresent();
    }

    @RetryingTest(5)
    public void testCannotRemoveDocumentNotPendingOccurrence() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.PNG_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillDocumentField(path);

        editOccurrencePage.submitForm();

        viewOccurrencePage.getStartReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getStartReviewModal());

        viewOccurrencePage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.startReview.success"));

        assertThat(viewOccurrencePage.getRemoveDocumentButton()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).hasText("Arquivo do BO");
    }
}
