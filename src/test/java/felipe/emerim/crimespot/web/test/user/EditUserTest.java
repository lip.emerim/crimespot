package felipe.emerim.crimespot.web.test.user;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.ErrorPage;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.user.EditUserPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class EditUserTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    EditUserPage editUserPage;

    @Page
    IndexPage indexPage;

    @Page
    ErrorPage errorPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testEditUser() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");
        assertThat(editUserPage.getChangePasswordButton().present()).isTrue();

        editUserPage.fillUserForm(
                "John Doe",
                "admin",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();


        indexPage.isAt();
        indexPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(), messages.get("user.saved"));
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.$("#user-info")).present();

        indexPage.closeToast("success");
        indexPage.verifyLoggedIn();
        assertThat(indexPage.getUserInfo().$("span").get(0).text()).isEqualTo("John Doe");

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.checkFieldValues(
                "John Doe",
                "admin",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2"
        );
    }

    @RetryingTest(5)
    public void testEditUserUsername() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();

        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        assertThat(editUserPage.getChangePasswordButton().present()).isTrue();

        editUserPage.getUsernameField().click().fill().withText("john.doe");

        editUserPage.submitForm();

        loginPage.isAt();

        loginPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(), messages.get("user.saved"));

        loginPage.fillAndSubmitFormAwait("john.doe", "user");

        indexPage.isAt();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getUserInfo()).present();
    }

    @RetryingTest(5)
    public void testEditDifferentUser() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        editUserPage.go("1001");

        errorPage.checkErrorStatus("403 FORBIDDEN");
    }

    @RetryingTest(5)
    public void testEditUserMissingRequiredFields() {
        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.fillUserForm(
                "",
                "",
                "",
                "",
                "",
                "2"
        ).fillAddressForm(
                "9999999",
                "",
                ""
        ).submitForm();

        editUserPage.isAt();
    }

    @RetryingTest(5)
    public void testEditUserInvalidFields() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.fillUserForm(
                "J",
                "j",
                "a",
                "09/09/9999",
                "776190",
                "2"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        editUserPage.isAt();
        editUserPage.assertFormErrorCount(editUserPage.getForm(), 5);
    }

    @RetryingTest(5)
    public void testEditUserDuplicateName() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.fillUserForm(
                "John Doe",
                "moderator",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        editUserPage.isAt();

        editUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.exist"));
    }

    @RetryingTest(5)
    public void testEditUserDuplicateEmail() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.fillUserForm(
                "John Doe",
                "admin",
                "moderator@gmail.com",
                "26/06/1998",
                "77619014063",
                "2"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        editUserPage.isAt();

        editUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.exist"));
    }

    @RetryingTest(5)
    public void testEditUserDuplicateCpf() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.fillUserForm(
                "John Doe",
                "admin",
                "john.doe@gmail.com",
                "26/06/1998",
                "80789265028",
                "2"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        editUserPage.isAt();

        editUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.exist"));
    }

    @RetryingTest(5)
    public void testEditUserInvalidAddress() {

        this.mockGeocodeRequestNotFound();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");

        editUserPage.fillUserForm(
                "John Doe",
                "john.doe",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        ).fillAddressFormWithoutPostalCode(
                "Brasil",
                "dasasdasdas",
                "dasdasdsa",
                "sadasdfsadsa",
                "asdfjadshas",
                "599",
                "Cwerte"
        ).submitForm();

        editUserPage.isAt();
        editUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("address.notFound"));
    }
}
