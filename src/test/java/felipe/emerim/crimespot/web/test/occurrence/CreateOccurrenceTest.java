package felipe.emerim.crimespot.web.test.occurrence;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.occurrence.CreateOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ListOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ViewOccurrencePage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class CreateOccurrenceTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @Page
    ListOccurrencePage listOccurrencePage;

    @Page
    CreateOccurrencePage createOccurrencePage;

    @Page
    ViewOccurrencePage viewOccurrencePage;

    @RetryingTest(5)
    public void testCreateOccurrence() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.getCreateOccurrenceButton().click();

        createOccurrencePage.isAt();

        Resource resource = FileHelper.loadFile(FileHelper.PDF_PATH);
        String path = resource.getFile().getAbsolutePath();

        createOccurrencePage.fillForm(
                "cool occurrence",
                "cool occurrence description",
                "12/10/2020",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).submitForm();

        listOccurrencePage.isAt();
        listOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.saved")
        );

        listOccurrencePage.verifyAnyContainTerm("cool occurrence");
        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1");

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isPresent();
        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isDisplayed();

        assertThat(viewOccurrencePage.getOccurrenceTitleField()).hasText("cool occurrence");
        // Since our user is a moderator, he has the link view available
        assertThat(viewOccurrencePage.getOccurrenceUserLinkText()).hasText("moderator");
    }

    @RetryingTest(5)
    public void testCreateOccurrenceInvalidData() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        await().explicitlyFor(2, TimeUnit.SECONDS);
        listOccurrencePage.getCreateOccurrenceButton().click();

        createOccurrencePage.isAt();

        Resource resource = FileHelper.loadFile(FileHelper.TXT_PATH);
        String path = resource.getFile().getAbsolutePath();

        createOccurrencePage.fillForm(
                "c",
                "c",
                "20/13/5098",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).submitForm();

        createOccurrencePage.isAt();
        createOccurrencePage.assertFormErrorCount(createOccurrencePage.getForm(), 4);

    }

    @RetryingTest(5)
    public void testCreateOccurrenceMissingRequiredFields() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.getCreateOccurrenceButton().click();

        createOccurrencePage.isAt();

        createOccurrencePage.fillForm(
                "",
                "",
                "",
                "",
                null
        ).fillAddressForm(
                "92032-360",
                "",
                ""
        ).submitForm();

        createOccurrencePage.isAt();
    }

    @RetryingTest(5)
    public void testCreateOccurrenceInvalidAddress() throws IOException {
        this.mockGeocodeRequestNotFound();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.getCreateOccurrenceButton().click();

        createOccurrencePage.isAt();

        Resource resource = FileHelper.loadFile(FileHelper.PDF_PATH);
        String path = resource.getFile().getAbsolutePath();

        createOccurrencePage.fillForm(
                "cool occurrence",
                "cool occurrence description",
                "12/10/2020",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).fillAddressFormWithoutPostalCode(
                "Brasil",
                "dasasdasdas",
                "dasdasdsa",
                "sadasdfsadsa",
                "asdhasdasdga",
                "599",
                "Cwerte"
        ).submitForm();

        createOccurrencePage.isAt();

        createOccurrencePage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("address.notFound"));

        assertThat(createOccurrencePage.getDownloadDocumentButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testCreateOccurrenceSameTypeTimeout() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.getCreateOccurrenceButton().click();

        createOccurrencePage.isAt();

        Resource resource = FileHelper.loadFile(FileHelper.PDF_PATH);
        String path = resource.getFile().getAbsolutePath();

        createOccurrencePage.fillForm(
                "cool occurrence",
                "cool occurrence description",
                "12/10/2020",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).submitForm();

        listOccurrencePage.isAt();
        listOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.saved")
        );
        listOccurrencePage.closeToast(AlertTypes.SUCCESS.getDescription());

        listOccurrencePage.getCreateOccurrenceButton().click();

        createOccurrencePage.isAt();
        createOccurrencePage.fillForm(
                "cool occurrence",
                "cool occurrence description",
                "12/10/2020",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).submitForm();

        createOccurrencePage.isAt();
        createOccurrencePage.verifyToastWithValue(
                AlertTypes.DANGER.getDescription(),
                messages.get("occurrence.timeout")
        );

        assertThat(createOccurrencePage.getDownloadDocumentButton()).isNotPresent();
    }
}
