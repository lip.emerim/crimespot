package felipe.emerim.crimespot.web.test.user;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.user.ChangePasswordPage;
import felipe.emerim.crimespot.web.page.user.EditUserPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangePasswordTest extends BaseFluentTest {
    @Page
    ChangePasswordPage changePasswordPage;

    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Page
    EditUserPage editUserPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testChangePassword() {
        loginPage.go();

        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();
        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");
        editUserPage.getChangePasswordButton().click();

        changePasswordPage.isAt();

        changePasswordPage
                .fillForm("user", "sadas", "sadas")
                .submitForm();

        editUserPage.isAt("1000");
        editUserPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("user.passwordChange.success"));

        editUserPage.closeToast(AlertTypes.SUCCESS.getDescription());

        editUserPage.logout();

        indexPage.clickLoginButton();

        loginPage.isAt();

        loginPage.fillAndSubmitForm("admin", "user");
        assertThat($(".alert")).hasSize(1);
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "sadas");
        indexPage.isAt();

        indexPage.verifyLoggedIn();

    }

    @RetryingTest(5)
    public void testChangePasswordInvalidOriginal() {
        loginPage.go();

        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();
        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");
        editUserPage.getChangePasswordButton().click();

        changePasswordPage.isAt();

        changePasswordPage
                .fillForm("reffus", "sadas", "sadas")
                .submitForm();

        changePasswordPage.isAt();
        changePasswordPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.passwordChange.invalidOriginal"));
    }

    @RetryingTest(5)
    public void testChangePasswordInvalidNew() {
        loginPage.go();

        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();
        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");
        editUserPage.getChangePasswordButton().click();

        changePasswordPage.isAt();

        changePasswordPage
                .fillForm("user", "sadas", "reffus")
                .submitForm();

        changePasswordPage.isAt();
        changePasswordPage.assertFormErrorCount(changePasswordPage.getForm(), 1);
    }
}
