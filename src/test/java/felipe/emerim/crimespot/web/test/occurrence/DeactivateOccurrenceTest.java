package felipe.emerim.crimespot.web.test.occurrence;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.ErrorPage;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.occurrence.ListOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ViewOccurrencePage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class DeactivateOccurrenceTest extends BaseFluentTest {

    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @Page
    ListOccurrencePage listOccurrencePage;

    @Page
    ViewOccurrencePage viewOccurrencePage;

    @Page
    ErrorPage errorPage;

    @RetryingTest(5)
    public void testDeactivateOccurrenceAdmin() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.verifyAnyContainTerm("Ocorrência de Roubo 6");

        listOccurrencePage.clickItemElement(1, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1065");

        viewOccurrencePage.getDeactivateOccurrenceButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getDeactivateOccurrenceModal());

        listOccurrencePage.isAt();

        listOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.deactivated")
        );

        listOccurrencePage.verifyNoneContainTerm("Ocorrência Roubo 6");

        viewOccurrencePage.go("1065");

        errorPage.checkErrorStatus("404 NOT_FOUND");
    }

    @RetryingTest(5)
    public void testDeactivateOccurrenceModerator() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.verifyAnyContainTerm("Ocorrência pendente");

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");

        assertThat(viewOccurrencePage.getDeactivateOccurrenceButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testDeactivateOccurrenceOwner() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.verifyAnyContainTerm("Ocorrência de Maus tratos a animais 4");

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1052");

        viewOccurrencePage.getDeactivateOccurrenceButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getDeactivateOccurrenceModal());

        listOccurrencePage.isAt();

        listOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.deactivated")
        );

        listOccurrencePage.verifyNoneContainTerm("Ocorrência de Maus tratos a animais 4");

        viewOccurrencePage.go("1052");

        errorPage.checkErrorStatus("404 NOT_FOUND");
    }
}
