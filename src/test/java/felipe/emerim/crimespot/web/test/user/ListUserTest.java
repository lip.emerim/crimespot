package felipe.emerim.crimespot.web.test.user;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.ErrorPage;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.user.ListUserPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class ListUserTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Page
    ListUserPage listUserPage;

    @Page
    ErrorPage errorPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testUserCannotSeeUserList() {
        loginPage.go();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");

        indexPage.isAt();

        assertThat(indexPage.getListUsersLink()).isNotPresent();

        listUserPage.go();

        errorPage.checkErrorStatus("403 FORBIDDEN");
    }

    @RetryingTest(5)
    public void testListUsers() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.verifyElementCount(8, ListUserPage.LIST_SELECTOR);
        listUserPage.verifyAnyContainTerm("admin");

        // Check admin cannot change other admins
        assertThat(listUserPage.$(ListUserPage.LIST_SELECTOR).get(0)
                .$(ListUserPage.TOGGLE_MODERATOR_SELECTOR)).hasSize(0);

        assertThat(listUserPage.$(ListUserPage.LIST_SELECTOR).get(0)
                .$(ListUserPage.TOGGLE_USER_SELECTOR)).hasSize(0);

        listUserPage.verifyElementCount(7, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.verifyElementCount(7, ListUserPage.TOGGLE_MODERATOR_SELECTOR);

        assertThat(listUserPage.getSearchTermField()).isPresent();
        assertThat(listUserPage.getRoleField()).isPresent();
        assertThat(listUserPage.getStatusField()).isPresent();
    }

    @RetryingTest(5)
    public void testListUsersModerator() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("moderator", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.verifyElementCount(8, ListUserPage.LIST_SELECTOR);

        listUserPage.verifyElementCount(8, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.verifyElementCount(0, ListUserPage.TOGGLE_MODERATOR_SELECTOR);

        assertThat(listUserPage.getSearchTermField()).isPresent();
        assertThat(listUserPage.getStatusField()).isPresent();
    }

    @RetryingTest(5)
    public void testListUserSearchTerm() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.getSearchTermField().fill().with("admin");
        listUserPage.getSearchButton().click();

        listUserPage.verifyElementCount(1, ListUserPage.LIST_SELECTOR);
        listUserPage.verifyAllContainTerm("admin");
        listUserPage.verifyElementCount(0, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.verifyElementCount(0, ListUserPage.TOGGLE_MODERATOR_SELECTOR);
    }

    @RetryingTest(5)
    public void testListUserProfileType() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.fillFieldUsingJavascript(
                "#" + listUserPage.getRoleField().id(),
                RoleEnum.ROLE_MODERATOR.name(),
                new String[]{"change"}
        );

        listUserPage.getSearchButton().click();

        listUserPage.verifyElementCount(1, ListUserPage.LIST_SELECTOR);
        listUserPage.verifyAllContainTerm(messages.get(RoleEnum.ROLE_MODERATOR.getDescription()));
        listUserPage.verifyElementCount(1, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.verifyElementCount(1, ListUserPage.TOGGLE_MODERATOR_SELECTOR);
    }

    @RetryingTest(5)
    public void testListUserStatus() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.fillFieldUsingJavascript(
                "#" + listUserPage.getStatusField().id(),
                "false",
                new String[]{"change"}
        );

        listUserPage.getSearchButton().click();

        listUserPage.verifyElementCount(1, ListUserPage.LIST_SELECTOR);
        listUserPage.verifyAllContainTerm("green.archer");
        listUserPage.verifyElementCount(1, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.verifyElementCount(1, ListUserPage.TOGGLE_MODERATOR_SELECTOR);
    }

    @RetryingTest(5)
    public void testListUserToggleModerator() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.getSearchTermField().fill().with("accelerator");
        listUserPage.getSearchButton().click();

        assertThat(listUserPage.isItemModerator(0)).isFalse();
        assertThat(listUserPage.isItemUser(0)).isTrue();

        listUserPage.clickItemElement(0, ListUserPage.TOGGLE_MODERATOR_SELECTOR);
        listUserPage.confirmModal(listUserPage.getModalConfirm());

        listUserPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("user.activate.moderator.success", "accelerator"));

        listUserPage.getSearchTermField().fill().with("accelerator");
        listUserPage.getSearchButton().click();

        assertThat(listUserPage.isItemModerator(0)).isTrue();
        assertThat(listUserPage.isItemUser(0)).isFalse();

        listUserPage.clickItemElement(0, ListUserPage.TOGGLE_MODERATOR_SELECTOR);
        listUserPage.confirmModal(listUserPage.getModalConfirm());

        listUserPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("user.deactivate.moderator.success", "accelerator"));

        listUserPage.getSearchTermField().fill().with("accelerator");
        listUserPage.getSearchButton().click();

        assertThat(listUserPage.isItemModerator(0)).isFalse();
        assertThat(listUserPage.isItemUser(0)).isTrue();

    }

    @RetryingTest(5)
    public void testListUserToggleUserAdmin() {
        this.testListToggleUser("admin", "user");
    }

    @RetryingTest(5)
    public void testListUserToggleUserModerator() {
        this.testListToggleUser("moderator", "user");
    }

    protected void testListToggleUser(String loginName, String loginPassword) {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait(loginName, loginPassword);

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.getSearchTermField().fill().with("accelerator");
        listUserPage.getSearchButton().click();

        assertThat(listUserPage.isItemActive(0)).isTrue();
        assertThat(listUserPage.isItemInactive(0)).isFalse();

        listUserPage.clickItemElement(0, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.confirmModal(listUserPage.getModalConfirm());

        listUserPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("user.deactivate.success", "accelerator"));

        listUserPage.getSearchTermField().fill().with("accelerator");

        listUserPage.fillFieldUsingJavascript(
                "#" + listUserPage.getStatusField().id(),
                "false",
                new String[]{"change"}
        );

        listUserPage.getSearchButton().click();

        assertThat(listUserPage.isItemActive(0)).isFalse();
        assertThat(listUserPage.isItemInactive(0)).isTrue();

        listUserPage.clickItemElement(0, ListUserPage.TOGGLE_USER_SELECTOR);
        listUserPage.confirmModal(listUserPage.getModalConfirm());

        listUserPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("user.activate.success", "accelerator"));

        listUserPage.getSearchTermField().fill().with("accelerator");

        listUserPage.fillFieldUsingJavascript(
                "#" + listUserPage.getStatusField().id(),
                "true",
                new String[]{"change"}
        );

        listUserPage.getSearchButton().click();

        assertThat(listUserPage.isItemActive(0)).isTrue();
        assertThat(listUserPage.isItemInactive(0)).isFalse();
    }

    @RetryingTest(5)
    public void testListNoResults() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        indexPage.isAt();

        indexPage.getListUsersLink().click();

        listUserPage.isAt();

        listUserPage.getSearchTermField().fill().with("sadas");
        listUserPage.getSearchButton().click();

        listUserPage.verifyElementCount(0, listUserPage.getListSelector());
    }
}
