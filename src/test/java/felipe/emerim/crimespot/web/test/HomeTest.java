package felipe.emerim.crimespot.web.test;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class HomeTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testListOccurrences() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapOccurrenceList()).displayed();

        indexPage.verifyOccurrenceCount(30);
        indexPage.verifyAnyContainTerm("Ocorrência de Roubo 1");
        indexPage.verifyAllContainTerm("Canoas");
    }

    @RetryingTest(5)
    public void testViewOccurrenceUsingList() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        String occurrence = IndexPage.LIST_ITEM_SELECTOR + "1010";

        await().atMost(15, TimeUnit.SECONDS).until($(occurrence)).displayed();

        $(occurrence).click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapCallout()).displayed();
        assertThat(indexPage.getMapCallout()).hasTextContaining("Ocorrência de Roubo 1");
        indexPage.verifyAllContainTerm("Canoas");
    }

    @RetryingTest(5)
    public void testListOccurrencesSearchTerm() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapOccurrenceList()).displayed();

        indexPage.getMapFilterButton().click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapFilters()).displayed();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getSearchTermField()).enabled();

        indexPage.getSearchTermField().fill().with("Roubo");
        indexPage.getSubmitSearchButton().click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapOccurrenceList()).displayed();

        indexPage.verifyOccurrenceCount(7);
        indexPage.verifyAllContainTerm("Roubo");
        indexPage.verifyAllContainTerm("Canoas");
    }

    @RetryingTest(5)
    public void testListOccurrencesByType() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapOccurrenceList()).displayed();

        indexPage.getMapFilterButton().click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapFilters()).displayed();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getOccurrenceTypeEnumsField()).enabled();

        indexPage.fillFieldUsingJavascript(
                "#" + indexPage.getOccurrenceTypeEnumsField().id(),
                OccurrenceTypeEnum.ROBBERY.name(),
                new String[]{"change"}
        );

        indexPage.getSubmitSearchButton().click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapFilters()).not().displayed();

        indexPage.verifyOccurrenceCount(4);
        indexPage.verifyAllContainTerm("Roubo");
        indexPage.verifyAllContainTerm("Canoas");
    }

    @RetryingTest(5)
    public void testListOccurrencesVerifiedOnly() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapOccurrenceList()).displayed();

        indexPage.getMapFilterButton().click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapFilters()).displayed();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getOnlyVerifiedField()).enabled();

        indexPage.getOnlyVerifiedField().click();

        indexPage.getSubmitSearchButton().click();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getMapFilters()).not().displayed();

        indexPage.verifyOccurrenceCount(16);
        indexPage.checkAllOccurrencesVerified();
        indexPage.verifyAllContainTerm("Canoas");
    }
}
