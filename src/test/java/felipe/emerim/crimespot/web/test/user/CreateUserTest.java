package felipe.emerim.crimespot.web.test.user;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.user.CreateUserPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateUserTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    CreateUserPage createUserPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testCreateUser() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();

        createUserPage.fillUserForm(
                "John Doe",
                "john.doe",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2",
                "password",
                "password"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();


        loginPage.isAt();
        loginPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(), messages.get("user.saved"));
        loginPage.fillAndSubmitFormAwait("john.doe", "password");
        indexPage.isAt();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getUserInfo()).present();
    }

    @RetryingTest(5)
    public void testCreateUserMissingRequiredFields() {
        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();
        createUserPage.fillUserForm(
                "",
                "",
                "",
                "",
                "",
                "3",
                "",
                ""
        ).fillAddressForm(
                "9999999",
                "",
                ""
        ).submitForm();

        createUserPage.isAt();
    }

    @RetryingTest(5)
    public void testCreateUserInvalidFields() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();

        createUserPage.fillUserForm(
                "J",
                "j",
                "a",
                "09/09/9999",
                "776190",
                "3",
                "password",
                "passworda"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        createUserPage.isAt();
        createUserPage.assertFormErrorCount(createUserPage.getForm(), 6);
    }

    @RetryingTest(5)
    public void testCreateUserDuplicateName() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();

        createUserPage.fillUserForm(
                "John Doe",
                "admin",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2",
                "password",
                "password"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        createUserPage.isAt();

        createUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.exist"));
    }

    @RetryingTest(5)
    public void testCreateUserDuplicateEmail() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();

        createUserPage.fillUserForm(
                "John Doe",
                "john.doe",
                "admin@gmail.com",
                "26/06/1998",
                "77619014063",
                "2",
                "password",
                "password"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        createUserPage.isAt();

        createUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.exist"));
    }

    @RetryingTest(5)
    public void testCreateUserDuplicateCpf() {
        this.mockGeocodeRequest();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();

        createUserPage.fillUserForm(
                "John Doe",
                "john.doe",
                "john.doe@gmail.com",
                "26/06/1998",
                "88547082085",
                "2",
                "password",
                "password"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        )
                .submitForm();

        createUserPage.isAt();

        createUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("user.exist"));
    }

    @RetryingTest(5)
    public void testCreateUserInvalidAddress() {

        this.mockGeocodeRequestNotFound();

        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.clickCreateUserLink();
        createUserPage.isAt();

        assertThat(createUserPage.getChangePasswordButton().present()).isFalse();
        assertThat(createUserPage.getDeactivateUserButton().present()).isFalse();

        createUserPage.fillUserForm(
                "John Doe",
                "john.doe",
                "john.doe@gmail.com",
                "26/06/1998",
                "77619014063",
                "2",
                "password",
                "password"
        ).fillAddressForm(
                "92032-230",
                "",
                "Casa"
        ).fillAddressFormWithoutPostalCode(
                "Brasil",
                "dasasdasdas",
                "dasdasdsa",
                "sadasdfsadsa",
                "sadasdasdasd",
                "599",
                "Cwerte"
        ).submitForm();

        createUserPage.isAt();
        createUserPage.verifyToastWithValue(AlertTypes.DANGER.getDescription(),
                messages.get("address.notFound"));
    }

    @RetryingTest(5)
    public void checkLoggedInUserCannotCreateUser() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        createUserPage.go();
        indexPage.isAt();
    }
}
