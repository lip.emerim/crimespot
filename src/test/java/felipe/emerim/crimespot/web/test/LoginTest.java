package felipe.emerim.crimespot.web.test;

import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginTest extends BaseFluentTest {

    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @RetryingTest(5)
    public void checkLoginSucceedModerator() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.$("#user-info")).present();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getListOccurrencesDropdown()).present();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getListUsersLink()).present();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getListOccurrencesLink()).not().present();
    }

    @RetryingTest(5)
    public void checkLoginSucceedUser() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.$("#user-info")).present();

        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getListOccurrencesLink()).present();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getListUsersLink()).not().present();
        await().atMost(15, TimeUnit.SECONDS).until(indexPage.getListOccurrencesDropdown()).not().present();
    }

    @RetryingTest(5)
    public void checkLoginFailed() {
        loginPage.go();
        loginPage.fillAndSubmitForm("wrongUser", "wrongPass");
        assertThat($(".alert")).hasSize(1);
        loginPage.isAt();
    }

    @RetryingTest(5)
    public void checkLogoutSucceed() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        super.logout();
        indexPage.isAt();

        assertThat($("#user-info").present()).isFalse();

    }

    @RetryingTest(5)
    public void checkLoggedInUserCannotAccessLogin() {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        loginPage.go();
        indexPage.isAt();
    }
}
