package felipe.emerim.crimespot.web.test.user;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.user.EditUserPage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class DeactivateUserTest extends BaseFluentTest {
    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Page
    EditUserPage editUserPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testDeactivateUser() {
        loginPage.go();

        loginPage.fillAndSubmitFormAwait("moderator", "user");

        indexPage.isAt();
        indexPage.goToEditUserPage();

        editUserPage.isAt("1001");
        editUserPage.getDeactivateUserButton().click();

        editUserPage.confirmModal(editUserPage.getDeleteModal());

        loginPage.isAt();
        loginPage.verifyToastWithValue(AlertTypes.SUCCESS.getDescription(),
                messages.get("user.deactivate.success", "moderator"));

        loginPage.closeToast(AlertTypes.SUCCESS.getDescription());

        loginPage.fillAndSubmitForm("moderator", "user");
        assertThat($(".alert")).hasSize(1);
        loginPage.isAt();
    }

    @RetryingTest(5)
    public void testAdminCannotDeactivateProfile() {
        indexPage.go();
        assertThat(indexPage.getUserInfo().present()).isFalse();
        indexPage.clickLoginButton();
        loginPage.isAt();
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.goToEditUserPage();

        editUserPage.isAt("1000");
        assertThat(editUserPage.getDeactivateUserButton().present()).isFalse();
    }
}
