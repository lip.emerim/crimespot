package felipe.emerim.crimespot.web.test.occurrence;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.ErrorPage;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.occurrence.EditOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ListOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ViewOccurrencePage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import java.io.IOException;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class ViewOccurrenceTest extends BaseFluentTest {

    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @Page
    ListOccurrencePage listOccurrencePage;

    @Page
    EditOccurrencePage editOccurrencePage;

    @Page
    ViewOccurrencePage viewOccurrencePage;

    @Page
    ErrorPage errorPage;

    @RetryingTest(5)
    public void testViewOccurrenceModerator() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");

        assertThat(viewOccurrencePage.getOccurrenceTitleField())
                .hasTextContaining("Ocorrência pendente");
        assertThat(viewOccurrencePage.getOccurrenceDescriptionField()).hasTextContaining(
                "Descrição da ocorrência pendente"
        );
        assertThat(viewOccurrencePage.getOccurrenceTypeField()).hasTextContaining(
                messages.get(OccurrenceTypeEnum.ROBBERY.getDescription())
        );
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.WAITING_ANALYSIS.getDescription())
        );

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceUserLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceUserLink().attribute("href")).contains(
                "/user/list?searchTerm=88547082085"
        );
        assertThat(viewOccurrencePage.getOccurrenceModerator()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasText("");

        assertThat(viewOccurrencePage.getPostalCodeField()).hasTextContaining("90220-021");
        assertThat(viewOccurrencePage.getCountryField()).hasTextContaining("Brasil");
        assertThat(viewOccurrencePage.getStateField()).hasTextContaining("Rio Grande do Sul");
        assertThat(viewOccurrencePage.getCityField()).hasTextContaining("Porto Alegre");
        assertThat(viewOccurrencePage.getNeighborhoodField()).hasTextContaining("Floresta");
        assertThat(viewOccurrencePage.getStreetField()).hasTextContaining("Rua Almirante Barroso");
        assertThat(viewOccurrencePage.getNumberField()).hasTextContaining("735");
        assertThat(viewOccurrencePage.getComplementField()).hasTextContaining("Sala 799");

        assertThat(viewOccurrencePage.getFormattedAddressField()).hasTextContaining(
                "R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil"
        );
    }

    @RetryingTest(5)
    public void testViewOccurrenceUser() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1052");

        assertThat(viewOccurrencePage.getOccurrenceTitleField())
                .hasTextContaining("Ocorrência de Maus tratos a animais 4");
        assertThat(viewOccurrencePage.getOccurrenceDescriptionField()).hasTextContaining(
                "Descrição da ocorrência de Maus tratos a animais 4"
        );
        assertThat(viewOccurrencePage.getOccurrenceTypeField()).hasTextContaining(
                messages.get(OccurrenceTypeEnum.ANIMAL_ABUSE.getDescription())
        );
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );

        assertThat(viewOccurrencePage.getModeratorComments()).hasTextContaining(
                "Comentários da ocorrência de Maus tratos a animais 4"
        );
        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceUser()).hasTextContaining("accelerator");
        assertThat(viewOccurrencePage.getOccurrenceUserLink()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceModerator()).hasTextContaining(
                "moderator"
        );
        assertThat(viewOccurrencePage.getOccurrenceModeratorLink()).isNotPresent();

        assertThat(viewOccurrencePage.getPostalCodeField()).hasTextContaining("92020-510");
        assertThat(viewOccurrencePage.getCountryField()).hasTextContaining("Brasil");
        assertThat(viewOccurrencePage.getStateField()).hasTextContaining("Rio Grande do Sul");
        assertThat(viewOccurrencePage.getCityField()).hasTextContaining("Canoas");
        assertThat(viewOccurrencePage.getStreetField()).hasTextContaining("Rua Aurora");
        assertThat(viewOccurrencePage.getNumberField()).hasText("");
        assertThat(viewOccurrencePage.getComplementField()).hasText("");

        assertThat(viewOccurrencePage.getFormattedAddressField()).hasTextContaining(
                "R. Aurora, 850-834 - Mal. Rondon, Canoas - RS, 92020-510"
        );
    }

    @RetryingTest(5)
    public void testViewOccurrenceUserNotOwner() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();

        viewOccurrencePage.go("1000");

        errorPage.checkErrorStatus("403 FORBIDDEN");
    }

    @RetryingTest(5)
    public void testViewOccurrenceWithDocumentOwner() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.PNG_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillDocumentField(path);

        editOccurrencePage.submitForm();

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).hasTextContaining(
                "Arquivo do BO"
        );

        assertThat(viewOccurrencePage.getOccurrenceDocumentField().attribute("href")).contains(
                "/occurrence/1000/document/download/1"
        );

        assertThat(viewOccurrencePage.getRemoveDocumentButton()).isPresent();
    }

    @RetryingTest(5)
    public void testViewOccurrenceWithDocumentModerator() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");
        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.PNG_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillDocumentField(path);

        editOccurrencePage.submitForm();

        viewOccurrencePage.logout();

        indexPage.isAt();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();

        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);
        viewOccurrencePage.isAt("1000");

        assertThat(viewOccurrencePage.getOccurrenceDocumentField()).hasTextContaining(
                "Arquivo do BO"
        );

        assertThat(viewOccurrencePage.getOccurrenceDocumentField().attribute("href")).contains(
                "/occurrence/1000/document/download/1"
        );

        assertThat(viewOccurrencePage.getRemoveDocumentButton()).isNotPresent();
    }
}
