package felipe.emerim.crimespot.web.test.occurrence;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.occurrence.CreateOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.EditOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ListOccurrencePage;
import felipe.emerim.crimespot.web.page.occurrence.ViewOccurrencePage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import java.io.IOException;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class ReviewOccurrenceTest extends BaseFluentTest {

    @Page
    LoginPage loginPage;

    @Page
    IndexPage indexPage;

    @Autowired
    Messages messages;

    @Page
    ListOccurrencePage listOccurrencePage;

    @Page
    CreateOccurrencePage createOccurrencePage;

    @Page
    EditOccurrencePage editOccurrencePage;

    @Page
    ViewOccurrencePage viewOccurrencePage;

    @RetryingTest(5)
    public void testStartReviewRegularUser() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.getCreateOccurrenceButton().click();

        Resource resource = FileHelper.loadFile(FileHelper.PDF_PATH);
        String path = resource.getFile().getAbsolutePath();

        createOccurrencePage.fillForm(
                "cool occurrence",
                "cool occurrence description",
                "12/10/2020",
                "6", //id of type
                path
        ).fillAddressForm(
                "92032-360",
                "",
                "Casa"
        ).submitForm();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt();

        assertThat(viewOccurrencePage.getOccurrenceTitleField()).hasTextContaining("cool occurrence");
        assertThat(viewOccurrencePage.getStartReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testStartReview() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1000");

        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isPresent();

        viewOccurrencePage.getStartReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getStartReviewModal());

        viewOccurrencePage.isAt();
        viewOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.startReview.success")
        );

        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.IN_ANALYSIS.getDescription())
        );

        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isPresent();
    }

    @RetryingTest(5)
    public void testStartReviewNotPending() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(1, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1065");

        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasNotTextContaining(
                messages.get(OccurrenceStatusEnum.WAITING_ANALYSIS.getDescription())
        );

        assertThat(viewOccurrencePage.getStartReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testContinueReview() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickInAnalysisLink();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isTrue();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR);

        viewOccurrencePage.isAt("1001");

        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.IN_ANALYSIS.getDescription())
        );
        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isPresent();


    }

    @RetryingTest(5)
    public void testContinueReviewDifferentUser() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getStatusField().id(),
                OccurrenceStatusEnum.IN_ANALYSIS.name(),
                new String[]{"change"}
        );

        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isFalse();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1001");

        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.IN_ANALYSIS.getDescription())
        );
        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testFinishReviewNoComments() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickInAnalysisLink();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isTrue();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR);

        viewOccurrencePage.isAt("1001");

        viewOccurrencePage.getApproveReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getApproveOccurrenceModal());

        viewOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.approved")
        );

        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );
        assertThat(viewOccurrencePage.getModeratorComments()).hasText("");

    }

    @RetryingTest(5)
    public void testApproveOccurrence() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickInAnalysisLink();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isTrue();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR);

        viewOccurrencePage.isAt("1001");

        viewOccurrencePage.getModeratorCommentsField().fill().withText("I approve this occurrence");
        viewOccurrencePage.getApproveReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getApproveOccurrenceModal());

        viewOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.approved")
        );

        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );
        assertThat(viewOccurrencePage.getModeratorComments()).hasTextContaining(
                "I approve this occurrence"
        );

        assertThat(viewOccurrencePage.getOccurrenceVerifiedYes()).isNotPresent();

        assertThat(viewOccurrencePage.getOccurrenceVerifiedNo()).hasTextContaining(
                messages.get("occurrence.verified.no")
        );
    }

    @RetryingTest(5)
    public void testApproveOccurrenceWithDocument() throws IOException {
        this.mockGeocodeRequest();

        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        listOccurrencePage.go();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1000");

        viewOccurrencePage.getEditOccurrenceButton().click();

        editOccurrencePage.isAt("1000");

        Resource resource = FileHelper.loadFile(FileHelper.PNG_PATH);
        String path = resource.getFile().getAbsolutePath();

        editOccurrencePage.fillDocumentField(path);

        editOccurrencePage.submitForm();

        viewOccurrencePage.isAt("1000");

        viewOccurrencePage.getStartReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getStartReviewModal());

        viewOccurrencePage.isAt();

        viewOccurrencePage.getModeratorCommentsField().fill().withText("I approve this occurrence");
        viewOccurrencePage.getApproveReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getApproveOccurrenceModal());

        viewOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.approved")
        );

        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );
        assertThat(viewOccurrencePage.getModeratorComments()).hasTextContaining(
                "I approve this occurrence"
        );

        assertThat(viewOccurrencePage.getOccurrenceVerifiedNo()).isNotPresent();

        assertThat(viewOccurrencePage.getOccurrenceVerifiedYes()).hasTextContaining(
                messages.get("occurrence.verified.yes")
        );
    }

    @RetryingTest(5)
    public void testRejectOccurrence() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickInAnalysisLink();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isTrue();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR);

        viewOccurrencePage.isAt("1001");

        viewOccurrencePage.getModeratorCommentsField().fill().withText("I reject this occurrence");
        viewOccurrencePage.getRejectReviewButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getRejectOccurrenceModal());

        viewOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.rejected")
        );

        assertThat(viewOccurrencePage.getEditOccurrenceButton()).isNotPresent();
        assertThat(viewOccurrencePage.getModeratorCommentsField()).isNotPresent();
        assertThat(viewOccurrencePage.getApproveReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getRejectReviewButton()).isNotPresent();
        assertThat(viewOccurrencePage.getOccurrenceModeratorLinkText()).hasTextContaining("admin");
        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.REJECTED.getDescription())
        );
        assertThat(viewOccurrencePage.getModeratorComments()).hasTextContaining(
                "I reject this occurrence"
        );
    }

    @RetryingTest(5)
    public void testRevokeApprovalAdmin() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.clickItemElement(1, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1065");

        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );

        viewOccurrencePage.getRevokeApprovalButton().click();
        viewOccurrencePage.confirmModal(viewOccurrencePage.getRevokeApprovalModal());

        viewOccurrencePage.verifyToastWithValue(
                AlertTypes.SUCCESS.getDescription(),
                messages.get("occurrence.revokedApproval")
        );

        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.REJECTED_BY_ADMIN.getDescription())
        );
    }

    @RetryingTest(5)
    public void testRevokeApprovalModerator() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_SELECTOR).present()).isTrue();

        listOccurrencePage.clickItemElement(1, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1065");

        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );

        assertThat(viewOccurrencePage.getRevokeApprovalButton()).isNotPresent();
    }

    @RetryingTest(5)
    public void testRevokeApprovalUser() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_SELECTOR).present()).isTrue();

        listOccurrencePage.clickItemElement(0, ListOccurrencePage.DETAIL_BUTTON_SELECTOR);

        viewOccurrencePage.isAt("1052");

        assertThat(viewOccurrencePage.getOccurrenceStatusField()).hasTextContaining(
                messages.get(OccurrenceStatusEnum.APPROVED.getDescription())
        );

        assertThat(viewOccurrencePage.getRevokeApprovalButton()).isNotPresent();
    }
}
