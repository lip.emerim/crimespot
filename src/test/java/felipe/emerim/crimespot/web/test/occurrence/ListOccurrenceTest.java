package felipe.emerim.crimespot.web.test.occurrence;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.enums.OccurrenceSortEnum;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import felipe.emerim.crimespot.web.config.BaseFluentTest;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import felipe.emerim.crimespot.web.page.occurrence.ListOccurrencePage;
import org.fluentlenium.core.annotation.Page;
import org.junitpioneer.jupiter.RetryingTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class ListOccurrenceTest extends BaseFluentTest {
    @Page
    IndexPage indexPage;

    @Page
    ListOccurrencePage listOccurrencePage;

    @Page
    LoginPage loginPage;

    @Autowired
    Messages messages;

    @RetryingTest(5)
    public void testListOccurrenceModerator() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.verifyElementCount(8, listOccurrencePage.getListSelector());
        assertThat($(ListOccurrencePage.DETAIL_BUTTON_SELECTOR).present()).isTrue();

        assertThat($(listOccurrencePage.getListSelector()).get(0).find("span"))
                .hasTextContaining("Ocorrência pendente");
    }

    @RetryingTest(5)
    public void testListOccurrenceModeratorSearchTerm() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.getSearchTermField().fill().with("carina.emerim");
        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(4, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm("carina.emerim");
    }

    @RetryingTest(5)
    public void testListOccurrenceModeratorStatus() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getStatusField().id(),
                OccurrenceStatusEnum.IN_ANALYSIS.name(),
                new String[]{"change"}
        );

        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(1, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm(messages.get(OccurrenceStatusEnum.IN_ANALYSIS.getDescription()));
    }

    @RetryingTest(5)
    public void testListOccurrenceModeratorOrder() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getSortField().id(),
                OccurrenceSortEnum.LEAST_RECENT.name(),
                new String[]{"change"}
        );
        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(8, listOccurrencePage.getListSelector());
        assertThat($(listOccurrencePage.getListSelector()).get(0).find("span"))
                .hasTextContaining("Ocorrência denunciada");

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getSortField().id(),
                OccurrenceSortEnum.NEAREST.name(),
                new String[]{"change"}
        );
        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(8, listOccurrencePage.getListSelector());
        assertThat($(listOccurrencePage.getListSelector()).get(0).find("span"))
                .hasTextContaining("Ocorrência de Vandalismo 5");
    }

    @RetryingTest(5)
    public void testListOccurrenceModeratorType() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getTypesField().id(),
                OccurrenceTypeEnum.ANIMAL_ABUSE.name(),
                new String[]{"change"}
        );

        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(5, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm(messages.get(OccurrenceTypeEnum.ANIMAL_ABUSE.getDescription()));

    }

    @RetryingTest(5)
    public void testListOccurrenceModeratorOnlyRelatedToMe() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("moderator", "user");
        indexPage.isAt();
        indexPage.clickAllOccurrencesLink();

        listOccurrencePage.isAt();

        listOccurrencePage.getOnlyRelatedToMeField().click();
        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(8, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm("moderator");
    }

    @RetryingTest(5)
    public void testListOccurrenceModeratorContinueAnalysis() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();
        indexPage.clickInAnalysisLink();

        listOccurrencePage.isAt();

        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isTrue();
    }

    @RetryingTest(5)
    public void testInAnalysisLink() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.checkInAnalysisCounter(1);

        indexPage.clickInAnalysisLink();

        listOccurrencePage.isAt();

        listOccurrencePage.verifyElementCount(1, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm(messages.get(OccurrenceStatusEnum.IN_ANALYSIS.getDescription()));
        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isTrue();
    }

    @RetryingTest(5)
    public void testPendingLink() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.checkPendingCounter(1);
        indexPage.clickPendingLink();

        listOccurrencePage.isAt();

        listOccurrencePage.verifyElementCount(1, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm(messages.get(OccurrenceStatusEnum.WAITING_ANALYSIS.getDescription()));
        assertThat($(ListOccurrencePage.DETAIL_BUTTON_IN_REVIEW_SELECTOR).present()).isFalse();
        assertThat($(ListOccurrencePage.DETAIL_BUTTON_SELECTOR).present()).isTrue();
    }


    @RetryingTest(5)
    public void testReportedLink() {
        loginPage.go();
        loginPage.isAt();

        // admin is also a moderator
        loginPage.fillAndSubmitFormAwait("admin", "user");
        indexPage.isAt();

        indexPage.checkReportedCounter(3);
        indexPage.clickReportedLink();

        listOccurrencePage.isAt();

        assertThat(listOccurrencePage.getSortField().value()).isEqualTo(OccurrenceSortEnum.MOST_REPORTED.name());
    }

    @RetryingTest(5)
    public void testListOccurrenceUser() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();

        assertThat(indexPage.getInAnalysisOccurrencesLink()).isNotPresent();
        assertThat(indexPage.getPendingOccurrencesLink()).isNotPresent();

        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        assertThat(listOccurrencePage.getStatusField()).isPresent();
        assertThat(listOccurrencePage.getSortField()).isPresent();
        assertThat(listOccurrencePage.getOnlyRelatedToMeField()).isNotPresent();
        assertThat(listOccurrencePage.getTypesField()).isPresent();
        assertThat(listOccurrencePage.getSearchTermField()).isPresent();

        listOccurrencePage.verifyAllContainTerm("accelerator");
        listOccurrencePage.verifyElementCount(4, listOccurrencePage.getListSelector());

    }

    @RetryingTest(5)
    public void testListOccurrenceUserSearchTerm() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.getSearchTermField().fill().with("Armas");
        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyAllContainTerm("accelerator");
        listOccurrencePage.verifyAllContainTerm("Armas");
        listOccurrencePage.verifyElementCount(2, listOccurrencePage.getListSelector());
    }

    @RetryingTest(5)
    public void testListOccurrenceUserStatus() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("draculina", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getStatusField().id(),
                OccurrenceStatusEnum.APPROVED.name(),
                new String[]{"change"}
        );

        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(1, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm(messages.get(OccurrenceStatusEnum.APPROVED.getDescription()));
    }

    @RetryingTest(5)
    public void testListOccurrenceUserType() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getTypesField().id(),
                OccurrenceTypeEnum.ANIMAL_ABUSE.name(),
                new String[]{"change"}
        );

        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(2, listOccurrencePage.getListSelector());
        listOccurrencePage.verifyAllContainTerm(messages.get(OccurrenceTypeEnum.ANIMAL_ABUSE.getDescription()));
    }

    @RetryingTest(5)
    public void testListOccurrenceUserOrder() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.fillFieldUsingJavascript(
                "#" + listOccurrencePage.getSortField().id(),
                OccurrenceSortEnum.NEAREST.name(),
                new String[]{"change"}
        );
        listOccurrencePage.getSearchButton().click();

        listOccurrencePage.verifyElementCount(4, listOccurrencePage.getListSelector());
        assertThat($(listOccurrencePage.getListSelector()).get(0).find("span"))
                .hasTextContaining("Ocorrência de Armas 4");
    }

    @RetryingTest(5)
    public void testNonLoggedInUserCannotListOccurrences() {
        indexPage.go();
        indexPage.isAt();
        assertThat(indexPage.getListOccurrencesLink()).isNotPresent();
        assertThat(indexPage.getInAnalysisOccurrencesLink()).isNotPresent();
        assertThat(indexPage.getPendingOccurrencesLink()).isNotPresent();

        listOccurrencePage.go();
        loginPage.isAt();
    }

    @RetryingTest(5)
    public void testListOccurrencesNoResult() {
        loginPage.go();
        loginPage.isAt();

        loginPage.fillAndSubmitFormAwait("accelerator", "user");
        indexPage.isAt();
        indexPage.getListOccurrencesLink().click();

        listOccurrencePage.isAt();

        listOccurrencePage.getSearchTermField().fill().with("sadas");
        listOccurrencePage.getSearchButton().click();


        listOccurrencePage.verifyElementCount(0, listOccurrencePage.getListSelector());

        assertThat(listOccurrencePage.getNoResultsContainer()).isPresent();
    }
}
