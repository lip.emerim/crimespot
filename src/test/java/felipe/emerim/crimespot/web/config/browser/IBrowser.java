package felipe.emerim.crimespot.web.config.browser;

import felipe.emerim.crimespot.web.config.ConfigException;
import org.openqa.selenium.Capabilities;

import java.util.Map;

public interface IBrowser {

    Chrome chrome = new Chrome();
    Firefox firefox = new Firefox();

    Map<String, IBrowser> browsers = Map.ofEntries(
            Map.entry("chrome", chrome),
            Map.entry("firefox", firefox)
    );

    static IBrowser getBrowser(String browserName) {
        return browsers.getOrDefault(browserName, chrome);
    }

    Capabilities getCapabilities();

    default String getDriverExecutableName() {
        throw new ConfigException("Not supported");
    }

    default String getDriverSystemPropertyName() {
        throw new ConfigException("Not supported");
    }

    default void manage() {
        throw new ConfigException("Not supported");
    }

    void setHeadless(boolean headless);
}
