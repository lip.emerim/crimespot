package felipe.emerim.crimespot.web.config;

import felipe.emerim.crimespot.domain.GeocodingAddress;
import felipe.emerim.crimespot.service.GeocodingService;
import felipe.emerim.crimespot.web.config.browser.IBrowser;
import felipe.emerim.crimespot.web.page.IndexPage;
import felipe.emerim.crimespot.web.page.LoginPage;
import org.fluentlenium.adapter.junit.jupiter.FluentTest;
import org.fluentlenium.configuration.ConfigurationProperties;
import org.fluentlenium.configuration.FluentConfiguration;
import org.fluentlenium.core.annotation.Page;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.MalformedURLException;
import java.net.URL;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@FluentConfiguration(
        htmlDumpMode = ConfigurationProperties.TriggerMode.AUTOMATIC_ON_FAIL,
        htmlDumpPath = "test_html_dumps",
        screenshotMode = ConfigurationProperties.TriggerMode.AUTOMATIC_ON_FAIL,
        screenshotPath = "test_screenshots"
)
public abstract class BaseFluentTest extends FluentTest {

    private static final Logger log = LoggerFactory.getLogger(BaseFluentTest.class);
    @LocalServerPort
    protected int port;
    @Page
    protected LoginPage loginPage;
    @Page
    protected IndexPage indexPage;

    @MockBean
    protected GeocodingService geocodingService;

    @BeforeAll
    public static void setupClass() {
        if (!SeleniumBrowserConfigProperties.useHub()) {
            getBrowser().manage();
        }
    }

    public void mockGeocodeRequest() {
        when(geocodingService.geocode(any(String.class))).thenReturn(
                GeocodingAddress
                        .builder()
                        .city("Canoas")
                        .country("Brasil")
                        .state("Rio Grande do Sul")
                        .latitude(-29.906377999181206)
                        .longitude(-51.144211644250575)
                        .formattedAddress("R. Maristela - Estância Velha, Canoas - RS")
                        .neighborhood("Estância Velha")
                        .number("")
                        .street("Rua Maristela")
                        .postalCode("92032-230")
                        .build()
        );
    }

    public void mockGeocodeRequestNotFound() {
        when(geocodingService.geocode(any(String.class))).thenThrow(
                IllegalArgumentException.class
        );
    }

    @Override
    public WebDriver newWebDriver() {

        WebDriver webDriver = this.createWebDriver();
        webDriver.manage().window().setSize(new Dimension(1920, 1080));

        return webDriver;
    }

    private WebDriver createWebDriver() {
        if (SeleniumBrowserConfigProperties.useHub()) {
            // Don't log your Grid url because you may expose your SauceLabs/BrowserStack API key :)
            log.info(() -> "Running test on Grid using " + SeleniumBrowserConfigProperties.getBrowserName());
            return runRemoteWebDriver();
        }
        log.info(() -> "Running test locally using " + SeleniumBrowserConfigProperties.getBrowserName());
        return super.newWebDriver();
    }

    private WebDriver runRemoteWebDriver() {
        try {
            return new Augmenter().augment(
                    new RemoteWebDriver(new URL(getRemoteUrl()), getBrowser().getCapabilities()));
        } catch (MalformedURLException e) {
            throw new ConfigException("Invalid hub location: " + getRemoteUrl(), e);
        }
    }

    @Override
    public String getWebDriver() {
        return SeleniumBrowserConfigProperties.getBrowserName();
    }

    @Override
    public String getRemoteUrl() {
        return SeleniumBrowserConfigProperties.getGridUrl();
    }

    @Override
    public Capabilities getCapabilities() {
        return getBrowser().getCapabilities();
    }

    @Override
    public String getBaseUrl() {
        return SeleniumBrowserConfigProperties.getPageUrl(port);
    }

    private static IBrowser getBrowser() {
        IBrowser browser = IBrowser.getBrowser(SeleniumBrowserConfigProperties.getBrowserName());
        browser.setHeadless(SeleniumBrowserConfigProperties.isHeadless());
        return IBrowser.getBrowser(SeleniumBrowserConfigProperties.getBrowserName());
    }

    public void loginAdmin() {
        doLogin("admin", "user");
    }

    public void logout() {
        indexPage.logout();
    }

    private void doLogin(String name, String password) {
        loginPage.go();
        loginPage.fillAndSubmitFormAwait(name, password)
                .awaitUntilFormDisappear();
        indexPage.isAt();
    }

}
