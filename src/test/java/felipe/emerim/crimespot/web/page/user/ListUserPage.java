package felipe.emerim.crimespot.web.page.user;

import felipe.emerim.crimespot.web.page.ListPage;
import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;


@PageUrl("/user/list")
@Getter
public class ListUserPage extends ListPage<ListUserPage> {
    @FindBy(id = "searchTerm")
    protected FluentWebElement searchTermField;

    @FindBy(id = "role")
    protected FluentWebElement roleField;

    @FindBy(id = "active")
    protected FluentWebElement statusField;

    @FindBy(id = "search-button")
    protected FluentWebElement searchButton;

    protected String modalConfirmSelector = "#modal-confirm";

    @FindBy(id = "no-results-container")
    protected FluentWebElement noResultsContainer;

    public static String LIST_SELECTOR = ".callout-secondary";
    public static String TOGGLE_MODERATOR_SELECTOR = ".btn-toggle-moderator.btn-dark";
    public static String TOGGLE_USER_SELECTOR = ".btn-toggle-user.btn-danger";

    public boolean isItemModerator(int position) {
        FluentWebElement item = $(LIST_SELECTOR).get(0);
        return item.$(TOGGLE_MODERATOR_SELECTOR + " .fa-handshake-slash").present();
    }

    public boolean isItemActive(int position) {
        FluentWebElement item = $(LIST_SELECTOR).get(0);
        return item.$(TOGGLE_USER_SELECTOR + " .fa-lock").present();
    }

    public boolean isItemUser(int position) {
        FluentWebElement item = $(LIST_SELECTOR).get(0);
        return item.$(TOGGLE_MODERATOR_SELECTOR + " .fa-handshake").present();
    }

    public boolean isItemInactive(int position) {
        FluentWebElement item = $(LIST_SELECTOR).get(0);
        return item.$(TOGGLE_USER_SELECTOR + " .fa-lock-open").present();
    }

    public FluentWebElement getModalConfirm() {
        return el(this.modalConfirmSelector);
    }

    @Override
    public String getListSelector() {
        return LIST_SELECTOR;
    }
}
