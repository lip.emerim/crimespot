package felipe.emerim.crimespot.web.page.occurrence;

import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;

@PageUrl("/occurrence/edit/{occurrenceId}")
@Getter
public class EditOccurrencePage extends CreateOccurrencePage {
}
