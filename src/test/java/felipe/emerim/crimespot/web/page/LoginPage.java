package felipe.emerim.crimespot.web.page;

import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

@PageUrl("/login")
public class LoginPage extends GenericPage {

    private static final String LOGIN_FORM = "#login-user-form";

    @FindBy(id = "submit-button")
    private FluentWebElement submitButton;

    @FindBy(id = "create-user-link")
    private FluentWebElement createUserLink;

    public LoginPage fillAndSubmitFormAwait(String... paramsOrdered) {
        /*
         * 2020-10-02: Firefox currently has a bug on selenium grid where it cannot
         * properly update cookie values. As a result, even after a successful login
         * the user would appear as non logged in, because the session cookie was not
         * properly updated. Here we unset the session cookie before each login attempt
         * so that firefox will not have to update the cookie but instead, always add
         * a new one.
         */
        this.deleteCookieByName("JSESSIONID");
        this.fillAndSubmitForm(paramsOrdered);
        this.awaitUntilFormDisappear();
        return this;
    }

    public LoginPage fillAndSubmitForm(String... paramsOrdered) {
        $("input").fill().with(paramsOrdered);
        $("#submit-button").submit();
        return this;
    }

    public LoginPage awaitUntilFormDisappear() {
        await().atMost(15, TimeUnit.SECONDS).until(el(LOGIN_FORM)).not().present();
        return this;
    }

    public LoginPage clickCreateUserLink() {
        createUserLink.click();
        return this;
    }

}
