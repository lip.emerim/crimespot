package felipe.emerim.crimespot.web.page.user;

import felipe.emerim.crimespot.web.page.GenericPage;
import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@Getter
@PageUrl("/user/change-password")
public class ChangePasswordPage extends GenericPage {

    @FindBy(id = "change-password-form")
    protected FluentWebElement form;

    @FindBy(id = "original-password")
    protected FluentWebElement originalPasswordField;

    @FindBy(id = "new-password")
    protected FluentWebElement newPasswordField;

    @FindBy(id = "new-password-confirm")
    protected FluentWebElement newPasswordConfirmField;

    @FindBy(id = "submit-button")
    protected FluentWebElement submitButton;

    public ChangePasswordPage fillForm(
            String originalPassword,
            String newPassword,
            String newPasswordConfirm
    ) {
        this.originalPasswordField.fill().with(originalPassword);
        this.newPasswordField.fill().with(newPassword);
        this.newPasswordConfirmField.fill().with(newPasswordConfirm);

        return this;
    }

    public ChangePasswordPage submitForm() {
        this.submitButton.click();
        return this;
    }
}
