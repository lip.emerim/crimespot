package felipe.emerim.crimespot.web.page.occurrence;

import felipe.emerim.crimespot.web.page.GenericPage;
import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@PageUrl("/occurrence/view/{occurrenceId}")
@Getter
public class ViewOccurrencePage extends GenericPage {
    @FindBy(id = "start-review-button")
    protected FluentWebElement startReviewButton;

    @FindBy(id = "edit-occurrence-button")
    protected FluentWebElement editOccurrenceButton;

    @FindBy(id = "deactivate-occurrence-button")
    protected FluentWebElement deactivateOccurrenceButton;

    @FindBy(id = "revoke-approval-button")
    protected FluentWebElement revokeApprovalButton;

    @FindBy(id = "occurrence-title")
    protected FluentWebElement occurrenceTitleField;

    @FindBy(id = "occurrence-description")
    protected FluentWebElement occurrenceDescriptionField;

    @FindBy(id = "occurrence-type")
    protected FluentWebElement occurrenceTypeField;

    @FindBy(id = "occurrence-status")
    protected FluentWebElement occurrenceStatusField;

    @FindBy(id = "remove-document-button")
    protected FluentWebElement removeDocumentButton;

    @FindBy(id = "occurrence-document")
    protected FluentWebElement occurrenceDocumentField;

    @FindBy(id = "occurrence-user-link")
    protected FluentWebElement occurrenceUserLink;

    @FindBy(id = "occurrence-user-link-text")
    protected FluentWebElement occurrenceUserLinkText;

    @FindBy(id = "occurrence-user")
    protected FluentWebElement occurrenceUser;

    @FindBy(id = "occurrence-moderator-link")
    protected FluentWebElement occurrenceModeratorLink;

    @FindBy(id = "occurrence-moderator-link-text")
    protected FluentWebElement occurrenceModeratorLinkText;

    @FindBy(id = "occurrence-moderator")
    protected FluentWebElement occurrenceModerator;

    @FindBy(id = "occurrence-verified-yes")
    protected FluentWebElement occurrenceVerifiedYes;

    @FindBy(id = "occurrence-verified-no")
    protected FluentWebElement occurrenceVerifiedNo;

    @FindBy(id = "moderator-comments")
    protected FluentWebElement moderatorComments;

    @FindBy(id = "review-form")
    protected FluentWebElement reviewForm;

    @FindBy(id = "moderatorComments")
    protected FluentWebElement moderatorCommentsField;

    @FindBy(id = "address-postal-code")
    protected FluentWebElement postalCodeField;

    @FindBy(id = "address-country")
    protected FluentWebElement countryField;

    @FindBy(id = "address-state")
    protected FluentWebElement stateField;

    @FindBy(id = "address-city")
    protected FluentWebElement cityField;

    @FindBy(id = "address-neighborhood")
    protected FluentWebElement neighborhoodField;

    @FindBy(id = "address-street")
    protected FluentWebElement streetField;

    @FindBy(id = "address-number")
    protected FluentWebElement numberField;

    @FindBy(id = "address-complement")
    protected FluentWebElement complementField;

    @FindBy(id = "address-formatted-address")
    protected FluentWebElement formattedAddressField;

    @FindBy(id = "approve-review-button")
    protected FluentWebElement approveReviewButton;

    @FindBy(id = "reject-review-button")
    protected FluentWebElement rejectReviewButton;

    @FindBy(id = "revoke-approval-modal")
    protected FluentWebElement revokeApprovalModal;

    @FindBy(id = "deactivate-occurrence-modal")
    protected FluentWebElement deactivateOccurrenceModal;

    @FindBy(id = "start-review-modal")
    protected FluentWebElement startReviewModal;

    @FindBy(id = "approve-occurrence-modal")
    protected FluentWebElement approveOccurrenceModal;

    @FindBy(id = "reject-occurrence-modal")
    protected FluentWebElement rejectOccurrenceModal;

    @FindBy(id = "remove-document-modal")
    protected FluentWebElement removeDocumentModal;
}
