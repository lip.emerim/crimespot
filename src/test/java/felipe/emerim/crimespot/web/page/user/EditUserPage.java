package felipe.emerim.crimespot.web.page.user;

import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@PageUrl("/user/edit/{userId}")
@Getter
public class EditUserPage extends CreateUserPage {

    @FindBy(id = "deactivate-profile-modal")
    protected FluentWebElement deleteModal;

    public EditUserPage fillUserForm(
            String name,
            String username,
            String email,
            String dateOfBirth,
            String cpf,
            String searchRadius
    ) {
        this.nameField.click().fill().with(name);
        this.usernameField.click().fill().with(username);
        this.emailField.click().fill().with(email);
        this.fillFieldUsingJavascript(
                "#" + this.dateOfBirthField.id(),
                dateOfBirth,
                null
        );
        this.fillFieldUsingJavascript(
                "#" + this.cpfField.id(),
                cpf,
                null
        );
        this.fillFieldUsingJavascript(
                "#" + this.searchRadiusField.id(),
                searchRadius,
                new String[]{"change"}
        );
        return this;
    }

    public EditUserPage checkFieldValues(
            String name,
            String username,
            String email,
            String dateOfBirth,
            String cpf,
            String searchRadius
    ) {
        verifyFieldValue(this.nameField, name);
        verifyFieldValue(this.usernameField, username);
        verifyFieldValue(this.emailField, email);
        verifyFieldValue(this.dateOfBirthField, dateOfBirth);
        verifyFieldValue(this.cpfField, cpf);
        verifyFieldValue(this.searchRadiusField, searchRadius);
        return this;
    }
}
