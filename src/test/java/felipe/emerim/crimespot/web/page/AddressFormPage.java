package felipe.emerim.crimespot.web.page;

import lombok.Getter;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

@SuppressWarnings("unchecked")
@Getter
public class AddressFormPage<T> extends GenericPage {
    @FindBy(id = "postal-code")
    protected FluentWebElement postalCodeField;

    @FindBy(id = "number")
    protected FluentWebElement numberField;

    @FindBy(id = "complement")
    protected FluentWebElement complementField;

    @FindBy(id = "country")
    protected FluentWebElement countryField;

    @FindBy(id = "state")
    protected FluentWebElement stateField;

    @FindBy(id = "city")
    protected FluentWebElement cityField;

    @FindBy(id = "street")
    protected FluentWebElement streetField;

    @FindBy(id = "neighborhood")
    protected FluentWebElement neighborhoodField;

    public T fillAddressForm(String postalCode, String number, String complement) {

        this.fillFieldUsingJavascript(
                "#" + this.postalCodeField.id(),
                postalCode,
                new String[]{"keyup"}
        );
        this.awaitGeocodeFakeRequest();
        this.fillFieldUsingJavascript(
                "#" + this.numberField.id(),
                number,
                null
        );
        this.complementField.click().fill().with(complement);
        return (T) this;
    }

    public T fillAddressFormWithoutPostalCode(
            String country,
            String state,
            String city,
            String neighborhood,
            String street,
            String number,
            String complement
    ) {
        this.countryField.click().fill().with(country);
        this.stateField.click().fill().with(state);
        this.cityField.click().fill().with(city);
        this.neighborhoodField.click().fill().with(neighborhood);
        this.streetField.click().fill().with(street);
        this.numberField.click().fill().with(number);
        this.complementField.click().fill().with(complement);

        return (T) this;
    }

    public T awaitGeocodeFakeRequest() {
        await().atMost(15, TimeUnit.SECONDS).until($("#country[readonly]")).present();
        await().atMost(15, TimeUnit.SECONDS).until($("#country:not([readonly])")).present();
        return (T) this;
    }
}
