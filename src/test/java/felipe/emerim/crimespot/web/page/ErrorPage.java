package felipe.emerim.crimespot.web.page;

import lombok.Getter;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@Getter
public class ErrorPage extends FluentPage {
    @FindBy(id = "error-status")
    protected FluentWebElement errorStatus;

    public ErrorPage checkErrorStatus(String status) {

        await().atMost(15, TimeUnit.SECONDS).until(this.errorStatus).present();

        assertThat(this.errorStatus).hasText(status);
        return this;
    }
}
