package felipe.emerim.crimespot.web.page.user;

import felipe.emerim.crimespot.web.page.AddressFormPage;
import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@PageUrl("/user/create")
@Getter
public class CreateUserPage extends AddressFormPage<CreateUserPage> {
    @FindBy(id = "user-form")
    protected FluentWebElement form;

    @FindBy(id = "submit-button")
    protected FluentWebElement submitButton;

    @FindBy(id = "deactivate-user-button")
    protected FluentWebElement deactivateUserButton;

    @FindBy(id = "change-password-button")
    protected FluentWebElement changePasswordButton;

    @FindBy(id = "name")
    protected FluentWebElement nameField;

    @FindBy(id = "username")
    protected FluentWebElement usernameField;

    @FindBy(id = "email")
    protected FluentWebElement emailField;

    @FindBy(id = "date-of-birth-label")
    protected FluentWebElement dateOfBirthField;

    @FindBy(id = "cpf")
    protected FluentWebElement cpfField;

    @FindBy(id = "search-radius")
    protected FluentWebElement searchRadiusField;

    @FindBy(id = "password")
    protected FluentWebElement passwordField;

    @FindBy(id = "password-confirm")
    protected FluentWebElement passwordConfirmField;

    public CreateUserPage fillUserForm(
            String name,
            String username,
            String email,
            String dateOfBirth,
            String cpf,
            String searchRadius,
            String password,
            String passwordConfirm
    ) {
        this.nameField.click().fill().with(name);
        this.usernameField.click().fill().with(username);
        this.emailField.click().fill().with(email);
        this.fillFieldUsingJavascript(
                "#" + this.dateOfBirthField.id(),
                dateOfBirth,
                null
        );
        this.fillFieldUsingJavascript(
                "#" + this.cpfField.id(),
                cpf,
                null
        );
        this.fillFieldUsingJavascript(
                "#" + this.searchRadiusField.id(),
                searchRadius,
                new String[]{"change"}
        );
        this.passwordField.click().fill().with(password);
        this.passwordConfirmField.click().fill().with(passwordConfirm);
        return this;
    }

    public CreateUserPage submitForm() {
        this.submitButton.click();
        return this;
    }
}
