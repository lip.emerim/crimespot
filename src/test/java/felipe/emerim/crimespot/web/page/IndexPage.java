package felipe.emerim.crimespot.web.page;

import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

/**
 * Created Felipe Emerim on 06/05/2019
 **/

@PageUrl("/")
@Getter
public class IndexPage extends GenericPage {
    @FindBy(id = "login-link")
    protected FluentWebElement loginLink;

    @FindBy(id = "map-filter-button")
    protected FluentWebElement mapFilterButton;

    @FindBy(id = "map-filter-close")
    protected FluentWebElement mapFilterClose;

    @FindBy(id = "searchTerm")
    protected FluentWebElement searchTermField;

    @FindBy(id = "map-range")
    protected FluentWebElement mapRangeField;

    @FindBy(id = "occurrenceTypeEnums")
    protected FluentWebElement occurrenceTypeEnumsField;

    @FindBy(id = "only-verified")
    protected FluentWebElement onlyVerifiedField;

    @FindBy(id = "submit-search-button")
    protected FluentWebElement submitSearchButton;

    @FindBy(id = "map-occurrence-list")
    protected FluentWebElement mapOccurrenceList;

    @FindBy(id = "map-callout")
    protected FluentWebElement mapCallout;

    @FindBy(id = "map-filters")
    protected FluentWebElement mapFilters;

    public static String LIST_ITEM_SELECTOR = "#occurrence-item-";

    public static String LIST_SELECTOR = ".map-occurrence-item";

    public IndexPage checkAllOccurrencesVerified() {
        FluentList<FluentWebElement> elements = this.mapOccurrenceList.find(LIST_SELECTOR);

        for (FluentWebElement element : elements) {
            String iconPath = element.find("img").first().attribute("src");

            assertThat(iconPath).containsIgnoringCase("verified");
        }

        return this;
    }

    public IndexPage verifyOccurrenceCount(int count) {
        FluentList<FluentWebElement> elements = this.mapOccurrenceList.find(LIST_SELECTOR);

        assertThat(elements).hasSize(count);


        return this;
    }

    public IndexPage verifyAnyContainTerm(String term) {
        FluentList<FluentWebElement> elements = this.mapOccurrenceList.find(LIST_SELECTOR);

        boolean hasTerm = false;

        for (FluentWebElement element : elements) {
            boolean inTitle = element.$("h4").texts().stream().anyMatch(t -> t.contains(term));
            boolean inContent = element.$("p").texts().stream().anyMatch(t -> t.contains(term));

            if (inTitle || inContent) {
                hasTerm = true;
                break;
            }
        }

        assertThat(hasTerm).isTrue();

        return this;
    }

    public IndexPage verifyAllContainTerm(String term) {
        FluentList<FluentWebElement> elements = this.mapOccurrenceList.find(LIST_SELECTOR);

        for (FluentWebElement element : elements) {
            boolean inTitle = element.$("h4").texts().stream().anyMatch(t -> t.contains(term));
            boolean inContent = element.$("p").texts().stream().anyMatch(t -> t.contains(term));

            boolean hasTerm = inTitle || inContent;

            assertThat(hasTerm).isTrue();
        }

        return this;
    }

    public IndexPage verifyMapCalloutTitle(String title) {
        await().atMost(15, TimeUnit.SECONDS).until(
                this.mapCallout.find(".callout-secondary h5").first()
        ).present();

        assertThat(this.mapCallout.find(".callout-secondary h5").first()).hasTextContaining(title);

        return this;
    }

    public IndexPage clickLoginButton() {

        loginLink.click();
        return this;
    }
}
