package felipe.emerim.crimespot.web.page;

import lombok.Getter;
import org.fluentlenium.core.FluentPage;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@Getter
public class GenericPage extends FluentPage {
    private static final String LOGIN_FORM = "#login-user-form";

    private final String toastContainerSelector = "#toastsContainerTopRight";

    @FindBy(id = "user-info")
    private FluentWebElement userInfo;

    @FindBy(id = "logout-button")
    private FluentWebElement logoutButton;

    @FindBy(id = "list-users-link")
    private FluentWebElement listUsersLink;

    private final String listOccurrencesLinkSelector = "#list-occurrences-link";

    @FindBy(id = "in-analysis-occurrences-link")
    private FluentWebElement inAnalysisOccurrencesLink;

    @FindBy(id = "pending-occurrences-link")
    private FluentWebElement pendingOccurrencesLink;

    @FindBy(id = "reported-occurrences-link")
    private FluentWebElement reportedOccurrencesLink;

    @FindBy(id = "all-occurrences-link")
    private FluentWebElement allOccurrencesLink;

    @FindBy(id = "list-occurrences-dropdown")
    private FluentWebElement listOccurrencesDropdown;

    public GenericPage logout() {
        userInfo.click();
        logoutButton.click();
        return this;
    }

    public GenericPage verifyLoggedIn() {
        await().atMost(15, TimeUnit.SECONDS).until($("#user-info")).present();
        return this;
    }

    public GenericPage goToEditUserPage() {
        await().atMost(15, TimeUnit.SECONDS).until($("#user-info")).clickable();
        $("#user-info").click();
        await().atMost(15, TimeUnit.SECONDS).until($("#profile-button")).clickable();
        $("#profile-button").click();

        return this;
    }

    public GenericPage closeToast(String type) {
        await().atMost(15, TimeUnit.SECONDS).until(
                this.getToastsContainer()
        ).present();

        await().atMost(15, TimeUnit.SECONDS).until(
                this.getToastsContainer().el(".toast.bg-" + type + " .close")
        ).clickable();

        this.getToastsContainer().el(".toast.bg-" + type + " .close").click();

        await().atMost(15, TimeUnit.SECONDS).until(
                this.getToastsContainer().el(".toast.bg-" + type + " .close")
        ).not().present();

        return this;
    }

    /**
     * Set the value of bootstrap custom file inputs.
     * <p>
     * In this case we need to show the real input, set the value, then
     * hide it again.
     *
     * @param selector The input selector
     * @param filePath The value of the selector
     * @return self
     */
    public GenericPage fillFileInput(String selector, String filePath) {
        String script = "$('{selector}').css('opacity', '{opacity}')";
        script = script.replace("{selector}", selector);

        this.executeScript(script.replace("{opacity}", "1"));

        $(selector).fill().with(filePath);

        this.executeScript(script.replace("{opacity}", "1"));

        return this;
    }

    /**
     * Fill a field using javascript
     *
     * @param fieldSelector The selector of the field
     * @param value         The value of the field
     * @param postEvents    The events to trigger after setting the value
     * @return self
     */
    public GenericPage fillFieldUsingJavascript(String fieldSelector, String value, String[] postEvents) {
        String script = "$('{selector}').val('{value}');";
        script = script.replace("{selector}", fieldSelector);
        script = script.replace("{value}", value);

        this.executeScript(script);

        if (postEvents == null) {
            return this;
        }

        script = "$('{selector}').trigger('{event}')";
        script = script.replace("{selector}", fieldSelector);

        for (String event : postEvents) {
            this.executeScript(script.replace("{event}", event));
        }

        return this;
    }

    public GenericPage verifyFieldValue(FluentWebElement field, String value) {
        await().atMost(15, TimeUnit.SECONDS).until(field).value(value);
        return this;
    }

    public GenericPage verifyToastWithValue(String type, String value) {

        await().atMost(15, TimeUnit.SECONDS).until(
                this.getToastsContainer()
        ).present();

        await().atMost(15, TimeUnit.SECONDS).until(
                this.getToastsContainer().el(".toast.bg-" + type + " .toast-body span")
        ).present();


        assertThat(this.getToastsContainer().el(".toast-body span").text()).isEqualTo(
                value
        );

        return this;
    }

    public GenericPage assertFormErrorCount(FluentWebElement form, int count) {
        assertThat(form.$(".is-invalid").size()).isEqualTo(count);
        return this;
    }

    public GenericPage confirmModal(FluentWebElement modal) {
        await().atMost(5, TimeUnit.SECONDS).until(
                modal.$(".btn-primary")
        ).clickable();

        modal.$(".btn-primary").click();

        return this;
    }

    public GenericPage deleteCookieByName(String name) {
        this.getDriver().manage().deleteCookieNamed(name);
        return this;
    }

    public GenericPage checkInAnalysisCounter(int count) {
        return this.checkCounter(this.getInAnalysisOccurrencesLink(), count);
    }

    public GenericPage checkPendingCounter(int count) {
        return this.checkCounter(this.getPendingOccurrencesLink(), count);
    }

    public GenericPage checkReportedCounter(int count) {
        return this.checkCounter(this.getReportedOccurrencesLink(), count);
    }

    protected GenericPage checkCounter(FluentWebElement element, int count) {
        this.listOccurrencesDropdown.click();
        await().atMost(15, TimeUnit.SECONDS)
                .until(element.find(".badge.badge-secondary")).displayed();

        assertThat(element.find(".badge.badge-secondary").get(0).text()).contains(String.valueOf(count));

        this.listOccurrencesDropdown.click();

        await().atMost(15, TimeUnit.SECONDS)
                .until(element.find(".badge.badge-secondary")).not().displayed();

        return this;
    }

    public void clickInAnalysisLink() {
        this.clickOccurrencesDropdownLink(this.inAnalysisOccurrencesLink);
    }

    public void clickPendingLink() {
        this.clickOccurrencesDropdownLink(this.pendingOccurrencesLink);
    }

    public void clickReportedLink() {
        this.clickOccurrencesDropdownLink(this.reportedOccurrencesLink);
    }

    public void clickAllOccurrencesLink() {
        this.clickOccurrencesDropdownLink(this.allOccurrencesLink);
    }

    public void clickOccurrencesDropdownLink(FluentWebElement link) {
        this.listOccurrencesDropdown.click();
        await().atMost(15, TimeUnit.SECONDS)
                .until(link).displayed();
        link.click();
    }

    public FluentWebElement getListOccurrencesLink() {
        return el(this.listOccurrencesLinkSelector);
    }

    public FluentWebElement getToastsContainer() {
        return el(this.toastContainerSelector);
    }
}
