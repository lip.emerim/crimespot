package felipe.emerim.crimespot.web.page.occurrence;

import felipe.emerim.crimespot.web.page.ListPage;
import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

@PageUrl("/occurrence/list")
@Getter
public class ListOccurrencePage extends ListPage<ListOccurrencePage> {

    @FindBy(id = "create-occurrence-button")
    protected FluentWebElement createOccurrenceButton;

    @FindBy(id = "searchTerm")
    protected FluentWebElement searchTermField;

    @FindBy(id = "occurrenceStatus")
    protected FluentWebElement statusField;

    @FindBy(id = "occurrenceSort")
    protected FluentWebElement sortField;

    @FindBy(id = "occurrenceTypes")
    protected FluentWebElement typesField;

    @FindBy(id = "only-related-to-me")
    protected FluentWebElement onlyRelatedToMeField;

    @FindBy(id = "search-button")
    protected FluentWebElement searchButton;

    @FindBy(id = "no-results-container")
    protected FluentWebElement noResultsContainer;

    public static String LIST_SELECTOR = ".callout-secondary";
    public static String DETAIL_BUTTON_SELECTOR = ".btn-see-details.btn-secondary";
    public static String DETAIL_BUTTON_IN_REVIEW_SELECTOR = ".btn-continue-review.btn-secondary";

    @Override
    public String getListSelector() {
        return LIST_SELECTOR;
    }
}
