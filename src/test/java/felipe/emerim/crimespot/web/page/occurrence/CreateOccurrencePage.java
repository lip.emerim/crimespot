package felipe.emerim.crimespot.web.page.occurrence;

import felipe.emerim.crimespot.web.page.AddressFormPage;
import lombok.Getter;
import org.fluentlenium.core.annotation.PageUrl;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;


@PageUrl("/occurrence/create")
@Getter
public class CreateOccurrencePage extends AddressFormPage<CreateOccurrencePage> {
    @FindBy(id = "occurrence-form")
    protected FluentWebElement form;

    @FindBy(id = "title")
    protected FluentWebElement titleField;

    @FindBy(id = "description")
    protected FluentWebElement descriptionField;

    @FindBy(id = "occurrence-date-label")
    protected FluentWebElement dateField;

    @FindBy(id = "type")
    protected FluentWebElement typeField;

    @FindBy(id = "file")
    protected FluentWebElement documentField;

    @FindBy(id = "submit-button")
    protected FluentWebElement submitButton;

    @FindBy(id = "download-document-button")
    protected FluentWebElement downloadDocumentButton;

    public CreateOccurrencePage fillForm(
            String title,
            String description,
            String date,
            String type,
            String documentPath
    ) {
        this.titleField.fill().with(title);
        this.descriptionField.fill().with(description);
        this.fillFieldUsingJavascript(
                "#" + this.dateField.id(),
                date,
                null
        );

        this.fillFieldUsingJavascript(
                "#" + this.typeField.id(),
                type,
                new String[]{"change"}
        );

        if (documentPath != null) {
            this.fillDocumentField(documentPath);
        }

        return this;
    }

    public CreateOccurrencePage fillDocumentField(String documentPath) {
        this.fillFileInput(
                "#" + this.documentField.id(),
                documentPath
        );

        return this;
    }

    public CreateOccurrencePage submitForm() {
        this.submitButton.click();
        return this;
    }
}
