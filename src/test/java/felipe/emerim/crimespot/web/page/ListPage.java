package felipe.emerim.crimespot.web.page;

import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@SuppressWarnings("unchecked")
public abstract class ListPage<T> extends GenericPage {

    public abstract String getListSelector();

    public T clickItemElement(int position, String elementSelector) {
        $(this.getListSelector()).get(position).$(elementSelector).click();
        return (T) this;
    }

    public T verifyAnyContainTerm(String term) {
        assertThat($(this.getListSelector() + " span")).hasTextContaining(term);
        return (T) this;
    }

    public T verifyAllContainTerm(String term) {
        FluentList<FluentWebElement> elements = $(this.getListSelector());

        for (FluentWebElement element : elements) {
            assertThat(element.$("span")).hasTextContaining(term);
        }
        return (T) this;
    }

    public T verifyElementCount(int count, String selector) {
        assertThat($(selector)).hasSize(count);
        return (T) this;
    }

    public T verifyNoneContainTerm(String term) {
        FluentList<FluentWebElement> elements = $(this.getListSelector());

        for (FluentWebElement element : elements) {
            assertThat(element.$("span")).hasNotTextContaining(term);
        }
        return (T) this;
    }

}
