package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.dao.OccurrenceDao;
import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.FileTypeEnum;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import felipe.emerim.crimespot.form.OccurrenceSearchForm;
import felipe.emerim.crimespot.helper.FileHelper;
import felipe.emerim.crimespot.helper.OccurrenceHelper;
import felipe.emerim.crimespot.repository.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.verify;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class OccurrenceServiceTest extends BaseServiceTest {
    @MockBean
    OccurrenceDao occurrenceDao;
    @Autowired
    private OccurrenceRepository occurrenceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OccurrenceService occurrenceService;
    @Autowired
    private OccurrenceTypeRepository occurrenceTypeRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private OccurrenceStatusRepository occurrenceStatusRepository;
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private ReportRepository reportRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testWaitedSinceLastOccurrenceByTypeAndUserTrue() {
        User user = userRepository.findById(1000L).get();
        OccurrenceType type = occurrenceTypeRepository.findById(1L).get();

        Occurrence occurrence = occurrenceRepository
                .findByOccurrenceTypeAndUserOrderByCreatedAtDesc(type, user);

        occurrence.setCreatedAt(LocalDateTime.now().minusMinutes(6L));
        occurrenceRepository.save(occurrence);

        assertThat(occurrenceService.waitedSinceLastOccurrenceByTypeAndUser(
                type,
                user
        )).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testWaitedSinceLastOccurrenceByTypeAndUserFalse() {
        User user = userRepository.findById(1000L).get();
        OccurrenceType type = occurrenceTypeRepository.findById(1L).get();

        Occurrence occurrence = occurrenceRepository
                .findByOccurrenceTypeAndUserOrderByCreatedAtDesc(type, user);

        occurrence.setCreatedAt(LocalDateTime.now().minusMinutes(4L));
        occurrenceRepository.save(occurrence);

        assertThat(occurrenceService.waitedSinceLastOccurrenceByTypeAndUser(
                type,
                user
        )).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testWaitedSinceLastOccurrenceByTypeAndUserNoLastOccurrence() {
        User user = userRepository.findById(1000L).get();
        OccurrenceType type = occurrenceTypeRepository.findById(1L).get();

        Occurrence occurrence = occurrenceRepository
                .findByOccurrenceTypeAndUserOrderByCreatedAtDesc(type, user);

        occurrenceRepository.delete(occurrence);

        assertThat(occurrenceService.waitedSinceLastOccurrenceByTypeAndUser(
                type,
                user
        )).isTrue();
    }

    @Test
    public void testFindByIdAndUserActiveIsTrueAndActiveIsTrueExists() {
        Optional<Occurrence> occurrence = occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(1000L);

        assertThat(occurrence.isPresent()).isTrue();
    }

    @Test
    public void testFindByIdAndUserActiveIsTrueAndActiveIsTrueNotExists() {
        Optional<Occurrence> occurrence = occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(100000L);

        assertThat(occurrence.isPresent()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateOccurrenceWaited() throws IOException {
        User user = userRepository.findById(1000L).get();
        OccurrenceType type = occurrenceTypeRepository.findById(1L).get();

        Occurrence occurrence = occurrenceRepository
                .findByOccurrenceTypeAndUserOrderByCreatedAtDesc(type, user);

        occurrence.setCreatedAt(LocalDateTime.now().minusMinutes(6L));
        occurrenceRepository.save(occurrence);

        occurrence = this.getOccurrenceToSave();

        occurrence = occurrenceService.save(user, occurrence);
        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.getUser()).isEqualTo(user);
        assertThat(occurrence.getId()).isNotNull();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.WAITING_ANALYSIS);
        assertThat(occurrence.isVerified()).isFalse();
        assertThat(occurrence.isActive()).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateOccurrenceDidNotWait() throws IOException {
        User user = userRepository.findById(1000L).get();
        OccurrenceType type = occurrenceTypeRepository.findById(1L).get();

        Occurrence occurrence = occurrenceRepository
                .findByOccurrenceTypeAndUserOrderByCreatedAtDesc(type, user);

        occurrence.setCreatedAt(LocalDateTime.now().minusMinutes(4L));
        occurrenceRepository.save(occurrence);

        Occurrence occurrenceSave = this.getOccurrenceToSave();

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> occurrenceService.save(user, occurrenceSave)
        ).withMessage("An occurrence with this type was recently created");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    protected void testEditOccurrence() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        LocalDate localDate = LocalDate.now().minusDays(1);
        Long occurrenceId = occurrence.getId();
        File file = FileHelper.createPDFFile();
        OccurrenceType type = occurrenceTypeRepository.findById(2L).get();

        // Avoid hibernate auto update feature
        entityManager.flush();
        entityManager.detach(occurrence);

        occurrence.getAddress().setComplement("Cool complement");
        occurrence.setTitle("new amazing title");
        occurrence.setDescription("new amazing description with more than 15 characters");
        occurrence.setOccurrenceDate(localDate);
        occurrence.setOccurrenceType(type);
        occurrence.setOccurrenceDocument(file);

        occurrenceService.save(user, occurrence);
        occurrence = occurrenceRepository.findById(occurrenceId).get();

        assertThat(occurrence.getAddress().getComplement()).isEqualTo("Cool complement");
        assertThat(occurrence.getUser().getId()).isEqualTo(user.getId());
        assertThat(occurrence.getTitle()).isEqualTo("new amazing title");
        assertThat(occurrence.getDescription()).isEqualTo("new amazing description with more than 15 characters");
        assertThat(occurrence.getOccurrenceType()).isEqualTo(type);
        assertThat(occurrence.getOccurrenceDate()).isEqualTo(localDate);
        assertThat(occurrence.getOccurrenceDocument()).isNotNull();
        assertThat(occurrence.getOccurrenceDocument().getFileContent()).isEqualTo(file.getFileContent());
        assertThat(occurrence.getId()).isEqualTo(occurrenceId);
        assertThat(occurrence.isActive()).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    protected void testEditOccurrenceNoDocument() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        LocalDate localDate = LocalDate.now().minusDays(1);
        Long occurrenceId = occurrence.getId();
        OccurrenceType type = occurrenceTypeRepository.findById(2L).get();

        // Avoid hibernate auto update feature
        entityManager.flush();
        entityManager.detach(occurrence);

        occurrence.getAddress().setComplement("Cool complement");
        occurrence.setTitle("new amazing title");
        occurrence.setDescription("new amazing description with more than 15 characters");
        occurrence.setOccurrenceDate(localDate);
        occurrence.setOccurrenceType(type);
        occurrence.setOccurrenceDocument(null);

        occurrenceService.save(user, occurrence);
        occurrence = occurrenceRepository.findById(occurrenceId).get();

        assertThat(occurrence.getAddress().getComplement()).isEqualTo("Cool complement");
        assertThat(occurrence.getTitle()).isEqualTo("new amazing title");
        assertThat(occurrence.getDescription()).isEqualTo("new amazing description with more than 15 characters");
        assertThat(occurrence.getOccurrenceType()).isEqualTo(type);
        assertThat(occurrence.getOccurrenceDate()).isEqualTo(localDate);
        assertThat(occurrence.getOccurrenceDocument()).isNull();
        assertThat(occurrence.getId()).isEqualTo(occurrenceId);
        assertThat(occurrence.isActive()).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    protected void testEditOccurrenceUpdateExistingDocument() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        LocalDate localDate = LocalDate.now().minusDays(1);
        Long occurrenceId = occurrence.getId();
        File file = FileHelper.createPDFFile();
        OccurrenceType type = occurrenceTypeRepository.findById(2L).get();

        occurrence.setOccurrenceDocument(FileHelper.createPNGFile());
        occurrenceRepository.save(occurrence);

        // Avoid hibernate auto update feature
        entityManager.flush();
        entityManager.detach(occurrence);

        Long documentId = occurrence.getOccurrenceDocument().getId();

        occurrence.getAddress().setComplement("Cool complement");
        occurrence.setTitle("new amazing title");
        occurrence.setDescription("new amazing description with more than 15 characters");
        occurrence.setOccurrenceDate(localDate);
        occurrence.setOccurrenceType(type);
        occurrence.setOccurrenceDocument(file);

        occurrenceService.save(user, occurrence);
        occurrence = occurrenceRepository.findById(occurrenceId).get();

        assertThat(occurrence.getAddress().getComplement()).isEqualTo("Cool complement");
        assertThat(occurrence.getTitle()).isEqualTo("new amazing title");
        assertThat(occurrence.getDescription()).isEqualTo("new amazing description with more than 15 characters");
        assertThat(occurrence.getOccurrenceType()).isEqualTo(type);
        assertThat(occurrence.getOccurrenceDate()).isEqualTo(localDate);
        assertThat(occurrence.getOccurrenceDocument()).isNotNull();
        assertThat(occurrence.getOccurrenceDocument().getFileContent()).isEqualTo(file.getFileContent());
        assertThat(occurrence.getOccurrenceDocument().getId()).isEqualTo(documentId);
        assertThat(occurrence.getOccurrenceDocument().getContentType()).isEqualTo(file.getContentType());
        assertThat(occurrence.getId()).isEqualTo(occurrenceId);
        assertThat(occurrence.isActive()).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    protected void testEditOccurrenceNotOwner() {
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();

        // Avoid hibernate auto update feature
        entityManager.flush();
        entityManager.detach(occurrence);

        occurrence.setTitle("new amazing title");

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.save(user, occurrence)
        ).withMessage(messages.get("error.forbidden"));

        Occurrence persistedOccurrence = occurrenceRepository.findById(1000L).get();
        assertThat(persistedOccurrence.getTitle()).isNotEqualTo("new amazing title");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    protected void testEditOccurrenceNotFound() {
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();

        // Avoid hibernate auto update feature
        entityManager.flush();
        entityManager.detach(occurrence);

        occurrence.setId(40000L);

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.save(user, occurrence)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testStartReview() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);

        occurrence = occurrenceService.startReview(user, occurrence.getId());
        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.getModerator().getId()).isEqualTo(user.getId());
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.IN_ANALYSIS);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testStartReviewOccurrenceNotFound() {
        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.startReview(user, 50000L)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testStartReviewOccurrenceNotModerator() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);

        user.removeRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.startReview(user, occurrence.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testStartReviewOccurrenceNotReviewable() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        occurrence.setOccurrenceStatus(occurrenceStatusRepository.findByDescription(OccurrenceStatusEnum.APPROVED));
        occurrenceRepository.save(occurrence);

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> occurrenceService.startReview(user, occurrence.getId())
        ).withMessage("Occurrence cannot be reviewed");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testApproveOccurrenceWithDocument() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        occurrence = occurrenceService.finishReview(user, occurrence.getId(),
                "that is an occurrence alright",
                OccurrenceStatusEnum.APPROVED);

        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.isVerified()).isTrue();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.APPROVED);
        assertThat(occurrence.getModeratorComments()).isEqualTo("that is an occurrence alright");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testApproveOccurrenceWithoutDocument() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceDocument(null);
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        occurrence = occurrenceService.finishReview(user, occurrence.getId(),
                "that is an occurrence alright",
                OccurrenceStatusEnum.APPROVED);

        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.isVerified()).isFalse();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.APPROVED);
        assertThat(occurrence.getModeratorComments()).isEqualTo("that is an occurrence alright");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRejectOccurrenceWithDocument() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        occurrence = occurrenceService.finishReview(user, occurrence.getId(),
                "that is an occurrence alright",
                OccurrenceStatusEnum.REJECTED);

        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.isVerified()).isFalse();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.REJECTED);
        assertThat(occurrence.getModeratorComments()).isEqualTo("that is an occurrence alright");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRejectOccurrenceWithoutDocument() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceDocument(null);
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        occurrence = occurrenceService.finishReview(user, occurrence.getId(),
                "that is an occurrence alright",
                OccurrenceStatusEnum.REJECTED);

        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.isVerified()).isFalse();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.REJECTED);
        assertThat(occurrence.getModeratorComments()).isEqualTo("that is an occurrence alright");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testFinishReviewOccurrenceNotInReviewByUser() throws IOException {
        User user = userRepository.findById(1000L).get();
        User wrongUser = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceDocument(null);
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.finishReview(wrongUser, occurrence.getId(),
                        "that is an occurrence alright",
                        OccurrenceStatusEnum.APPROVED)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testFinishReviewOccurrenceDoesNotExist() {
        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.finishReview(user, 40000L,
                        "that is an occurrence alright",
                        OccurrenceStatusEnum.APPROVED)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRevokeApproval() throws IOException {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        occurrenceService.finishReview(user, occurrence.getId(),
                "that is an occurrence alright",
                OccurrenceStatusEnum.APPROVED);

        occurrenceService.revokeApproval(user, occurrence.getId());
        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.REJECTED_BY_ADMIN);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRevokeApprovalOccurrenceDoesNotExist() {

        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.revokeApproval(user, 40000L)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRevokeApprovalUserNotAdmin() throws IOException {
        User user = userRepository.findById(1000L).get();
        User wrongUser = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrenceService.startReview(user, occurrence.getId());

        occurrenceService.finishReview(user, occurrence.getId(),
                "that is an occurrence alright",
                OccurrenceStatusEnum.APPROVED);

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.revokeApproval(wrongUser, occurrence.getId())
        ).withMessage(messages.get("error.forbidden"));

    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRevokeApprovalOccurrenceNotApproved() {

        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.revokeApproval(user, 1000L)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDeactivateOccurrenceAdmin() throws IOException {
        User admin = userRepository.findById(1000L).get();
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrence = occurrenceService.deactivate(admin, occurrence.getId());
        occurrence = occurrenceRepository.findById(occurrence.getId()).get();

        assertThat(occurrence.isActive()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDeactivateOccurrenceOwner() throws IOException {
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrenceService.save(user, occurrence);
        occurrence = occurrenceService.deactivate(user, occurrence.getId());
        occurrence = occurrenceRepository.findById(occurrence.getId()).get();
        assertThat(occurrence.isActive()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDeactivateOccurrenceNeitherAdminNorOwner() throws IOException {
        User admin = userRepository.findById(1000L).get();
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.deactivate(user, persistedOccurrence.getId())
        ).withMessage(messages.get("error.forbidden"));

        assertThat(persistedOccurrence.isActive()).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDeactivateOccurrenceNotExist() {
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.revokeApproval(admin, 40000L)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCanEditOccurrenceOwnerButNotEditable() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        occurrence = occurrenceService.save(admin, occurrence);
        occurrenceService.startReview(admin, occurrence.getId());

        assertThat(occurrenceService.canEditOccurrence(admin, occurrence)).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCanEditOccurrenceEditableButNotOwner() throws IOException {
        User admin = userRepository.findById(1000L).get();
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThat(occurrenceService.canEditOccurrence(user, persistedOccurrence)).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCanEditOccurrenceOwnerAndEditable() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThat(occurrenceService.canEditOccurrence(admin, persistedOccurrence)).isTrue();
    }

    @Test
    public void testCanViewOccurrenceModerator() {
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        User user = userRepository.findById(1001L).get();

        assertThat(occurrenceService.canViewOccurrence(user, occurrence)).isTrue();

    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCanViewOccurrenceOwner() {
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        User user = userRepository.findById(1000L).get();
        user.removeRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());

        assertThat(occurrenceService.canViewOccurrence(user, occurrence)).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCanViewOccurrenceNeitherOwnerNorModerator() {
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        User user = userRepository.findById(1001L).get();
        user.removeRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());

        assertThat(occurrenceService.canViewOccurrence(user, occurrence)).isFalse();
    }

    @Test
    public void testFullSearch() {
        User user = userRepository.findById(1000L).get();
        OccurrenceSearchForm form = new OccurrenceSearchForm();
        int pageLength = 8;

        occurrenceService.fullSearch(user, form, pageLength);

        verify(occurrenceDao).fullSearch(user, form, pageLength);
    }

    @Test
    public void testMapFullSearch() {
        OccurrenceMapSearchForm form = new OccurrenceMapSearchForm();

        occurrenceService.mapFullSearch(form);

        verify(occurrenceDao).mapFullSearch(form);
    }

    @Test
    public void testCheckOccurrenceOwnershipTrue() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();

        assertThatCode(() -> occurrenceService
                .checkOccurrenceOwnership(user, occurrence, occurrence.getAddress())
        ).doesNotThrowAnyException();
    }

    @Test
    public void testCheckOccurrenceOwnershipFalse() {
        User user = userRepository.findById(1001L).get();
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.checkOccurrenceOwnership(user, occurrence, occurrence.getAddress())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    public void testSetOccurrenceDocumentNullDocument() throws IOException {
        Occurrence occurrence = this.getOccurrenceToSave();
        File resource = FileHelper.createPDFFile();
        occurrence.setOccurrenceDocument(null);
        MultipartFile file = new MockMultipartFile(
                "filename", "filename", resource.getContentType(), resource.getDecodedContent()
        );

        occurrenceService.setDocument(occurrence, file);

        assertThat(occurrence.getOccurrenceDocument().getContentType()).isEqualTo(resource.getContentType());
        assertThat(occurrence.getOccurrenceDocument().getFileContent()).isEqualTo(resource.getFileContent());
        assertThat(occurrence.getOccurrenceDocument().getFileType()).isEqualTo(FileTypeEnum.OCCURRENCE_DOCUMENT);
    }

    @Test
    public void testSetOccurrenceDocumentNonDocument() throws IOException {
        Occurrence occurrence = this.getOccurrenceToSave();
        File resource = FileHelper.createPDFFile();
        MultipartFile file = new MockMultipartFile(
                "filename", "filename", resource.getContentType(), resource.getDecodedContent()
        );

        occurrenceService.setDocument(occurrence, file);

        assertThat(occurrence.getOccurrenceDocument().getContentType()).isEqualTo(resource.getContentType());
        assertThat(occurrence.getOccurrenceDocument().getFileContent()).isEqualTo(resource.getFileContent());
        assertThat(occurrence.getOccurrenceDocument().getFileType()).isEqualTo(FileTypeEnum.OCCURRENCE_DOCUMENT);
    }

    @Test
    public void testSetOccurrenceDocumentNullFile() throws IOException {
        Occurrence occurrence = this.getOccurrenceToSave();
        File originalDocument = occurrence.getOccurrenceDocument();

        occurrenceService.setDocument(occurrence, null);

        assertThat(occurrence.getOccurrenceDocument()).isEqualTo(originalDocument);
    }

    @Test
    public void testSetOccurrenceDocumentEmptyFile() throws IOException {
        Occurrence occurrence = this.getOccurrenceToSave();
        File originalDocument = occurrence.getOccurrenceDocument();
        File resource = FileHelper.createJPGFile();
        MultipartFile file = new MockMultipartFile(
                "filename", "filename", resource.getContentType(), new byte[]{}
        );

        occurrenceService.setDocument(occurrence, file);

        assertThat(occurrence.getOccurrenceDocument()).isEqualTo(originalDocument);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testGetDocument() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatCode(() -> occurrenceService.getDocument(admin,
                persistedOccurrence.getId(), persistedOccurrence.getOccurrenceDocument().getId())
        ).doesNotThrowAnyException();

        File document = occurrenceService.getDocument(admin,
                persistedOccurrence.getId(), persistedOccurrence.getOccurrenceDocument().getId());

        assertThat(document).isEqualTo(occurrence.getOccurrenceDocument());

    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testGetDocumentNoDocumentAvailable() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceDocument(null);
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.getDocument(admin, persistedOccurrence.getId(),
                        1L)
        ).withMessage(messages.get("error.notFound"));


    }

    @Test
    public void testGetDocumentOccurrenceNotFound() {
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.getDocument(admin, 40000L,
                        1L)
        ).withMessage(messages.get("error.notFound"));


    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testGetDocumentModeratorButNotOwner() throws IOException {
        User admin = userRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatCode(() -> occurrenceService.getDocument(moderator,
                persistedOccurrence.getId(), persistedOccurrence.getOccurrenceDocument().getId())
        ).doesNotThrowAnyException();

        File document = occurrenceService.getDocument(admin,
                persistedOccurrence.getId(), persistedOccurrence.getOccurrenceDocument().getId());

        assertThat(document).isEqualTo(occurrence.getOccurrenceDocument());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testGetDocumentOwnerButNotModerator() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        admin.removeRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatCode(() -> occurrenceService.getDocument(admin,
                persistedOccurrence.getId(), persistedOccurrence.getOccurrenceDocument().getId())
        ).doesNotThrowAnyException();

        File document = occurrenceService.getDocument(admin,
                persistedOccurrence.getId(), persistedOccurrence.getOccurrenceDocument().getId());

        assertThat(document).isEqualTo(occurrence.getOccurrenceDocument());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testGetDocumentNeitherOwnerNorModerator() throws IOException {
        User admin = userRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());

        moderator.removeRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());

        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.getDocument(moderator, persistedOccurrence.getId(),
                        occurrence.getOccurrenceDocument().getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRemoveDocument() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());
        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        Long documentId = persistedOccurrence.getOccurrenceDocument().getId();

        assertThatCode(() -> occurrenceService.removeDocument(admin,
                occurrence.getId(), occurrence.getOccurrenceDocument().getId())
        ).doesNotThrowAnyException();

        assertThat(fileRepository.findById(documentId).isPresent()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRemoveDocumentNoDocumentAvailable() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceDocument(null);
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());
        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.removeDocument(admin, persistedOccurrence.getId(),
                        1L)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRemoveDocumentUserNotOwner() throws IOException {
        User admin = userRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());
        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.removeDocument(moderator, persistedOccurrence.getId(),
                        1L)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRemoveDocumentOccurrenceNotEditable() throws IOException {
        User admin = userRepository.findById(1000L).get();
        Occurrence occurrence = this.getOccurrenceToSave();
        occurrence.setOccurrenceType(occurrenceTypeRepository.findById(2L).get());
        Occurrence persistedOccurrence = occurrenceService.save(admin, occurrence);

        occurrenceService.startReview(admin, occurrence.getId());

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.removeDocument(admin, persistedOccurrence.getId(),
                        1L)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    public void testRemoveDocumentOccurrenceNotFound() {
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.removeDocument(admin, 40000L,
                        1L)
        ).withMessage(messages.get("error.notFound"));
    }

    protected Occurrence getOccurrenceToSave() throws IOException {

        User user = userRepository.findById(1000L).get();
        OccurrenceType type = occurrenceTypeRepository.findById(1L).get();

        Occurrence occurrence = OccurrenceHelper.createValidOccurrence();
        occurrence.setUser(user);
        occurrence.setOccurrenceStatus(null);
        occurrence.setOccurrenceType(type);
        occurrence.setModerator(null);

        return occurrence;
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testAcceptReports() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1012L).get();

        occurrenceService.acceptReports(occurrence.getId(), user);

        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.REPORT_ACCEPTED);
        assertThat(occurrence.getReportsCount()).isEqualTo(3L);

        List<Report> reports = reportRepository.findAllByOccurrenceIdAndActiveIsTrue(occurrence.getId());

        assertThat(reports).hasSize(3);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testRejectReports() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1012L).get();

        occurrenceService.rejectReports(occurrence.getId(), user);

        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.APPROVED);
        assertThat(occurrence.getReportsCount()).isEqualTo(0L);

        List<Report> reports = reportRepository.findAllByOccurrenceIdAndActiveIsTrue(occurrence.getId());

        assertThat(reports).hasSize(0);
    }

    @Test
    public void testGetReportedOccurrence() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1012L).get();

        assertThat(occurrenceService.getReportedOccurrence(occurrence.getId(), user)).isEqualTo(occurrence);
    }

    @Test
    public void testGetReportedOccurrenceNotFound() {
        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> occurrenceService.getReportedOccurrence(991290129L, user)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    public void testGetReportedOccurrenceNotApproved() {
        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.getReportedOccurrence(1000L, user)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    public void testGetReportedOccurrenceNotAdmin() {
        User user = userRepository.findById(1001L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.getReportedOccurrence(1012L, user)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    public void testGetReportedOccurrenceNoReports() {
        User user = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> occurrenceService.getReportedOccurrence(1018L, user)
        ).withMessage(messages.get("error.forbidden"));
    }

}
