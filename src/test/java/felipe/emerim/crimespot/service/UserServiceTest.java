package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.dao.UserDao;
import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.ChangePasswordForm;
import felipe.emerim.crimespot.form.UserSearchForm;
import felipe.emerim.crimespot.helper.UserHelper;
import felipe.emerim.crimespot.repository.OccurrenceRepository;
import felipe.emerim.crimespot.repository.OccurrenceStatusRepository;
import felipe.emerim.crimespot.repository.RoleRepository;
import felipe.emerim.crimespot.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class UserServiceTest extends BaseServiceTest {
    @MockBean
    UserDao userDao;
    @Autowired
    private OccurrenceRepository occurrenceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private OccurrenceStatusRepository occurrenceStatusRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testEncryptPassword() {
        User user = UserHelper.createUser();
        String originalPassword = user.getPassword();
        userService.encryptPassword(user);

        assertThat(passwordEncoder.matches(originalPassword, user.getPassword())).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateUser() {
        User user = UserHelper.createValidUser();

        user = userService.save(null, user);
        user = userRepository.findById(user.getId()).get();

        assertThat(user.getId()).isNotNull();
        assertThat(user.isActive()).isTrue();
        assertThat(user.getRoles()).hasSize(1);
        assertThat(user.hasRole(RoleEnum.ROLE_USER)).isTrue();

        assertThat(userRepository.findById(user.getId()).isPresent()).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateUserExistingName() {
        User user = UserHelper.createValidUser();
        user.setUsername("admin");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.save(null, user)
        ).withMessage("User already exists");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateUserExistingCpf() {
        User user = UserHelper.createValidUser();
        user.setCpf("88547082085");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.save(null, user)
        ).withMessage("User already exists");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateUserExistingEmail() {
        User user = UserHelper.createValidUser();
        user.setEmail("admin@gmail.com");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.save(null, user)
        ).withMessage("User already exists");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUser() {
        User user = userRepository.findById(1001L).get();
        Long userId = user.getId();

        user.addRole(roleRepository.findByRole(RoleEnum.ROLE_ADMIN).get());
        user.setUsername("test_user_2");
        user.setEmail("test_user_2@gmail.com");
        entityManager.detach(user);

        userService.save(user, user);
        user = userRepository.findById(userId).get();

        // Roles should not be updated this way
        assertThat(user.getRoles()).hasSize(2);
        assertThat(user.hasRole(RoleEnum.ROLE_USER)).isTrue();
        assertThat(user.hasRole(RoleEnum.ROLE_MODERATOR)).isTrue();

        assertThat(user.getUsername()).isEqualTo("test_user_2");
        assertThat(user.getEmail()).isEqualTo("test_user_2@gmail.com");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUserExistingName() {
        User user = userRepository.findById(1001L).get();

        entityManager.detach(user);

        user.setUsername("admin");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.save(user, user)
        ).withMessage("User already exists");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUserExistingCpf() {
        User user = userRepository.findById(1001L).get();

        entityManager.detach(user);

        user.setCpf("88547082085");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.save(user, user)
        ).withMessage("User already exists");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUserExistingEmail() {
        User user = userRepository.findById(1001L).get();

        entityManager.detach(user);

        user.setEmail("admin@gmail.com");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.save(user, user)
        ).withMessage("User already exists");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUserNotExists() {
        User user = userRepository.findById(1001L).get();

        entityManager.detach(user);
        user.setId(40000L);

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> userService.save(user, user)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdateUserNotCurrentUser() {
        User user = userRepository.findById(1001L).get();
        User wrongUser = userRepository.findById(1000L).get();

        entityManager.detach(user);

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.save(wrongUser, user)
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserAdminAdmin() {
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggle(admin, admin.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserAdminModerator() {
        User admin = userRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();

        userService.toggle(admin, moderator.getId());
        moderator = userRepository.findById(moderator.getId()).get();

        assertThat(moderator.isActive()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserAdminUser() {
        User admin = userRepository.findById(1000L).get();
        User user = userRepository.findById(1002L).get();

        userService.toggle(admin, user.getId());
        user = userRepository.findById(user.getId()).get();

        assertThat(user.isActive()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserModeratorAdmin() {
        User moderator = userRepository.findById(1001L).get();
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggle(moderator, admin.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserModeratorModerator() {
        User moderator = userRepository.findById(1001L).get();
        User anotherUser = userRepository.findById(1002L).get();

        anotherUser.addRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());
        userRepository.save(anotherUser);

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggle(moderator, anotherUser.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserModeratorUser() {
        User moderator = userRepository.findById(1001L).get();
        User user = userRepository.findById(1002L).get();

        userService.toggle(moderator, user.getId());

        user = userRepository.findById(user.getId()).get();

        assertThat(user.isActive()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserUserAdmin() {
        User user = userRepository.findById(1001L).get();
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggle(user, admin.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserUserModerator() {
        User user = userRepository.findById(1002L).get();
        User moderator = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggle(user, moderator.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserUserUser() {
        User user = userRepository.findById(1002L).get();
        User anotherUser = userRepository.findById(1003L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggle(user, anotherUser.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleUserNotExists() {
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> userService.toggle(admin, 40000L)
        ).withMessage(messages.get("error.notFound"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorResetInAnalysisOccurrences() {
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();
        User admin = userRepository.findById(1000L).get();

        occurrence.setModerator(moderator);
        occurrence.setOccurrenceStatus(occurrenceStatusRepository.findByDescription(OccurrenceStatusEnum.IN_ANALYSIS));
        occurrenceRepository.save(occurrence);

        userService.toggle(admin, moderator.getId());

        assertThat(occurrence.getModerator()).isNull();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.WAITING_ANALYSIS);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorAdd() {
        User admin = userRepository.findById(1000L).get();
        User user = userRepository.findById(1002L).get();

        userService.toggleModerator(admin, user.getId());
        user = userRepository.findById(user.getId()).get();

        assertThat(user.getRoles()).hasSize(2);
        assertThat(user.hasRole(RoleEnum.ROLE_USER)).isTrue();
        assertThat(user.hasRole(RoleEnum.ROLE_MODERATOR)).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorRemove() {
        User admin = userRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();

        userService.toggleModerator(admin, moderator.getId());

        moderator = userRepository.findById(moderator.getId()).get();

        assertThat(moderator.getRoles()).hasSize(1);
        assertThat(moderator.hasRole(RoleEnum.ROLE_USER)).isTrue();
        assertThat(moderator.hasRole(RoleEnum.ROLE_MODERATOR)).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorNonExisting() {
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> userService.toggleModerator(admin, 40000L)
        ).withMessage(messages.get("error.notFound"));

    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorNonAdmin() {
        User moderator = userRepository.findById(1001L).get();
        User user = userRepository.findById(1002L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggleModerator(moderator, user.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorTargetIsAdmin() {
        User moderator = userRepository.findById(1001L).get();
        User admin = userRepository.findById(1000L).get();

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.toggleModerator(moderator, admin.getId())
        ).withMessage(messages.get("error.forbidden"));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testToggleModeratorWhenTargetIsModeratorInAnalysisOccurrencesAreReset() {
        Occurrence occurrence = occurrenceRepository.findById(1000L).get();
        User moderator = userRepository.findById(1001L).get();
        User admin = userRepository.findById(1000L).get();

        occurrence.setModerator(moderator);
        occurrence.setOccurrenceStatus(occurrenceStatusRepository.findByDescription(OccurrenceStatusEnum.IN_ANALYSIS));
        occurrenceRepository.save(occurrence);

        userService.toggleModerator(admin, moderator.getId());

        assertThat(occurrence.getModerator()).isNull();
        assertThat(occurrence.getOccurrenceStatus().getDescription()).isEqualTo(OccurrenceStatusEnum.WAITING_ANALYSIS);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testFindNearestBySearchTermAndRole() {
        UserSearchForm form = new UserSearchForm();
        User user = userRepository.findById(1000L).get();

        userService.findNearestBySearchTermAndRole(user, form);

        verify(userDao).findNearestBySearchTermAndRole(user, form);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testFindNearestBySearchTermAndRoleCurrentUserRoleUser() {
        UserSearchForm form = new UserSearchForm();
        User user = userRepository.findById(1000L).get();
        user.removeRole(roleRepository.findByRole(RoleEnum.ROLE_ADMIN).get());
        user.removeRole(roleRepository.findByRole(RoleEnum.ROLE_MODERATOR).get());

        assertThatExceptionOfType(ForbiddenException.class).isThrownBy(
                () -> userService.findNearestBySearchTermAndRole(user, form)
        ).withMessage(messages.get("error.forbidden"));

        verify(userDao, never()).findNearestBySearchTermAndRole(user, form);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testChangePassword() {
        ChangePasswordForm form = new ChangePasswordForm();

        User user = userRepository.findById(1000L).get();
        entityManager.detach(user);

        form.setOriginalPassword("user");
        form.setNewPassword("new_pass");
        form.setNewPasswordConfirm("new_pass");

        userService.changePassword(user, form);
        user = userRepository.findById(user.getId()).get();

        assertThat(passwordEncoder.matches("new_pass", user.getPassword())).isTrue();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testChangePasswordNotFound() {
        ChangePasswordForm form = new ChangePasswordForm();

        User user = userRepository.findById(1000L).get();
        entityManager.detach(user);

        user.setId(40000L);

        form.setOriginalPassword("user");
        form.setNewPassword("new_pass");
        form.setNewPasswordConfirm("new_pass");

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(
                () -> userService.changePassword(user, form)
        ).withMessage(messages.get("error.notFound"));

    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testChangePasswordNotMatched() {
        ChangePasswordForm form = new ChangePasswordForm();

        User user = userRepository.findById(1000L).get();
        entityManager.detach(user);

        form.setOriginalPassword("wrong_pass");
        form.setNewPassword("new_pass");
        form.setNewPasswordConfirm("new_pass");

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> userService.changePassword(user, form)
        ).withMessage("Invalid password supplied");
    }
}
