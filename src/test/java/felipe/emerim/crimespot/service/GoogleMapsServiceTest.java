package felipe.emerim.crimespot.service;

import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import felipe.emerim.crimespot.domain.GeocodingAddress;
import felipe.emerim.crimespot.helper.GeocodingResultsHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class GoogleMapsServiceTest extends BaseServiceTest {
    @Autowired
    private GoogleMapsService googleMapsService;
    @MockBean
    GeoApiContext geoApiContext;
    @MockBean
    GoogleMapsWrapperService googleMapsWrapperService;

    @Test
    public void testGeocode() throws InterruptedException, ApiException, IOException {
        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenReturn(GeocodingResultsHelper.createGeocodingResults());

        GeocodingAddress address = googleMapsService.geocode("sadas");

        assertThat(address.getCity()).isEqualTo("Canoas");
        assertThat(address.getCountry()).isEqualTo("Brasil");
        assertThat(address.getFormattedAddress())
                .isEqualTo("R. Tramandaí, 588 - Estância Velha, Canoas - RS, 92032-360, Brasil");
        assertThat(address.getLatitude()).isEqualTo(-29.907109);
        assertThat(address.getLongitude()).isEqualTo(-51.14332650000001);
        assertThat(address.getNumber()).isEqualTo("588");
        assertThat(address.getPostalCode()).isEqualTo("92032-360");
        assertThat(address.getStreet()).isEqualTo("Rua Tramandaí");
        assertThat(address.getNeighborhood()).isEqualTo("Estância Velha");
        assertThat(address.getState()).isEqualTo("Rio Grande do Sul");
    }

    @Test
    public void testGeocodeMissingSomeComponents() throws InterruptedException, ApiException, IOException {
        GeocodingResult result = GeocodingResultsHelper.createGeocodingResult();
        result.addressComponents = new AddressComponent[]{
                result.addressComponents[2],
                result.addressComponents[5],
                result.addressComponents[6],
        };

        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenReturn(new GeocodingResult[]{result});

        GeocodingAddress address = googleMapsService.geocode("sadas");

        assertThat(address.getCity()).isNull();
        assertThat(address.getCountry()).isEqualTo("Brasil");
        assertThat(address.getLatitude()).isEqualTo(-29.907109);
        assertThat(address.getLongitude()).isEqualTo(-51.14332650000001);
        assertThat(address.getNumber()).isNull();
        assertThat(address.getPostalCode()).isEqualTo("92032-360");
        assertThat(address.getStreet()).isNull();
        assertThat(address.getState()).isNull();
    }

    @Test
    public void testGeocodeMissingCityFromLocalityComponent() throws InterruptedException, ApiException, IOException {
        GeocodingResult result = GeocodingResultsHelper.createGeocodingResult();
        result.addressComponents[3].types = new AddressComponentType[]{AddressComponentType.LOCALITY};

        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenReturn(new GeocodingResult[]{result});

        GeocodingAddress address = googleMapsService.geocode("sadas");

        assertThat(address.getCity()).isEqualTo("Canoas");
        assertThat(address.getCountry()).isEqualTo("Brasil");
        assertThat(address.getFormattedAddress())
                .isEqualTo("R. Tramandaí, 588 - Estância Velha, Canoas - RS, 92032-360, Brasil");
        assertThat(address.getLatitude()).isEqualTo(-29.907109);
        assertThat(address.getLongitude()).isEqualTo(-51.14332650000001);
        assertThat(address.getNumber()).isEqualTo("588");
        assertThat(address.getPostalCode()).isEqualTo("92032-360");
        assertThat(address.getStreet()).isEqualTo("Rua Tramandaí");
        assertThat(address.getState()).isEqualTo("Rio Grande do Sul");
    }

    @Test
    public void testGeocodeNoResults() throws InterruptedException, ApiException, IOException {
        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenReturn(new GeocodingResult[]{});

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> googleMapsService.geocode("sadas")
        ).withMessage("Could not resolve address");
    }

    @Test
    public void testGeocodeInterruptedException() throws InterruptedException, ApiException, IOException {
        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenThrow(InterruptedException.class);

        assertThatExceptionOfType(RuntimeException.class).isThrownBy(
                () -> googleMapsService.geocode("sadas")
        ).withMessage("There was an error fetching the address");
    }

    @Test
    public void testGeocodeApiException() throws InterruptedException, ApiException, IOException {
        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenThrow(ApiException.class);

        assertThatExceptionOfType(RuntimeException.class).isThrownBy(
                () -> googleMapsService.geocode("sadas")
        ).withMessage("There was an error fetching the address");
    }

    @Test
    public void testGeocodeIOException() throws InterruptedException, ApiException, IOException {
        when(googleMapsWrapperService.geocode(any(String.class)))
                .thenThrow(IOException.class);

        assertThatExceptionOfType(RuntimeException.class).isThrownBy(
                () -> googleMapsService.geocode("sadas")
        ).withMessage("There was an error fetching the address");
    }
}
