package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.GeocodingAddress;
import felipe.emerim.crimespot.helper.AddressHelper;
import felipe.emerim.crimespot.helper.GeocodingResultsHelper;
import felipe.emerim.crimespot.repository.AddressRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class AddressServiceTest extends BaseServiceTest {

    @Autowired
    private AddressService addressService;

    @MockBean
    GeocodingService geocodingService;

    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void testSearch() {
        when(geocodingService.geocode(any(String.class))).thenReturn(null);

        this.addressService.search("sadas");

        verify(geocodingService).geocode("sadas");
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testSave() {
        when(geocodingService.geocode(any(String.class)))
                .thenReturn(GeocodingResultsHelper.createGoogleMapsAddress());

        Address address = AddressHelper.createAddress();

        Address persistedAddress = addressService.save(address);
        persistedAddress = addressRepository.findById(persistedAddress.getId()).get();

        assertThat(persistedAddress.getCity()).isEqualTo("Canoas");
        assertThat(persistedAddress.getCountry()).isEqualTo("Brasil");
        assertThat(persistedAddress.getLatitude()).isEqualTo(-29.907109);
        assertThat(persistedAddress.getLongitude()).isEqualTo(-51.14332650000001);
        assertThat(persistedAddress.getFormattedAddress())
                .isEqualTo("Rua Tramandaí, 588 - Estância Velha, Canoas - Rio Grande do Sul, 92032-360, Brasil");
        assertThat(persistedAddress.getNumber()).isEqualTo("588");
        assertThat(persistedAddress.getPostalCode()).isEqualTo("92032-360");
        assertThat(persistedAddress.getComplement()).isEqualTo("Casa");
        assertThat(persistedAddress.getNeighborhood()).isEqualTo("Estância Velha");
        assertThat(persistedAddress.getState()).isEqualTo("Rio Grande do Sul");
        assertThat(persistedAddress.getStreet()).isEqualTo("Rua Tramandaí");
        assertThat(persistedAddress.getUser()).isNull();
        assertThat(persistedAddress.getOccurrence()).isNull();
        assertThat(address.getId()).isNotNull();

        assertThat(addressRepository.findById(address.getId()).get()).isEqualTo(address);

    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testSaveMissingSomeComponents() {

        GeocodingAddress geocodingAddress = GeocodingAddress
                .builder()
                .latitude(-29.907109)
                .longitude(-51.14332650000001)
                .build();

        when(geocodingService.geocode(any(String.class)))
                .thenReturn(geocodingAddress);

        Address address = AddressHelper.createAddress();

        Address persistedAddress = addressService.save(address);
        persistedAddress = addressRepository.findById(persistedAddress.getId()).get();

        assertThat(persistedAddress.getCity()).isEqualTo("Canoas");
        assertThat(persistedAddress.getCountry()).isEqualTo("Brasil");
        assertThat(persistedAddress.getLatitude()).isEqualTo(-29.907109);
        assertThat(persistedAddress.getLongitude()).isEqualTo(-51.14332650000001);
        assertThat(persistedAddress.getFormattedAddress())
                .isEqualTo("Rua Tramandaí, 588 - Estância Velha, Canoas - Rio Grande do Sul, 92032-360, Brasil");
        assertThat(persistedAddress.getNumber()).isEqualTo("588");
        assertThat(persistedAddress.getPostalCode()).isEqualTo("92032-360");
        assertThat(persistedAddress.getComplement()).isEqualTo("Casa");
        assertThat(persistedAddress.getState()).isEqualTo("Rio Grande do Sul");
        assertThat(persistedAddress.getNeighborhood()).isEqualTo("Estância Velha");
        assertThat(persistedAddress.getStreet()).isEqualTo("Rua Tramandaí");
        assertThat(persistedAddress.getUser()).isNull();
        assertThat(persistedAddress.getOccurrence()).isNull();
        assertThat(address.getId()).isNotNull();

        assertThat(addressRepository.findById(address.getId()).get()).isEqualTo(address);

    }

    @Test
    public void testFindById() {
        Address address = this.addressService.findById(1000L).get();
        Address sameAddress = this.addressRepository.findById(1000L).get();

        assertThat(address).isEqualTo(sameAddress);
    }

    @Test
    public void testFindByIdNotExists() {
        assertThat(this.addressService.findById(4000L).isPresent()).isFalse();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDelete() {
        Address address = addressRepository.findById(1000L).get();
        addressService.delete(address);
        assertThat(addressRepository.findById(1000L).isPresent()).isFalse();
    }

}
