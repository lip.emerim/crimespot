package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SuppressWarnings("OptionalGetWithoutIsPresent")
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class UserDetailsImplServiceTest extends BaseServiceTest {
    @Autowired
    private UserDetailsImplService userDetailsImplService;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testLoadUserByUsername() {
        User user = userRepository.findById(1000L).get();

        UserImpl userDetails = userDetailsImplService.loadUserByUsername(user.getUsername());

        assertThat(userDetails.getUser()).isEqualTo(user);
        assertThat(userDetails.getUsername()).isEqualTo(user.getUsername());
        assertThat(userDetails.getPassword()).isEqualTo(user.getPassword());

        //This user has every role
        for (GrantedAuthority authority : userDetails.getAuthorities()) {
            assertThat(Arrays.stream(RoleEnum.values())
                    .anyMatch((r) -> r.name().equals(authority.getAuthority()))).isTrue();
        }
    }

    @Test
    public void testLoadByUsernameNotExists() {
        assertThatExceptionOfType(UsernameNotFoundException.class)
                .isThrownBy(() -> userDetailsImplService.loadUserByUsername("does_not_exist"))
                .withMessage("couldn't find does_not_exist!");
    }
}
