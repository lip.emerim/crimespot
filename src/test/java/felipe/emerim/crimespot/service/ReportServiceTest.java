package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.Report;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.exception.ReportAlreadyExistsException;
import felipe.emerim.crimespot.repository.OccurrenceRepository;
import felipe.emerim.crimespot.repository.ReportRepository;
import felipe.emerim.crimespot.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class ReportServiceTest extends BaseServiceTest {
    @Autowired
    protected ReportRepository reportRepository;
    @Autowired
    ReportService reportService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    OccurrenceRepository occurrenceRepository;

    @Test
    public void testGetActiveReportsByOccurrenceId() {
        List<Report> reports = reportService.getActiveReportsByOccurrenceId(1012L);

        assertThat(
                reports.stream().allMatch((r) -> r.getOccurrence().getId().equals(1012L))
        ).isTrue();
        assertThat(reports).hasSize(3);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDeactivateAllByOccurrenceId() throws InterruptedException {
        List<Report> reports = reportRepository.findAllByOccurrenceIdAndActiveIsTrue(1012L);
        assertThat(reports).hasSize(3);

        reportService.deactivateAllByOccurrenceId(1012L);

        reports = reportRepository.findAllByOccurrenceIdAndActiveIsTrue(1012L);
        assertThat(reports).isEmpty();
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testCreateReport() {
        User user = userRepository.findById(1000L).get();
        Occurrence occurrence = occurrenceRepository.findById(1030L).get();
        String description = "This occurrence is fake, please remove it from your system";
        Report report = reportService.createReport(user, occurrence.getId(), description);

        assertThat(report).isEqualTo(reportRepository.findById(report.getId()).get());
        assertThat(report.getUser()).isEqualTo(user);
        assertThat(report.getOccurrence()).isEqualTo(occurrence);
        assertThat(report.getDescription()).isEqualTo(description);

        assertThat(occurrence.getReportsCount()).isEqualTo(1);
    }

    @Test
    public void testCreateReportAlreadyReported() {
        User user = userRepository.findById(1008L).get();
        Occurrence occurrence = occurrenceRepository.findById(1012L).get();
        String description = "This occurrence is fake, please remove it from your system";

        assertThatExceptionOfType(ReportAlreadyExistsException.class).isThrownBy(() -> {
            reportService.createReport(user, occurrence.getId(), description);
        });
    }

    @Test
    public void testCreateReportOccurrenceNotFound() {
        User user = userRepository.findById(1008L).get();
        String description = "This occurrence is fake, please remove it from your system";

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
            reportService.createReport(user, 11111L, description);
        });
    }

    @Test
    public void testExistsByOccurrenceIdAndUserIdAndActiveIsTrueOk() {
        assertThat(
                reportService.existsByOccurrenceIdAndUserIdAndActiveIsTrue(1012L, 1008L)
        ).isTrue();
    }

    @Test
    public void testExistsByOccurrenceIdAndUserIdAndActiveIsTrueNotOk() {
        assertThat(
                reportService.existsByOccurrenceIdAndUserIdAndActiveIsTrue(1005L, 1004L)
        ).isTrue();
    }
}
