package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.OccurrenceType;
import felipe.emerim.crimespot.repository.OccurrenceTypeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OccurrenceTypeServiceTest extends BaseServiceTest {
    @Autowired
    private OccurrenceTypeService occurrenceTypeService;

    @Autowired
    private OccurrenceTypeRepository occurrenceTypeRepository;

    @Test
    public void findAll() {
        List<OccurrenceType> statuses = occurrenceTypeRepository.findAll();

        List<OccurrenceType> statusList = occurrenceTypeService.findAll();

        assertThat(statuses).isEqualTo(statusList);
    }
}
