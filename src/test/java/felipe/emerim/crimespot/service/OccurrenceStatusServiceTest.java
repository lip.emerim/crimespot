package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.OccurrenceStatus;
import felipe.emerim.crimespot.repository.OccurrenceStatusRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OccurrenceStatusServiceTest extends BaseServiceTest {

    @Autowired
    private OccurrenceStatusService occurrenceStatusService;

    @Autowired
    private OccurrenceStatusRepository occurrenceStatusRepository;

    @Test
    public void findAll() {
        List<OccurrenceStatus> statuses = occurrenceStatusRepository.findAll();

        List<OccurrenceStatus> statusList = occurrenceStatusService.findAll();

        assertThat(statuses).isEqualTo(statusList);
    }
}
