package felipe.emerim.crimespot.helper;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.User;

import java.time.LocalDateTime;

public class AddressHelper {
    public static Address createAddress() {
        return Address
                .builder()
                .active(true)
                .city("Canoas")
                .complement("Casa")
                .country("Brasil")
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .formattedAddress("lalala")
                .latitude(14)
                .longitude(45)
                .number("588")
                .postalCode("92032-360")
                .state("Rio Grande do Sul")
                .neighborhood("Estância Velha")
                .street("Rua Tramandaí")
                .build();
    }

    public static Address createValidUserAddress() {
        Address address = createAddress();
        User user = UserHelper.createUser();
        address.setUser(user);
        return address;
    }

    public static Address createValidOccurrenceAddress() {
        Address address = createAddress();
        Occurrence occurrence = OccurrenceHelper.createOccurrence();
        address.setOccurrence(occurrence);
        return address;
    }
}
