package felipe.emerim.crimespot.helper;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.Role;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.RoleEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class UserHelper {

    public static Role createRole(RoleEnum roleType) {
        return Role
                .builder()
                .role(roleType)
                .build();
    }

    public static User createUser() {

        Set<Role> roles = new HashSet<>();
        roles.add(createRole(RoleEnum.ROLE_USER));
        roles.add(createRole(RoleEnum.ROLE_MODERATOR));
        roles.add(createRole(RoleEnum.ROLE_ADMIN));

        return User
                .builder()
                .active(true)
                .cpf("11765223024")
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("java@gmail.com")
                .name("test_user")
                .username("test_user")
                .searchRadius(1D)
                .password("123456")
                .roles(roles)
                .build();
    }

    public static User createValidUser() {
        Address address = AddressHelper.createAddress();
        User user = createUser();

        user.setAddress(address);

        return user;
    }
}
