package felipe.emerim.crimespot.helper;

import felipe.emerim.crimespot.domain.File;
import felipe.emerim.crimespot.domain.Occurrence;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

import java.io.IOException;

public class FileHelper {

    public static final String PDF_PATH = "static/occurrenceDocument/validOccurrenceDocument.pdf";
    public static final String PNG_PATH = "static/occurrenceDocument/validOccurrenceDocument.png";
    public static final String JPG_PATH = "static/occurrenceDocument/validOccurrenceDocument.jpg";
    public static final String TXT_PATH = "static/occurrenceDocument/invalidOccurrenceDocument.txt";

    public static Resource loadFile(String path) {
        return new ClassPathResource(path);
    }

    public static File createPDFFile() throws IOException {
        return createFile(PDF_PATH, MediaType.APPLICATION_PDF);
    }

    public static File createJPGFile() throws IOException {
        return createFile(JPG_PATH, MediaType.IMAGE_JPEG);
    }

    public static File createPNGFile() throws IOException {
        return createFile(PNG_PATH, MediaType.IMAGE_PNG);
    }

    public static File createTXTFile() throws IOException {
        return createFile(TXT_PATH, MediaType.TEXT_PLAIN);
    }

    protected static File createFile(String path, MediaType mediaType) throws IOException {
        Resource resource = loadFile(path);
        java.io.File document = resource.getFile();
        File file = new File();
        file.setContentType(mediaType.toString());
        byte[] fileBytes = FileUtils.readFileToByteArray(document);
        file.setFileContent(org.apache.commons.codec.binary.Base64.encodeBase64(fileBytes));

        return file;
    }

    public static File createValidPDFFile() throws IOException {
        File file = createPDFFile();
        return createValidFile(file);

    }

    public static File createValidPNGFile() throws IOException {
        File file = createPNGFile();
        return createValidFile(file);

    }

    public static File createValidJPGFile() throws IOException {
        File file = createJPGFile();
        return createValidFile(file);

    }

    protected static File createValidFile(File file) {
        Occurrence occurrence = OccurrenceHelper.createOccurrence();
        file.setOccurrence(occurrence);

        return file;
    }
}
