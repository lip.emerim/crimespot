package felipe.emerim.crimespot.helper;

import felipe.emerim.crimespot.domain.Report;

import java.io.IOException;
import java.time.LocalDateTime;

public class ReportHelper {
    public static Report createValidReport() throws IOException {
        return Report.builder()
                .description("Really long description lorem ipsum asadas")
                .createdAt(LocalDateTime.now())
                .id(1000L)
                .occurrence(OccurrenceHelper.createValidOccurrence())
                .user(UserHelper.createValidUser())
                .updatedAt(LocalDateTime.now())
                .build();
    }
}
