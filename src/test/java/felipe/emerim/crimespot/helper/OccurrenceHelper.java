package felipe.emerim.crimespot.helper;

import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class OccurrenceHelper {
    public static OccurrenceStatus createOccurrenceStatus(OccurrenceStatusEnum status) {
        return OccurrenceStatus
                .builder()
                .description(status)
                .build();
    }

    public static OccurrenceType createOccurrenceType(OccurrenceTypeEnum type) {
        return OccurrenceType
                .builder()
                .description(type)
                .build();
    }

    public static Occurrence createOccurrence() {
        return Occurrence
                .builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .description("Dummy valid occurrence with a fine description")
                .title("Dummy occurrence")
                .isVerified(true)
                .moderatorComments("Some fine comments here")
                .occurrenceStatus(createOccurrenceStatus(OccurrenceStatusEnum.APPROVED))
                .occurrenceType(createOccurrenceType(OccurrenceTypeEnum.ARSON))
                .occurrenceDate(LocalDate.now())
                .build();
    }

    public static Occurrence createValidOccurrence() throws IOException {
        Occurrence occurrence = OccurrenceHelper.createOccurrence();
        File file = FileHelper.createPDFFile();
        User user = UserHelper.createUser();
        Address address = AddressHelper.createAddress();

        occurrence.setUser(user);
        occurrence.setModerator(user);
        occurrence.setAddress(address);
        occurrence.setOccurrenceDocument(file);

        return occurrence;
    }
}
