package felipe.emerim.crimespot.helper;

import felipe.emerim.crimespot.form.ReportCreateForm;

public class ReportCreateFormHelper {
    public static ReportCreateForm createValidForm() {
        return new ReportCreateForm("really cool report with more than 15 characters");
    }
}
