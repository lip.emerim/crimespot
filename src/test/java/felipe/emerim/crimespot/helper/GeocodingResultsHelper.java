package felipe.emerim.crimespot.helper;

import com.google.maps.model.*;
import felipe.emerim.crimespot.domain.GeocodingAddress;

public class GeocodingResultsHelper {
    public static GeocodingResult[] createGeocodingResults() {
        GeocodingResult[] geocodingResults = new GeocodingResult[1];

        geocodingResults[0] = createGeocodingResult();

        return geocodingResults;
    }

    public static GeocodingResult createGeocodingResult() {
        GeocodingResult geocodingResult = new GeocodingResult();

        geocodingResult.addressComponents = createAddressComponents();
        geocodingResult.formattedAddress = "R. Tramandaí, 588 - Estância Velha, Canoas - RS, 92032-360, Brasil";
        geocodingResult.geometry = createGeometry();
        geocodingResult.types = new AddressType[]{AddressType.STREET_ADDRESS};
        geocodingResult.partialMatch = false;
        geocodingResult.placeId = "ChIJM1eEZjFwGZUREsMHKTlR6e4";
        geocodingResult.plusCode = new PlusCode();
        geocodingResult.plusCode.globalCode = "582C3VV4+5M";
        geocodingResult.plusCode.compoundCode = "3VV4+5M Estância Velha, Canoas - RS, Brasil";

        return geocodingResult;
    }

    public static AddressComponent[] createAddressComponents() {
        AddressComponent[] addressComponents = new AddressComponent[7];

        AddressComponent street_number = new AddressComponent();
        street_number.shortName = "588";
        street_number.longName = "588";
        street_number.types = new AddressComponentType[]{AddressComponentType.STREET_NUMBER};

        addressComponents[0] = street_number;

        AddressComponent route = new AddressComponent();
        route.longName = "Rua Tramandaí";
        route.shortName = "R. Tramandaí";
        route.types = new AddressComponentType[]{AddressComponentType.ROUTE};

        addressComponents[1] = route;

        AddressComponent sublocality_1 = new AddressComponent();
        sublocality_1.longName = "Estância Velha";
        sublocality_1.shortName = "Estância Velha";
        sublocality_1.types = new AddressComponentType[]{
                AddressComponentType.POLITICAL,
                AddressComponentType.SUBLOCALITY,
                AddressComponentType.NEIGHBORHOOD,
                AddressComponentType.SUBLOCALITY_LEVEL_1,
        };

        addressComponents[2] = sublocality_1;

        AddressComponent admin_2 = new AddressComponent();
        admin_2.longName = "Canoas";
        admin_2.shortName = "Canoas";
        admin_2.types = new AddressComponentType[]{
                AddressComponentType.POLITICAL,
                AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2,
        };

        addressComponents[3] = admin_2;

        AddressComponent admin_1 = new AddressComponent();
        admin_1.longName = "Rio Grande do Sul";
        admin_1.shortName = "RS";
        admin_1.types = new AddressComponentType[]{
                AddressComponentType.POLITICAL,
                AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1,
        };

        addressComponents[4] = admin_1;

        AddressComponent country = new AddressComponent();
        country.longName = "Brasil";
        country.shortName = "BR";
        country.types = new AddressComponentType[]{
                AddressComponentType.POLITICAL,
                AddressComponentType.COUNTRY
        };

        addressComponents[5] = country;

        AddressComponent postalCode = new AddressComponent();
        postalCode.longName = "92032-360";
        postalCode.shortName = "92032-360";
        postalCode.types = new AddressComponentType[]{AddressComponentType.POSTAL_CODE};

        addressComponents[6] = postalCode;

        return addressComponents;
    }

    public static GeocodingAddress createGoogleMapsAddress() {
        return GeocodingAddress
                .builder()
                .city("Canoas")
                .country("Brasil")
                .formattedAddress("R. Tramandaí, 588 - Estância Velha, Canoas - RS, 92032-360, Brasil")
                .latitude(-29.907109)
                .longitude(-51.14332650000001)
                .number("588")
                .postalCode("92032-360")
                .neighborhood("Estância Velha")
                .state("Rio Grande do Sul")
                .street("Rua Tramandaí")
                .build();
    }

    public static Geometry createGeometry() {
        Geometry geometry = new Geometry();
        geometry.bounds = null;
        geometry.location = new LatLng(-29.907109, -51.14332650000001);
        geometry.locationType = LocationType.ROOFTOP;

        Bounds bounds = new Bounds();
        bounds.northeast = new LatLng(-29.9057600197085, -51.14197751970851);
        bounds.southwest = new LatLng(-29.90845798029149, -51.14467548029151);

        geometry.viewport = bounds;

        return geometry;
    }
}
