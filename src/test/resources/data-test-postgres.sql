-- Users addresses
INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1000, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Tramandaí, 165-287 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Tramandaí',
        null, -29.90934829911911, -51.14336807889483, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1001, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1002, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1003, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1004, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1005, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1006, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1007, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1008, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1009, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1010, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1011, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1012, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1013, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1014, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1015, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1016, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1017, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1018, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1019, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1020, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1021, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1022, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1023, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Farroupilha, 4545 - Mal. Rondon, Canoas - RS, 92020-475',
        'Marechal Rondon', 'Avenida Farroupilha',
        '4545', -29.916073, -51.1658464, '92020-475', 'Sala 5001', now(), now(), true);

--Occurrences addresses
INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1024, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1025, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Farroupilha, 4545 - Mal. Rondon, Canoas - RS, 92020-475',
        'Marechal Rondon', 'Avenida Farroupilha',
        '4545', -29.916073, -51.1658464, '92020-475', 'Sala 5001', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1026, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1027, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Farroupilha, 4545 - Mal. Rondon, Canoas - RS, 92020-475',
        'Marechal Rondon', 'Avenida Farroupilha',
        '4545', -29.916073, -51.1658464, '92020-475', 'Sala 5001', now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1028, 'Brasil', 'Rio Grande do Sul',
        'Porto Alegre', 'R. Alm. Barroso, 735 - Floresta, Porto Alegre - RS, 90220-021, Brasil',
        'Floresta', 'Rua Almirante Barroso',
        '735', -30.01819739999999, -51.2043776197085, '90220-021', 'Sala 799', now(), now(), true);



INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1029, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. A.J. Renner, 1912 - Estancia Velha, Canoas - RS, 92030340, Brasil',
        'Estância Velha', 'Avenida A.J. Renner',
        null, -29.908861899999998, -51.1446768, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1030, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Xangri Lá, 258-100 - Estância Velha, Canoas - RS, 92032-370',
        'Estância Velha', 'Rua Xangri-lá',
        null, -29.910443926311512, -51.14491297262808, '92032-370', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1031, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Noiva do Mar, 33-131 - Estância Velha, Canoas - RS, 92032-270',
        'Estância Velha', 'Rua Noiva do Mar',
        null, -29.907765707657827, -51.14391876853934, '92032-270', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1032, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Tramandaí, 700-640 - Estância Velha, Canoas - RS, 92032-360',
        'Estância Velha', 'Rua Tramandaí',
        null, -29.9062771991804, -51.14230148496761, '92032-360', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1033, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Imbé - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Imbé',
        null, -29.91209280692048, -51.14326918653794, '92032-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1034, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Dezessete de Abril - Guajuviras, Canoas - RS, 92415-000',
        'Guajurivas', 'Avenida Dezessete de Abril',
        null, -29.90358307950271, -51.14220203599073, '92415-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1035, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Camboatás - Igara, Canoas - RS, 92412-230',
        'Igara', 'Rua Camboatás',
        null, -29.904100269947723, -51.147335541867065, '92412-230', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1036, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Rua Osvaldo Optiz, 456-236 - Estância Velha, Canoas - RS, 92030-240',
        'Estância Velha', 'Rua Osvaldo Optiz',
        null, -29.91643591104885, -51.14941619263506, '92030-240', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1037, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Dr. Severo da Silva, 1649-1755 - Estância Velha, Canoas - RS, 92025-730',
        'Estância Velha', 'Avenida Doutor Severo da Silva',
        null, -29.91304677413017, -51.14986133155425, '92025-730', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1038, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Ernesto da Silva Rocha, 1298-1200 - Estância Velha, Canoas - RS, 92030-490',
        'Estância Velha', 'Rua Ernesto da Silva Rocha',
        null, -29.91394862245608, -51.14294850518709, '92030-490', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1039, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Cidreira, 699-855 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Cidreira',
        null, -29.916131046353062, -51.14464332535685, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1040, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Albatroz, 170-82 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Albatroz',
        null, -29.913005061551374, -51.145145615085724, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1041, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Nordeste, 214-28 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Nordeste',
        null, -29.91444522985279, -51.14481806559261, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1042, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Alexandre de Gusmão, 1446-1334 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Alexandre de Gusmão',
        null, -29.912462036276185, -51.141977379162554, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1043, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Dr. Sezefredo Azambuja Vieira, 370-306 - Olaria, Canoas - RS, 92020-020',
        'Estância Velha', 'Avenida Doutor Sezefredo Azambuja Vieira',
        null, -29.91923898424616, -51.141927934030214, '92020-020', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1044, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. das Azaléias, 246-502 - Igara, Canoas - RS, 92410-520',
        'Igara', 'Rua das Azaléias',
        null, -29.89986463666342, -51.15512051996068, '92410-520', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1045, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. dos Marmelos - Igara, Canoas - RS, 92412-505',
        'Igara', 'Rua dos Marmelos',
        null, -29.899172048812698, -51.1483060306532, '92412-505', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1046, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Rua Andre Luiz dos Conceição, 15 - Guajuviras, Canoas - RS, 92415-000',
        'Guajurivas', 'Rua Andre Luiz dos Conceição',
        null, -29.90269887435243, -51.1316712804365, '92415-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1047, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Dezessete de Abril, 593-651 - Guajuviras, Canoas - RS, 92415-000',
        'Guajuviras', 'Avenida Dezessete de Abril',
        null, -29.90037383590756, -51.138697414701284, '92415-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1048, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Dra. Maria Zélia Carneiro de Figueiredo, 870 - Igara, Canoas - RS, 92412-240',
        'Igara', 'Rua Dra. Maria Zélia Carneiro de Figueiredo',
        870, -29.90008760940626, -51.14982695501166, '92412-240', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1049, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Camboatás, 543-581 - Igara, Canoas - RS, 92412-230',
        'Igara', 'Rua Camboatás',
        null, -29.900821505738143, -51.14641196401114, '92412-230', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1050, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Boqueirão, 2725-2787 - Estância Velha, Canoas - RS, 92032-040',
        'Estância Velha', 'Avenida Boqueirão',
        null, -29.905475562460982, -51.14855296207988, '92032-040', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1051, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 3110 - Nossa Sra. das Gracas, Canoas - RS, 92030-010',
        'Nossa Senhora das Gracas', 'Avenida Santos Ferreira',
        3110, -29.92728552632447, -51.14794249443753, '92030-010', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1052, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 3854-3810 - Mal. Rondon, Canoas - RS, 92030-000',
        'Marechal Rondon', 'Avenida Santos Ferreira',
        null, -29.92423833486141, -51.14085386750127, '92030-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1053, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 1871-1875 - Mal. Rondon, Canoas - RS, 92025-580',
        'Marechal Rondon', 'Avenida Santos Ferreira',
        null, -29.92761234812517, -51.16197290312615, '92025-580', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1054, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 3001 - Mal. Rondon, Canoas - RS, 92027-025',
        'Marechal Rondon', 'Avenida Santos Ferreira',
        null, -29.928370544334683, -51.15082771071072, '92027-025', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1055, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 2624-2494 - Mal. Rondon, Canoas - RS, 92030-000',
        'Marechal Rondon', 'Avenida Santos Ferreira',
        null, -29.928130693636948, -51.15492684725199, '92030-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1056, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Gáspar Lemos, 213-299 - Nossa Sra. das Gracas, Canoas - RS, 92025-500',
        'Nossa Senhora das Gracas', 'Rua Gáspar Lemos',
        null, -29.93013821089306, -51.1617622976492, '92025-500', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1057, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Pedro Álvares Cabral, 310-100 - Nossa Sra. das Gracas, Canoas - RS, 92025-570',
        'Nossa Senhora das Gracas', 'Rua Pedro Álvares Cabral',
        null, -29.930063794559654, -51.16386622930879, '92025-570', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1058, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Rua Santa Terezinha, 548-476 - Nossa Sra. das Gracas, Canoas - RS, 92025-620',
        'Nossa Senhora das Gracas', 'Rua Santa Terezinha',
        null, -29.93079179866915, -51.166139921433455, '92025-620', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1059, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Pôrto Seguro, 197 - Nossa Sra. das Gracas, Canoas - RS, 92025-590',
        'Nossa Senhora das Gracas', 'Rua Pôrto Seguro',
        197, -29.933000348756185, -51.15865379275524, '92025-590', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1060, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Vinte e Dois de Abril, 800-702 - Nossa Sra. das Gracas, Canoas - RS, 92025-630',
        'Nossa Senhora das Gracas', 'Rua Vinte e Dois de Abril',
        null, -29.932296808899718, -51.15448007811468, '92025-630', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1061, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Monte Castelo, 1226-1164 - Nossa Sra. das Gracas, Canoas - RS, 92025-370',
        'Nossa Senhora das Gracas', 'Rua Monte Castelo',
        null, -29.92755577262975, -51.16300671473424, '92025-370', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1062, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 1668-1502 - Mal. Rondon, Canoas - RS, 92025-415',
        'Marechal Rondon', 'Avenida Santos Ferreira',
        null, -29.925988998190117, -51.16388114765967, '92025-415', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1063, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Monte Castelo, 742-638 - Nossa Sra. das Gracas, Canoas - RS, 92025-370',
        'Nossa Senhora das Gracas', 'Rua Monte Castelo',
        null, -29.927857719975034, -51.167936434091175, '92025-370', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1064, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Cecília Meireles, 330-192 - Mal. Rondon, Canoas - RS, 92025-010',
        'Marechal Rondon', 'Rua Cecília Meireles',
        null, -29.92439158591123, -51.164461131135226, '92025-010', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1065, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Fernando Abbot, 106-2 - Nossa Sra. das Gracas, Canoas - RS, 92025-330',
        'Nossa Senhora das Gracas', 'Rua Fernando Abbot',
        null, -29.926854287988355, -51.17383716210922, '92025-330', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1066, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Santos Ferreira, 746-718 - Mal. Rondon, Canoas - RS, 92020-001',
        'Marechal Rondon', 'Avenida Santos Ferreira',
        null, -29.923515514766557, -51.17238532739891, '92020-001', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1067, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Dona Rafaela, 345-399 - Mal. Rondon, Canoas - RS, 92020-030',
        'Marechal Rondon', 'Rua Dona Rafaela',
        null, -29.91710255462995, -51.1738206456705, '92020-030', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1068, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Dr. Sezefredo Azambuja Vieira, 753-799 - Mal. Rondon, Canoas - RS, 92020-020',
        'Marechal Rondon', 'Avenida Doutor Sezefredo Ajambuja Vieira',
        null, -29.917384635161493, -51.16953608879634, '92020-020', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1069, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Av. Dr. Sezefredo Azambuja Vieira - Mal. Rondon, Canoas - RS, 92010-011',
        'Marechal Rondon', 'Avenida Doutor Sezefredo Ajambuja Vieira',
        null, -29.916259838497137, -51.17477661131904, '92010-011', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1070, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Liberdade, 276-192 - Mal. Rondon, Canoas - RS, 92020-000',
        'Marechal Rondon', 'Rua Liberdade',
        null, -29.914815373690885, -51.17423746761678, '92020-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1071, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Aurora, 850-834 - Mal. Rondon, Canoas - RS, 92020-510',
        'Marechal Rondon', 'Rua Aurora',
        null, -29.910021908898972, -51.17056922348004, '92020-510', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1072, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Liberdade, 666-634 - Mal. Rondon, Canoas - RS, 92020-240',
        'Marechal Rondon', 'Rua Liberdade',
        null, -29.910560507536648, -51.1724433331399, '92020-240', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1073, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Irmão Pedro, 515-815 - Mal. Rondon, Canoas - RS, 92020-550',
        'Marechal Rondon', 'Rua Irmão Pedro',
        null, -29.90684568649125, -51.172731208343535, '92020-550', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1074, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Araguaia - Igara, Canoas - RS, 92410-000',
        'Igara', 'Rua Araguaia',
        null, -29.90182992292103, -51.17064501978583, '92410-000', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1075, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Tietê, 485-681 - Igara, Canoas - RS, 92410-290',
        'Igara', 'Rua Tietê',
        null, -29.899536334030447, -51.16669638984263, '92410-290', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1076, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Tramandaí, 165-287 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Tramandaí',
        null, -29.91015857622866, -51.14342845115813, '92030-340', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1077, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Capão da Canoa, 301-363 - Estância Velha, Canoas - RS, 92032-080',
        'Estância Velha', 'Rua Capão da Canoa',
        null, -29.90964455734959, -51.148433723433946, '92032-080', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1078, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Arroio Teixeira, 501-535 - Estância Velha, Canoas - RS, 92032-050',
        'Estância Velha', 'Rua Arroio Teixeira',
        null, -29.907834332005606, -51.146176844572956, '92032-050', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1079, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Arpoador, 80-20 - Estância Velha, Canoas - RS, 92032-030',
        'Estância Velha', 'Rua Arpoador',
        null, -29.907129282038987, -51.143697774346606, '92032-030', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1080, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Alcídes Sabedoti, 633-463 - Estância Velha, Canoas - RS, 92032-450',
        'Estância Velha', 'Rua Alcíces Sabedoti',
        null, -29.90846270140195, -51.138388717929836, '92032-450', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1081, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Arroio do Sal, 119-183 - Estância Velha, Canoas - RS, 92032-040',
        'Estância Velha', 'Rua Arroio do Sal',
        null, -29.90728165963508, -51.147990428779735, '92032-040', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1082, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Alexandre de Gusmão, 1413-1499 - Estância Velha, Canoas - RS, 92030-340',
        'Estância Velha', 'Rua Alexandre de Gusmão',
        null, -29.911684137292138, -51.14191517400221, '92030-040', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1083, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'R. Capão Novo, 121-141 - Estância Velha, Canoas - RS, 92032-090',
        'Estância Velha', 'Rua Capão Novo',
        null, -29.910965739276925, -51.15014160873495, '92032-090', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1084, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Rua Ulysses Gomes Ferreira, 157 - Estância Velha, Canoas - RS, 92031-165',
        'Estância Velha', 'Rua Ulysses Gomes Ferreira',
        157, -29.912637938203773, -51.15268154651821, '92031-165', null, now(), now(), true);

INSERT INTO address(id, country, state, city, formatted_address, neighborhood, street, "number", latitude, longitude,
                    postal_code, complement, created_at, updated_at, active)
VALUES (1085, 'Brasil', 'Rio Grande do Sul',
        'Canoas', 'Rua Ulysses Gomes Ferreira, 157 - Estância Velha, Canoas - RS, 92031-165',
        'Estância Velha', 'Rua Ulysses Gomes Ferreira',
        157, -29.912637938203773, -51.15268154651821, '92031-165', null, now(), now(), true);

--1000, admin, user, Felipe Leal, lip.emerim@gmail.com, true
-- INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth) VALUES
-- (1000, 'admin', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW', 'apVs9PBnyc5szTwKwrnqlg==','WG/O3AFtSfeQSWTTg060SMMunkVyyjT0SkD0kF5IIH0=',true,'1998-06-26');

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1023, 'green.archer', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Oliver Queen', 'green.archer@gmail.com', false, '1998-06-26', 1023, '37093426000', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1000, 'admin', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Felipe Leal', 'admin@gmail.com', true, '1998-06-26', 1000, '88547082085', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1001, 'moderator', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Felipe Leal', 'moderator@gmail.com', true, '1998-06-26', 1001, '80789265028', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1002, 'carina.emerim', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Carina Emerim', 'carina.emerim@gmail.com', true, '1998-06-26', 1002, '40564269026', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1003, 'maria.amelia', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Maria Amélia', 'maria.amelia@gmail.com', true, '1998-06-26', 1003, '83040770004', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1004, 'eraldo.leal', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Eraldo Leal', 'eraldo.leal@gmail.com', true, '1998-06-26', 1004, '58144378040', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1005, 'melissa.leal', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Melissa Leal', 'melissa.leal@gmail.com', true, '1998-06-26', 1005, '08462633001', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1006, 'tchui.leal', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Tchui Leal', 'tchui.leal@gmail.com', true, '1998-06-26', 1006, '48529616030', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1007, 'tuca.leal', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Tuca Leal', 'tuca.leal@gmail.com', true, '1998-06-26', 1007, '28894524094', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1008, 'romero.britto', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Romero Britto', 'romero.britto@gmail.com', true, '1998-06-26', 1008, '48913555069', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1009, 'severus.snape', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Severus Snape', 'severus.snape@gmail.com', true, '1998-06-26', 1009, '62588161006', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1010, 'lord.voldemort', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Lord Voldemort', 'lord.voldemort@gmail.com', true, '1998-06-26', 1010, '18483400014', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1011, 'sirius.black', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Sirius Black', 'sirius.black@gmail.com', true, '1998-06-26', 1011, '36524020065', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1012, 'harry.potter', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Harry Potter', 'harry.potter@gmail.com', true, '1998-06-26', 1012, '81130440001', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1013, 'ronald.weasley', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Ronald Weasley', 'rony.weasley@gmail.com', true, '1998-06-26', 1013, '67884583089', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1014, 'luna.lovegood', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Luna Lovegood', 'luna.lovegood@gmail.com', true, '1998-06-26', 1014, '14456052015', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1015, 'lucius.malfoy', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Lucius Malfoy', 'lucius.malfoy@gmail.com', true, '1998-06-26', 1015, '08315687042', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1016, 'hermione.granger', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Hermione Granger', 'hermione.granger@gmail.com', true, '1998-06-26', 1016, '29377265002', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1017, 'sebastian.castellanos', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Sebastian Castellanos', 'sebastian.castellanos@gmail.com', true, '1998-06-26', 1017, '66946949030', 3, now(),
        now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1018, 'alucard', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Nosferatu Alucard', 'nosferatu.alucard@gmail.com', true, '1998-06-26', 1018, '15861939020', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1019, 'integra.hellsing', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Integra Fairbook Wingates Hellsing', 'integra.hellsing@gmail.com', true, '1998-06-26', 1019, '04848328080', 3,
        now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1020, 'draculina', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Seras Victoria', 'seras.victoria@gmail.com', true, '1998-06-26', 1020, '31337669024', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1021, 'the_flash', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Barry Allen', 'barry.allen@gmail.com', true, '1998-06-26', 1021, '04118584085', 3, now(), now());

INSERT INTO "user"(id, username, password, "name", email, active, date_of_birth, address_id, cpf, search_radius,
                   created_at, updated_at)
VALUES (1022, 'accelerator', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW',
        'Accelerator', 'accelerator@gmail.com', true, '1998-06-26', 1022, '48615711038', 3, now(), now());

--ROLES
INSERT INTO role(id, role)
VALUES (1, 'ROLE_USER'),
       (2, 'ROLE_MODERATOR'),
       (3, 'ROLE_ADMIN');

INSERT INTO user_roles (user_id, roles_id)
VALUES (1000, 1),
       (1000, 2),
       (1000, 3),
       (1001, 1),
       (1001, 2),
       (1002, 1),
       (1003, 1),
       (1004, 1),
       (1005, 1),
       (1006, 1),
       (1007, 1),
       (1008, 1),
       (1009, 1),
       (1010, 1),
       (1011, 1),
       (1012, 1),
       (1013, 1),
       (1014, 1),
       (1015, 1),
       (1016, 1),
       (1017, 1),
       (1018, 1),
       (1019, 1),
       (1020, 1),
       (1021, 1),
       (1022, 1),
       (1023, 1);

INSERT INTO occurrence_status(id, description)
VALUES (1, 'WAITING_ANALYSIS'),
       (2, 'IN_ANALYSIS'),
       (3, 'APPROVED'),
       (4, 'REJECTED'),
       (5, 'REJECTED_BY_ADMIN'),
       (6, 'REPORT_ACCEPTED');

INSERT INTO occurrence_type(id, description)
VALUES (1, 'ROBBERY'),
       (2, 'THEFT'),
       (3, 'VEHICLE_THEFT'),
       (4, 'VANDALISM'),
       (5, 'DRUGS_VIOLATION'),
       (6, 'HOMICIDE'),
       (7, 'ARSON'),
       (8, 'DISTURBING'),
       (9, 'SHOOTING'),
       (10, 'ANIMAL_ABUSE'),
       (11, 'WEAPONS');

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1000, 'Ocorrência pendente', 'Descrição da ocorrência pendente', null, 1000, null, 1,
        1, true, false, NOW(), NOW(), 1024, NOW(), 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1001, 'Ocorrência em análise', 'Descrição da ocorrência em análise', null, 1018, 1000, 2,
        2, true, false, NOW() - INTERVAL '1 MONTHS', NOW(), 1025, NOW() - INTERVAL '1 MONTHS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1002, 'Ocorrência aprovada', E'Uma ocorrência aprovada com quebras de linha\ne descrição complexa\n' ||
 E'deve mostrar apenas na visualização', null, 1019, 1001, 3,
        9, true, false, NOW() - INTERVAL '2 MONTHS', NOW(), 1026, NOW() - INTERVAL '2 MONTHS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1003, 'Ocorrência recusada', 'Descrição da ocorrência recusada', null, 1020, 1000, 4,
        4, true, false, NOW() - INTERVAL '3 MONTHS', NOW(), 1027, NOW() - INTERVAL '3 MONTHS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1004, 'Ocorrência recusada pelo administrador',
        'Descrição da ocorrência recusada pelo administrador', null, 1018, 1000, 5, 11, true, false,
        NOW() - INTERVAL '4 MONTHS', NOW(), 1028, NOW() - INTERVAL '4 MONTHS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1005, 'Ocorrência denunciada',
        'Descrição da ocorrência denunciada', null, 1018, 1000, 6, 11, true, false,
        NOW() - INTERVAL '5 MONTHS', NOW(), 1085, NOW() - INTERVAL '5 MONTHS', 1);


--Test occurrences

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1010, 'Ocorrência de Roubo 1', 'Descrição da ocorrência de Roubo 1', 'Comentários da ocorrência de Roubo 1',
        1002, 1000, 3, 1, true, true, NOW() - INTERVAL '1 DAYS', NOW(), 1029, NOW() - INTERVAL '1 DAYS', 1);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1011, 'Ocorrência de Furto 1', 'Descrição da ocorrência de Furto 1', 'Comentários da ocorrência de Furto 1',
        1003, 1000, 3, 2, true, false, NOW() - INTERVAL '2 DAYS', NOW(), 1030, NOW() - INTERVAL '2 DAYS', 2);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1012, 'Ocorrência de Roubo de veículo 1', 'Descrição da ocorrência de Roubo de veículo 1',
        'Comentários da ocorrência de Roubo de veículo 1', 1004, 1000, 3,
        3, true, true, NOW() - INTERVAL '3 DAYS', NOW(), 1031, NOW() - INTERVAL '3 DAYS', 3);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1013, 'Ocorrência de Vandalismo 1', 'Descrição da ocorrência de Vandalismo 1',
        'Comentários da ocorrência de Vandalismo 1', 1005, 1000, 3,
        4, true, false, NOW() - INTERVAL '4 DAYS', NOW(), 1032, NOW() - INTERVAL '4 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1014, 'Ocorrência de Drogas 1', 'Descrição da ocorrência de Drogas 1 aaaaaaaaaaaaaaa',
        'Comentários da ocorrência de Drogas 1', 1006, 1000, 3,
        5, true, true, NOW() - INTERVAL '5 DAYS', NOW(), 1033, NOW() - INTERVAL '5 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1015, 'Ocorrência de Homicídio 1', 'Descrição da ocorrência de Homicídio 1',
        'Comentários da ocorrência de Homicídio 1', 1007, 1000, 3,
        6, true, false, NOW() - INTERVAL '6 DAYS', NOW(), 1034, NOW() - INTERVAL '6 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1016, 'Ocorrência de Incêndio criminoso 1', 'Descrição da ocorrência de Incêndio criminoso 1',
        'Comentários da ocorrência de Incêndio criminoso 1', 1008, 1000, 3,
        7, true, true, NOW() - INTERVAL '7 DAYS', NOW(), 1035, NOW() - INTERVAL '7 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1017, 'Ocorrência de Perturbação 1', 'Descrição da ocorrência de Perturbação',
        'Comentários da ocorrência de Perturbação 1', 1009,
        1000, 3,
        8, true, false, NOW() - INTERVAL '8 DAYS', NOW(), 1036, NOW() - INTERVAL '8 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1018, 'Ocorrência de Tiroteio 1', 'Descrição da ocorrência de Tiroteio 1',
        'Comentários da ocorrência de Tiroteio 1', 1010, 1000, 3,
        9, true, true, NOW() - INTERVAL '9 DAYS', NOW(), 1037, NOW() - INTERVAL '9 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1019, 'Ocorrência de Maus tratos a animais 1', 'Descrição da ocorrência de Maus tratos a animais 1',
        'Comentários da ocorrência de Maus tratos a animais 1', 1011, 1000, 3,
        10, true, false, NOW() - INTERVAL '10 DAYS', NOW(), 1038, NOW() - INTERVAL '10 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1020, 'Ocorrência de Armas 1', 'Descrição da ocorrência de Armas 1', 'Comentários da ocorrência de Armas 1',
        1012, 1000, 3, 11, true, true, NOW() - INTERVAL '11 DAYS', NOW(), 1039, NOW() - INTERVAL '11 DAYS', 0);



INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1021, 'Ocorrência de Roubo 2', 'Descrição da ocorrência de Roubo 2', 'Comentários da ocorrência de Roubo 2',
        1002, 1000, 3, 1, true, false, NOW() - INTERVAL '12 DAYS', NOW(), 1040, NOW() - INTERVAL '12 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1022, 'Ocorrência de Furto 2', 'Descrição da ocorrência de Furto 2', 'Comentários da ocorrência de Furto 2',
        1003, 1000, 3, 2, true, true, NOW() - INTERVAL '13 DAYS', NOW(), 1041, NOW() - INTERVAL '13 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1023, 'Ocorrência de Roubo de Veículo 2', 'Descrição da ocorrência de Roubo de Veículo 2',
        'Comentários da ocorrência de Roubo de veículo 2', 1004, 1000, 3,
        3, true, false, NOW() - INTERVAL '14 DAYS', NOW(), 1042, NOW() - INTERVAL '14 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1024, 'Ocorrência de Vandalismo 2', 'Descrição da ocorrência de Vandalismo 2',
        'Comentários da ocorrência de Vandalismo 2', 1005, 1000, 3,
        4, true, true, NOW() - INTERVAL '15 DAYS', NOW(), 1043, NOW() - INTERVAL '15 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1025, 'Ocorrência de Drogas 2', 'Descrição da ocorrência de Drogas 2', 'Comentários da ocorrência de Drogas 2',
        1006, 1000, 3, 5, true, false, NOW() - INTERVAL '16 DAYS', NOW(), 1044, NOW() - INTERVAL '16 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1026, 'Ocorrência de Homicídio 2', 'Descrição da ocorrência de Homicídio 2',
        'Comentários da ocorrência de Homicídio 2', 1007, 1000, 3,
        6, true, true, NOW() - INTERVAL '17 DAYS', NOW(), 1045, NOW() - INTERVAL '17 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1027, 'Ocorrência de Incêndio criminoso 2', 'Descrição da ocorrência de Incêndio criminoso 2',
        'Comentários da ocorrência de Incêndio criminoso 2', 1008, 1000, 3,
        7, true, false, NOW() - INTERVAL '18 DAYS', NOW(), 1046, NOW() - INTERVAL '18 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1028, 'Ocorrência de Perturbação 2', 'Descrição da ocorrência de Perturbação 2',
        'Comentários da ocorrência de Perturbação 2', 1009, 1000, 3,
        8, true, true, NOW() - INTERVAL '19 DAYS', NOW(), 1047, NOW() - INTERVAL '19 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1029, 'Ocorrência de Tiroteio 2', 'Descrição da ocorrência de Tiroteio 2',
        'Comentários da ocorrência de Tiroteio 2', 1010, 1000, 3,
        9, true, false, NOW() - INTERVAL '20 DAYS', NOW(), 1048, NOW() - INTERVAL '20 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1030, 'Ocorrência de Maus tratos a animais 2', 'Descrição da ocorrência de Maus tratos a animais 2',
        'Comentários da ocorrência de Maus tratos a animais 2', 1011, 1000, 3,
        10, true, true, NOW() - INTERVAL '21 DAYS', NOW(), 1049, NOW() - INTERVAL '21 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1031, 'Ocorrência de Armas 2', 'Descrição da ocorrência de Armas 2', 'Comentários da ocorrência de Armas 2',
        1012, 1000, 3, 11, true, false, NOW() - INTERVAL '22 DAYS', NOW(), 1050, NOW() - INTERVAL '22 DAYS', 0);



INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1032, 'Ocorrência de Roubo 3', 'Descrição da ocorrência de Roubo 3', 'Comentários da ocorrência de Roubo 3',
        1002, 1000, 3, 1, true, true, NOW() - INTERVAL '23 DAYS', NOW(), 1051, NOW() - INTERVAL '23 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1033, 'Ocorrência de Furto 3', 'Descrição da ocorrência de Furto 3', 'Comentários da ocorrência de Furto 3',
        1003, 1000, 3, 2, true, false, NOW() - INTERVAL '24 DAYS', NOW(), 1052, NOW() - INTERVAL '24 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1034, 'Ocorrência de Roubo de Veículo 3', 'Descrição da ocorrência de Roubo de Veículo 3',
        'Comentários da ocorrência de Roubo de veículo 3', 1004, 1000, 3,
        3, true, true, NOW() - INTERVAL '25 DAYS', NOW(), 1053, NOW() - INTERVAL '25 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1035, 'Ocorrência de Vandalismo 3', 'Descrição da ocorrência de Vandalismo 3',
        'Comentários da ocorrência de Vandalismo 3', 1005, 1000, 3,
        4, true, false, NOW() - INTERVAL '26 DAYS', NOW(), 1054, NOW() - INTERVAL '26 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1036, 'Ocorrência de Drogas 3', 'Descrição da ocorrência de Drogas 3', 'Comentários da ocorrência de Drogas 3',
        1006, 1000, 3, 5, true, true, NOW() - INTERVAL '27 DAYS', NOW(), 1055, NOW() - INTERVAL '27 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1037, 'Ocorrência de Homicídio 3', 'Descrição da ocorrência de Homicídio 3',
        'Comentários da ocorrência de Homicídio 3', 1007, 1000, 3,
        6, true, false, NOW() - INTERVAL '28 DAYS', NOW(), 1056, NOW() - INTERVAL '28 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1038, 'Ocorrência de Incêndio criminoso 3', 'Descrição da ocorrência de Incêndio criminoso 3',
        'Comentários da ocorrência de Incêndio criminoso 3', 1008, 1000, 3,
        7, true, true, NOW() - INTERVAL '29 DAYS', NOW(), 1057, NOW() - INTERVAL '29 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1039, 'Ocorrência de Perturbação 3', 'Descrição da ocorrência de Perturbação 3',
        'Comentários da ocorrência de Perturbação 3', 1009, 1000, 3,
        8, true, false, NOW() - INTERVAL '30 DAYS', NOW(), 1058, NOW() - INTERVAL '30 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1040, 'Ocorrência de Tiroteio 3', 'Descrição da ocorrência de Tiroteio 3',
        'Comentários da ocorrência de Tiroteio 3', 1010, 1000, 3,
        9, true, true, NOW() - INTERVAL '31 DAYS', NOW(), 1059, NOW() - INTERVAL '31 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1041, 'Ocorrência de Maus tratos a animais 3', 'Descrição da ocorrência de Maus tratos a animais 3',
        'Comentários da ocorrência de Maus tratos a animais 3', 1011, 1000, 3,
        10, true, false, NOW() - INTERVAL '32 DAYS', NOW(), 1060, NOW() - INTERVAL '32 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1042, 'Ocorrência de Armas 3', 'Descrição da ocorrência de Armas 3', 'Comentários da ocorrência de Armas 3',
        1012, 1000, 3, 11, true, true, NOW() - INTERVAL '33 DAYS', NOW(), 1061, NOW() - INTERVAL '33 DAYS', 0);



INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1043, 'Ocorrência de Roubo 4', 'Descrição da ocorrência de Roubo 4', 'Comentários da ocorrência de Roubo 4',
        1013, 1001, 3, 1, true, false, NOW() - INTERVAL '34 DAYS', NOW(), 1062, NOW() - INTERVAL '34 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1044, 'Ocorrência de Furto 4', 'Descrição da ocorrência de Furto 4', 'Comentários da ocorrência de Furto 4',
        1014, 1001, 3, 2, true, true, NOW() - INTERVAL '35 DAYS', NOW(), 1063, NOW() - INTERVAL '35 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1045, 'Ocorrência de Roubo de Veículo 4', 'Descrição da ocorrência de Roubo de Veículo 4',
        'Comentários da ocorrência de Roubo de veículo 4', 1015, 1001, 3,
        3, true, false, NOW() - INTERVAL '36 DAYS', NOW(), 1064, NOW() - INTERVAL '36 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1046, 'Ocorrência de Vandalismo 4', 'Descrição da ocorrência de Vandalismo 4',
        'Comentários da ocorrência de Vandalismo 4', 1016, 1001, 3,
        4, true, true, NOW() - INTERVAL '37 DAYS', NOW(), 1065, NOW() - INTERVAL '37 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1047, 'Ocorrência de Drogas 4', 'Descrição da ocorrência de Drogas 4', 'Comentários da ocorrência de Drogas 4',
        1017, 1001, 3, 5, true, false, NOW() - INTERVAL '38 DAYS', NOW(), 1066, NOW() - INTERVAL '38 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1048, 'Ocorrência de Homicídio 4', 'Descrição da ocorrência de Homicídio 4',
        'Comentários da ocorrência de Homicídio 4', 1018, 1001, 3,
        6, true, true, NOW() - INTERVAL '39 DAYS', NOW(), 1067, NOW() - INTERVAL '39 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1049, 'Ocorrência de Incêndio criminoso 4', 'Descrição da ocorrência de Incêndio criminoso 4',
        'Comentários da ocorrência de Incêndio criminoso 4', 1008, 1001, 3,
        7, true, false, NOW() - INTERVAL '40 DAYS', NOW(), 1019, NOW() - INTERVAL '40 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1050, 'Ocorrência de Perturbação 4', 'Descrição da ocorrência de Perturbação 4',
        'Comentários da ocorrência de Perturbação 4', 1009, 1001, 3,
        8, true, true, NOW() - INTERVAL '41 DAYS', NOW(), 1020, NOW() - INTERVAL '41 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1051, 'Ocorrência de Tiroteio 4', 'Descrição da ocorrência de Tiroteio 4',
        'Comentários da ocorrência de Tiroteio 4', 1021, 1001, 3,
        9, true, false, NOW() - INTERVAL '42 DAYS', NOW(), 1070, NOW() - INTERVAL '42 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1052, 'Ocorrência de Maus tratos a animais 4', 'Descrição da ocorrência de Maus tratos a animais 4',
        'Comentários da ocorrência de Maus tratos a animais 4', 1022, 1001, 3,
        10, true, true, NOW() - INTERVAL '43 DAYS', NOW(), 1071, NOW() - INTERVAL '43 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1053, 'Ocorrência de Armas 4', 'Descrição da ocorrência de Armas 4', 'Comentários da ocorrência de Armas 4',
        1022, 1001, 3, 11, true, false, NOW() - INTERVAL '44 DAYS', NOW(), 1072, NOW() - INTERVAL '44 DAYS', 0);



INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1054, 'Ocorrência de Roubo 5', 'Descrição da ocorrência de Roubo 5', 'Comentários da ocorrência de Roubo 5',
        1013, 1001, 3, 1, true, true, NOW() - INTERVAL '45 DAYS', NOW(), 1073, NOW() - INTERVAL '45 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1055, 'Ocorrência de Furto 5', 'Descrição da ocorrência de Furto 5', 'Comentários da ocorrência de Furto 5',
        1014, 1001, 3, 2, true, false, NOW() - INTERVAL '46 DAYS', NOW(), 1074, NOW() - INTERVAL '46 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1056, 'Ocorrência de Roubo de Veículo 5', 'Descrição da ocorrência de Roubo de Veículo 5',
        'Comentários da ocorrência de Roubo de veículo 5', 1015, 1001, 3,
        3, true, true, NOW() - INTERVAL '47 DAYS', NOW(), 1075, NOW() - INTERVAL '47 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1057, 'Ocorrência de Vandalismo 5', 'Descrição da ocorrência de Vandalismo 5',
        'Comentários da ocorrência de Vandalismo 5', 1016, 1001, 3,
        4, true, false, NOW() - INTERVAL '48 DAYS', NOW(), 1076, NOW() - INTERVAL '48 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1058, 'Ocorrência de Drogas 5', 'Descrição da ocorrência de Drogas 5', 'Comentários da ocorrência de Drogas 5',
        1017, 1001, 3, 5, true, true, NOW() - INTERVAL '49 DAYS', NOW(), 1077, NOW() - INTERVAL '49 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1059, 'Ocorrência de Homicídio 5', 'Descrição da ocorrência de Homicídio 5',
        'Comentários da ocorrência de Homicídio 5', 1018, 1001, 3,
        6, true, false, NOW() - INTERVAL '50 DAYS', NOW(), 1078, NOW() - INTERVAL '50 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1060, 'Ocorrência de Incêndio criminoso 5', 'Descrição da ocorrência de Incêndio criminoso 5',
        'Comentários da ocorrência de Incêndio criminoso 5', 1019, 1001, 3,
        7, true, true, NOW() - INTERVAL '51 DAYS', NOW(), 1079, NOW() - INTERVAL '51 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1061, 'Ocorrência de Perturbação 5', 'Descrição da ocorrência de Perturbação 5',
        'Comentários da ocorrência de Perturbação 5', 1020, 1001, 3,
        8, true, false, NOW() - INTERVAL '52 DAYS', NOW(), 1080, NOW() - INTERVAL '52 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1062, 'Ocorrência de Tiroteio 5', 'Descrição da ocorrência de Tiroteio 5',
        'Comentários da ocorrência de Tiroteio 5', 1021, 1001, 3,
        9, true, true, NOW() - INTERVAL '53 DAYS', NOW(), 1081, NOW() - INTERVAL '53 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1063, 'Ocorrência de Maus tratos a animais 5', 'Descrição da ocorrência de Maus tratos a animais 5',
        'Comentários da ocorrência de Maus tratos a animais 5', 1022, 1001, 3,
        10, true, false, NOW() - INTERVAL '54 DAYS', NOW(), 1082, NOW() - INTERVAL '54 DAYS', 0);

INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1064, 'Ocorrência de Armas 5', 'Descrição da ocorrência de Armas 5', 'Comentários da ocorrência de Armas 5',
        1022, 1001, 3, 11, true, true, NOW() - INTERVAL '55 DAYS', NOW(), 1083, NOW() - INTERVAL '55 DAYS', 0);



INSERT INTO occurrence(id, title, description, moderator_comments, user_id, moderator_id, occurrence_status_id,
                       occurrence_type_id, active, is_verified, created_at, updated_at, address_id, occurrence_date,
                       reports_count)
VALUES (1065, 'Ocorrência de Roubo 6', 'Descrição da ocorrência de Roubo 6', 'Comentários da ocorrência de Roubo 6',
        1002, 1000, 3, 1, true, true, NOW() - INTERVAL '1 DAYS', NOW(), 1084, NOW() - INTERVAL '1 DAYS', 0);

INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1000, 1004, 1005, TRUE, 'Essa ocorrência contém informações mentirosas', NOW(), NOW());


INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1001, 1003, 1010, TRUE, 'Denúncia de ocorrência 1', NOW(), NOW());


INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1002, 1005, 1011, TRUE, 'Denúncia de ocorrência 2', NOW(), NOW());

INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1003, 1006, 1011, TRUE, 'Denúncia de ocorrência 3', NOW(), NOW());


INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1004, 1008, 1012, TRUE, 'Denúncia de ocorrência 4', NOW(), NOW());

INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1005, 1003, 1012, TRUE, 'Denúncia de ocorrência 5', NOW(), NOW());

INSERT INTO report(id, user_id, occurrence_id, active, description, created_at, updated_at)
VALUES (1006, 1004, 1012, TRUE, 'Denúncia de ocorrência 6', NOW(), NOW());