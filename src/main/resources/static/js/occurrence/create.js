$(function () {

    moment.locale('pt-BR');
    let maskFormat = moment().localeData().longDateFormat('L').toLocaleLowerCase();

    let momentFormat = moment().localeData().longDateFormat('L');

    if ($('#occurrence-date-real').val()) {
        $('#occurrence-date-label').val(moment($('#occurrence-date-real').val()).format(momentFormat));
    }

    $('#occurrence-date-label').inputmask({
        alias: 'datetime',
        inputFormat: maskFormat
    })

    $('#postal-code').inputmask({mask: "9{5}-9{3}", greedy: false});
    $('#number').inputmask({mask: "9{2,11}"});

    $("#occurrence-form").submit(function () {
        let input = $(this).find("#occurrence-date-label");
        let date = moment($(input).val(), momentFormat);
        $('#occurrence-date-real').val(date.format('YYYY-MM-DD'));

    });

    $('input[type="file"]').change(function (e) {
        let fileName = e.target.files[0].name;
        $(this).parent().find('.custom-file-label').text(fileName);
    });
});