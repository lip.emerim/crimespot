class Callout extends google.maps.OverlayView {
    constructor() {
        super();
        this.position = null;
        this.containerDiv = document.createElement('div');
        this.containerDiv.id = "map-callout"
        this.containerDiv.className = 'map-callout';
        Callout.preventMapHitsAndGesturesFrom(this.containerDiv);
    }

    setContent(occurrence) {
        $('.verified-tooltip').tooltip('dispose');
        const html = this.getInfoWindowContentHTML(occurrence);
        this.containerDiv.innerHTML = '';
        this.containerDiv.insertAdjacentHTML('afterbegin', html);
    }

    close() {
        $('.verified-tooltip').tooltip('dispose');
        this.setMap(null);
    }

    open(map, marker) {
        this.setMap(map);
        this.position = marker.getPosition();
    }

    onAdd() {
        this.getPanes().floatPane.appendChild(this.containerDiv);
        $('.verified-tooltip').tooltip({
            container: this.containerDiv
        });
    }

    onRemove() {
        if (this.containerDiv.parentElement) {
            this.containerDiv.parentElement.removeChild(this.containerDiv);
        }
    }

    draw() {
        if (!this.position) {
            return;
        }

        const fixLeft = 20;

        const divPosition = this.getProjection().fromLatLngToDivPixel(
            this.position
        );

        this.containerDiv.style.left = (divPosition.x + fixLeft) + 'px';
        this.containerDiv.style.top = divPosition.y + 'px';
    }

    getInfoWindowContentHTML(occurrence) {
        const configsDiv = $('#configs');
        const occurrenceDate = occurrence.occurrenceDate
            ? occurrence.occurrenceDate.format(moment.localeData().longDateFormat('L'))
            : 'N/A';

        let title = `<h5>${occurrence.title}</h5>`;
        let verifiedMessage = $(configsDiv).data('occurrence-verified-message');
        let reportBtnMessage = $(configsDiv).data('report-btn-message');
        if (occurrence.verified) {
            title = `<h5><i class="fas fa-check-circle text-success verified-tooltip" 
                            style="cursor:pointer"
                            data-toggle="tooltip"
                            data-trigger="click"
                            title="${verifiedMessage}"></i> ${occurrence.title}</h5>`;
        }

        return `
            <div class="callout callout-secondary">   
                <div class="callout-container">
                    ${title}
                    <p class="d-block mb-2"><i class="fas fa-calendar"></i> ${occurrenceDate}</p>
                    <p class="d-block mb-0">${occurrence.address}</p>
                    <hr />
                    <pre class="d-block">${occurrence.description}</pre>
                    <hr />
                    <div class="text-right">
                        <button type="button" 
                        class="btn btn-sm btn-outline-secondary text-decoration-none" 
                        onclick="javascript:openReportModal(${occurrence.id})">
                            ${reportBtnMessage}
                        </button>
                    </div>
                </div>            
            </div>
        `;
    }
}