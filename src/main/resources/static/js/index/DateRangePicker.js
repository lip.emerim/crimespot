class DateRangePicker {
    config = {
        startInput: '#start-date',
        endInput: '#end-date',
        container: '#date-range-container',
        rangeStart: moment().subtract(29, 'days'),
        rangeEnd: moment(),
        rangeMin: moment().subtract(2, 'years'),
        rangeMax: moment(),
        postFilter: (start, end) => {
            start.set({'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0});
            end.set({'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0});
            $(this.config.startInput).val(start.format('YYYY-MM-DD'));
            $(this.config.endInput).val(end.format('YYYY-MM-DD'));
        }
    }

    constructor(userConfigs) {
        Object.assign(this.config, userConfigs);
        this.element = null;
    }

    initialize() {

        const container = $(this.config.container);

        let datepickerLanguage = {
            applyLabel: container.data('apply-label'),
            cancelLabel: container.data('cancel-label'),
            customRangeLabel: container.data('custom-range-label'),
            fromLabel: container.data('from-label'),
            toLabel: container.data('to-label'),
            weekLabel: container.data('week-label'),
            today: container.data('today'),
            yesterday: container.data('yesterday'),
            last7Days: container.data('last-7-days'),
            last30Days: container.data('last-30-days'),
            currentMonth: container.data('current-month'),
            lastMonth: container.data('last-month'),
        }

        let datepickerRanges = {};
        datepickerRanges[datepickerLanguage.today] = [moment(), moment()];
        datepickerRanges[datepickerLanguage.yesterday] = [moment().subtract(1, 'days'), moment().subtract(1, 'days')];
        datepickerRanges[datepickerLanguage.last7Days] = [moment().subtract(6, 'days'), moment()];
        datepickerRanges[datepickerLanguage.last30Days] = [moment().subtract(29, 'days'), moment()];
        datepickerRanges[datepickerLanguage.currentMonth] = [moment().startOf('month'), moment().endOf('month')];
        datepickerRanges[datepickerLanguage.lastMonth] = [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];

        this.element = container.daterangepicker({
            "locale": {
                "format": moment.localeData().longDateFormat('L'),
                "separator": " - ",
                "applyLabel": datepickerLanguage.applyLabel,
                "cancelLabel": datepickerLanguage.cancelLabel,
                "fromLabel": datepickerLanguage.fromLabel,
                "toLabel": datepickerLanguage.toLabel,
                "customRangeLabel": datepickerLanguage.customRangeLabel,
                "weekLabel": datepickerLanguage.weekLabel,
                "daysOfWeek": moment.weekdaysShort(true),
                "monthNames": moment.monthsShort(),
                "firstDay": 1
            },
            maxDate: this.config.rangeMax,
            minDate: this.config.rangeMin,
            startDate: this.config.rangeStart,
            endDate: this.config.rangeEnd,
            ranges: datepickerRanges,
        }, this.config.postFilter);

        this.config.postFilter(this.config.rangeStart, this.config.rangeEnd);

        return this;
    }

    getElement() {
        return this.element;
    }

}

