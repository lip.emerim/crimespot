class HomeMap {
    /**
     * @param {string} element
     */
    constructor(element) {
        this.container = $(element);
        this.form = $('form', this.container);
        this.div = document.getElementById('googlemaps');
        this.section = {};
        this.section.filter = $('#map-filters', this.container);
        this.section.occurrenceList = $('.map-occurrence', this.container);
        this.configsDiv = $('#configs');
        this.searchErrorMessage = this.configsDiv.data('map-search-error-message');
        this.callout = new Callout();
        this.maxOccurrences = 200;
        this.searchMaxMessage = this.configsDiv.data('max-map-occurrences-message').replace('{max}', this.maxOccurrences);
        this.ajax = null;
        this.markerInfo = {
            defaultPath: '/png/occurrence/marker/default/',
            verifiedPath: '/png/occurrence/marker/verified/',
            extension: '.png',
            // reduced to a scale of nine
            scaledSize: new google.maps.Size(40, 56),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 0),
        }

        this.markers = {};

        this.map = new google.maps.Map(this.div, {
            center: {
                lat: this.configsDiv.data('user-latitude') || -29.910443926311512,
                lng: this.configsDiv.data('user-longitude') || -51.14491297262808,
            },
            zoom: 15,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP]
            }, // here´s the array of controls
            disableDefaultUI: true,
            mapTypeControl: false,
            fullscreenControl: false,
            scaleControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
            },
            styles: this.getMapStyle(),
        });

        this.radius = 0.0;

        google.maps.event.addListener(this.map, 'idle', () => {
            this.search();
        });

        $('#current-location', this.container).on('click', (event) => this.setCenterToCurrentLocation(event));
        $('#my-location', this.container).on('click', (event) => {this.setCenterToAddressLocation(event)});

        this.form.on('submit', (event) => {
            event.preventDefault();
            this.search();
            this.closeFilterSection();
            this.scrollMobileToTop();
        });

        $('#map-filter-button', this.container).click((event) => {
            event.preventDefault();
            this.openFilterSection();
        });

        $('#map-filter-close', this.container).click((event) => {
            event.preventDefault();
            this.closeFilterSection();
        });
    }

    scrollMobileToTop() {
        if ($(window).width() < 992) {
            $('html').stop().animate({
                scrollTop: 0,
            }, 800);
        }
    }

    setCenterToAddressLocation(event) {
        if (event) {
            event.preventDefault();
        }
        const lat = this.configsDiv.data('user-latitude');
        const lng = this.configsDiv.data('user-longitude');
        this.map.panTo({lat, lng});
        this.scrollMobileToTop();
        this.closeFilterSection();
    }

    setCenterToCurrentLocation(event) {
        if (event) {
            event.preventDefault();
        }
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.map.panTo({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                });

                this.scrollMobileToTop();
                this.closeFilterSection();
            },
            (error) => {
                if (error.code === error.PERMISSION_DENIED) {
                    $.alert.append({ type: 'warning',  message: this.configsDiv.data('location-permission-denied-message') });
                    return;
                }

                $.alert.append({ type: 'warning',  message: this.configsDiv.data('location-error-message') });
                console.error(error);
            },
            {enableHighAccuracy:true}
        );
    }

    closeFilterSection() {
        this.section.filter.removeClass('active');
    }

    openFilterSection() {
        this.section.filter.addClass('active');
    }

    getItemHTML({id, address, title, icon}) {
        return $(`
            <span class="row align-items-center map-occurrence-item" id="occurrence-item-${id}">
                <span class="col-2">
                    <img src="${icon.url}" class="img-fluid">
                </span>
                <span class="col-10">
                    <h4>${title}</h4>
                    <p>${address}</p>
                </span>
            </span>
        `);
    }

    clearAjax() {
        if (this.ajax !== null) {
            this.ajax.abort();
            this.ajax = null;
        }
    }

    setRadius() {
        const bounds = this.map.getBounds();
        const center = bounds.getCenter();
        const ne = bounds.getNorthEast();
        this.radius = google.maps.geometry.spherical.computeDistanceBetween(center, ne);
    }

    /**
     * Remove orphan callouts, when the selected occurrence is removed from the list
     */
    clearCallout() {
        const hasSelection = $('.map-occurrence-item.active', this.container).length > 0;
        if (!hasSelection) {
            this.callout.close();
        }
    }

    search() {
        this.clearAjax();
        this.setRadius();

        const data = {};
        const arrayDataNames = [
            'occurrenceTypeEnums',
        ]
        $.each(this.form.serializeArray(), function _each() {
            if (data[this.name]) {
                if (!data[this.name].push) {
                    data[this.name] = [data[this.name]];
                }
                data[this.name].push(this.value || '');
            } else {

                if (arrayDataNames.includes(this.name)) {
                    data[this.name] = [this.value];
                    return;
                }

                data[this.name] = this.value || '';
            }
        });

        this.ajax = $.ajax({
            url: '/occurrence/map-list',
            cache: true,
            data: JSON.stringify({
                ...data,
                latitude: this.map.center.lat(),
                longitude: this.map.center.lng(),
                searchRadius: this.radius / 1000,
            }),
            dataType: 'JSON',
            contentType: 'application/json',
            method: 'POST',
            success: (data) => {
                this.update(data);
                this.clearCallout();
            },
            complete: () => {
                this.clearAjax()
            },
            error: (request, textStatus, errorThrown) => {
                if (request.statusText === 'abort') {
                    return;
                }
                $.alert.append({
                    type: 'danger',
                    message: this.searchErrorMessage,
                })
            }
        });
    }

    calculateCenter(occurrence, offsetX = 0, offsetY = -80) {
        const projection = this.map.getProjection();

        const markerPoint = projection.fromLatLngToPoint(occurrence.position);
        const zoomScale = Math.pow(2, this.map.getZoom());

        const offsetPoint = new google.maps.Point((offsetX / zoomScale), (offsetY / zoomScale));

        return projection.fromPointToLatLng(new google.maps.Point(
            markerPoint.x - offsetPoint.x,
            markerPoint.y + offsetPoint.y
        ));
    }

    selectOccurrence(occurrence, needToScroll = false) {
        const item = $(`#occurrence-item-${occurrence.id}`);

        if (item.hasClass('active')) {
            this.callout.close();
            $('.map-occurrence-item', this.container).removeClass('active');
            return;
        }

        $('.map-occurrence-item', this.container).removeClass('active');
        item.addClass('active');

        this.callout.setContent(occurrence);
        this.callout.open(this.map, occurrence.marker);

        if ($(window).width() < 992) {
            $('html').stop().animate({
                scrollTop: 0,
            }, 800);
        }

        let offsetY = 0;
        if ($(window).height() < 576) {
            offsetY = -80;
        }

        this.map.panTo(this.calculateCenter(occurrence, 0, offsetY));

        if (needToScroll) {
            const top = item.prop('offsetTop');
            this.section.occurrenceList.stop(true, true).animate({
                scrollTop: top - 2,
            }, 800);
        }
    }

    /**
     * @param {object[]} data
     */
    update({data, totalResults}) {
        const keys = Object.keys(this.markers);
        const ids = data.map(row => row.id);

        keys.forEach((id) => {
            if (!ids.includes(parseInt(id, 10))) {
                this.markers[id].marker.setMap(null);
                $(`#occurrence-item-${id}`, this.container)
                    .remove();
                delete this.markers[id];
            }
        });

        data.forEach((row) => {
            if (this.markers[row.id]) {
                return;
            }

            const occurrence = {
                id: row.id,
                marker: null,
                position: new google.maps.LatLng(row.address.latitude, row.address.longitude),
                address: row.address.formattedAddress,
                description: row.description,
                occurrenceDate: moment(row.occurrenceDate, 'YYYY-MM-DD'),
                verified: row.verified,
                title: row.title,
                icon: {
                    url: this.getMarkerIcon(row),
                    scaledSize: this.markerInfo.scaledSize,
                    origin: this.markerInfo.origin,
                    anchor: this.markerInfo.anchor,
                },
                occurrenceData: row,
            };

            occurrence.marker = new google.maps.Marker({
                position: occurrence.position,
                label: null,
                icon: occurrence.icon,
                map: this.map,
                title: row.title,
            });

            const html = this.getItemHTML(occurrence);
            html.on('click', (event) => {
                event.preventDefault();
                this.selectOccurrence(occurrence);
            });

            occurrence.marker.addListener('click', () => {
                this.selectOccurrence(occurrence, true);
            });

            this.section.occurrenceList.append(html);
            this.markers[row.id] = occurrence;
        });

        if (totalResults > this.maxOccurrences) {
            $.alert.append({
                type: 'warning',
                message: this.searchMaxMessage,
            })
        }
    }

    getMarkerIcon(occurrenceData) {
        let markerType = occurrenceData.occurrenceType.description.toLowerCase();
        let markerInfo = this.markerInfo;

        if (occurrenceData.verified) {
            return markerInfo.verifiedPath + markerType + markerInfo.extension;
        }

        return markerInfo.defaultPath + markerType + markerInfo.extension;
    }

    /**
     * Return the style of the google maps map.
     *
     * @returns {({stylers: [{color: string}], elementType: string}|{stylers: [{color: string}], elementType: string}|
     * {stylers: [{color: string}], elementType: string}|{featureType: string, stylers: [{color: string}], elementType: string}|
     * {featureType: string, stylers: [{color: string}], elementType: string})[]}
     * @private
     */
    getMapStyle() {
        return [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#757575"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#181818"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#1b1b1b"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#2c2c2c"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#8a8a8a"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#373737"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#3c3c3c"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#4e4e4e"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#3d3d3d"
                    }
                ]
            }
        ]
    }
}