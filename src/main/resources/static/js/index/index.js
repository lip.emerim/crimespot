let modalReportAnonymous = null;
let modalReportAuthenticated = null;
let checkReportAjax = null;
let submitReportAjax = null;

$(function () {

    modalReportAnonymous = $('#report-modal-anonymous');
    modalReportAuthenticated = $('#report-modal-authenticated');
    moment.locale('pt-BR');

    const dateRangePicker = new DateRangePicker({
        container: '#map-range'
    }).initialize();

    const searchForm = $('#search-form');
    const resetSearchButton = $('#clear-search-button');

    resetSearchButton.on('click', (event) => {
        event.preventDefault();
        searchForm.get(0).reset();
        searchForm.find('.select2-multiple').trigger('change');
        dateRangePicker.initialize();
    });

    $(modalReportAuthenticated).on('shown.bs.modal', () => {
        $('#report-description').focus();
    });

    $(modalReportAuthenticated).on('hidden.bs.modal', () => {
        $('#report-description').val('');
    });

    if (!navigator.geolocation) {
        $('#current-location').hide();
    }

    const homeMap = new HomeMap('#home-map');

    $('#report-occurrence-form').on('submit', (event) => {
        event.preventDefault();
        $('button[type="submit"]', modalReportAuthenticated).prop('disabled', true);
        reportOccurrence();
    })
});

function openReportModal(occurrenceId) {
    if (modalReportAnonymous.length) {
        modalReportAnonymous.modal('show');
        return;
    }

    if (checkReportAjax !== null) {
        checkReportAjax.abort();
    }

    checkReportAjax = $.ajax({
        url: `/occurrence/${occurrenceId}/report`,
        method: 'HEAD',
        contentType: 'application/json',
        dataType: 'json',
        statusCode: {
            404: () => {
                $('form', modalReportAuthenticated).prop('action', `/occurrence/${occurrenceId}/report`);
                modalReportAuthenticated.modal('show');
            },
            204: () => {
                $.alert.append({
                    message: $('#configs').data('occurrence-already-reported-message'),
                    type: 'warning',
                });
            },
        },
    });
}

function reportOccurrence() {
    if (submitReportAjax !== null) {
        submitReportAjax.abort();
    }

    const description = $('#report-description').val();
    const url = $('form', modalReportAuthenticated).prop('action');

    submitReportAjax = $.ajax({
        url,
        method: 'POST',
        data: JSON.stringify({
            description,
        }),
        contentType: 'application/json',
        dataType: 'json',
        success: (response) => {
            $.alert.append({
                message: response.message,
                type: response.type.toLowerCase(),
            });

            if (response.type === 'SUCCESS') {
                modalReportAuthenticated.modal('hide');
            }
        },
        error: (response, error) => {
            console.log(error)
            $.alert.append({
                message: $('#configs').data('unknown-error'),
                type: 'danger',
            });
        },
        complete: () => {
            $('button[type="submit"]', modalReportAuthenticated).prop('disabled', false);
        },
    });
}