$(function () {
    let searchClosure = function () {
        let timeout = null;

        $("#postal-code").off('keyup.search');

        $('#postal-code').on('keyup.search', function (e) {
            blockUnBlockInputs(true);
            e.preventDefault();
            e.stopPropagation();
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                if (e.target.value.length < 5 || e.target.value.length > 11) {
                    return;
                }
                searchByPostalCode(e.target.value);
            }, 2000);
        });
    };

    searchClosure();
});

function searchByPostalCode(postalCode) {
    $.get({
        url: "/address/search",
        data: {query: postalCode}
    }).done(function (response) {
        $('#country').val(response.country);
        $('#state').val(response.state);
        $('#city').val(response.city);
        $('#street').val(response.street);
        $('#formattedAddress').val(response.formattedAddress);
        $('#neighborhood').val(response.neighborhood);

    }).fail(function (xhr, status, response) {

        if (!response.isExpected) {
            console.log(response);
            return;
        }

        $.alert.append({
            type: "warning",
            message: response.message
        })
    }).always(function () {
        blockUnBlockInputs(false);
    });
}

function blockUnBlockInputs(blocked) {
    $('#country').prop('readonly', blocked);
    $('#state').prop('readonly', blocked);
    $('#city').prop('readonly', blocked);
    $('#street').prop('readonly', blocked);
    $('#formattedAddress').prop('readonly', blocked);
    $('#neighborhood').prop('readonly', blocked);
}