$(function () {

    $('#origin-url').val();

    $('.btn-toggle-user').on('click', function () {
        let userId = $(this).data('user-id');
        let isActive = $(this).data('active');

        setConfirmationForToggle(userId, isActive);
    });

    $('.btn-toggle-moderator').on('click', function () {
        let userId = $(this).data('user-id');
        let isModerator = $(this).data('active');

        setConfirmationForToggleModerator(userId, isModerator);
    });

    function setConfirmationForToggle(userId, isActive) {

        let modal = "#modal-confirm"

        let modalText = isActive ? 'deactivateText' : 'activateText';
        let modalBody = $(modal).find('.modal-body');
        let modalHeader = $(modal).find('.modal-header h5');

        modalBody.text(modalBody.data(modalText));
        modalHeader.text(modalHeader.data(modalText));


        $(modal).find('.confirm-button')
            .off('click.confirm')
            .on('click', function () {
                $('#user-id').val(userId);
                $('#toggle-user-form').attr('action', '/user/toggle').submit();

            });

        $(modal).modal('show');

    }

    function setConfirmationForToggleModerator(userId, isModerator) {

        let modal = "#modal-confirm"

        let modalText = isModerator ? 'deactivateModeratorText' : 'activateModeratorText';
        let modalBody = $(modal).find('.modal-body');
        let modalHeader = $(modal).find('.modal-header h5');

        modalBody.text(modalBody.data(modalText));
        modalHeader.text(modalHeader.data(modalText));


        $(modal).find('.confirm-button')
            .off('click.confirm')
            .on('click', function () {
                $('#user-id').val(userId);
                $('#toggle-user-form').attr('action', '/user/toggle-moderator').submit();

            });

        $(modal).modal('show');

    }

});