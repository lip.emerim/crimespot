$(function () {

    moment.locale('pt-BR');

    if ($('#date-of-birth-real').val()) {
        $('#date-of-birth-label').val(moment($('#date-of-birth-real').val()).format("L"));
    }

    $('#date-of-birth-label').inputmask({
        alias: 'datetime',
        inputFormat: moment().localeData().longDateFormat('L').toLocaleLowerCase()
    })
    $('#postal-code').inputmask({mask: "9{5}-9{3}", greedy: false});
    $('#cpf').inputmask({mask: "9{11}"});
    $('#number').inputmask({mask: "9{2,11}"});
    $('#searchRadius').inputmask("numeric", {
        min: 1,
        max: 10,
        rightAlign: false,
    });

    $("#user-form").submit(function () {
        let input = $("#date-of-birth-label");
        let date = moment($(input).val(), moment().localeData().longDateFormat('L'));
        $('#date-of-birth-real').val(date.format('YYYY-MM-DD'));
    });
});