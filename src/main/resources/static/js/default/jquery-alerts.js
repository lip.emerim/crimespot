(function ($) {

    let toastQueue = [];
    const MAX_TOASTS = 5;

    $.alert = {};
    $.alert.append = function (data) {
        let bodyClass;
        let icon;
        let autohide = true;
        let delay = 5000;
        // Types
        switch (data.type) {
            case 'danger':
                bodyClass = 'bg-danger';
                icon = 'fas fa-ban';
                delay = 15000;
                break;
            case 'warning':
                bodyClass = 'bg-warning';
                icon = 'fas fa-exclamation-triangle';
                delay = 10000;
                break;
            case 'success':
                bodyClass = 'bg-success';
                icon = 'fas fa-check';
                break;
            case 'info':
            default:
                bodyClass = 'bg-info';
                icon = 'fas fa-info';
                break;
        }

        toastQueue.push({
            title: data.title,
            message: data.message,
            autohide: autohide,
            bodyClass: bodyClass,
            delay: delay,
            icon: icon
        });

        return this;
    };

    /**
     * Limita o número máximo de toasts em tela para 5.
     *
     * Caso o limite seja atingido e mais toasts sejam adicionados, este método esperará um toast desaparecer
     * para adicionar outro. Também é adicionada uma barra de progresso
     */
    function toastQueueManager() {
        let toastCount = $('.toast').length;

        if (toastCount === MAX_TOASTS) {
            setTimeout(toastQueueManager, 1000);
            return;
        }

        let toast = toastQueue.shift();

        if (!toast) {
            setTimeout(toastQueueManager, 2000);
            return;
        }
        let progressId = `progress-${toastCount}`;
        let toastBody = `
            <div class="row">
                <div class="col-12">
                    <span>${toast.message}</span>
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-12">
                    <div class="progress" style="height: 0.2rem;">
                        <div id="${progressId}" class="progress-bar toast-progress-bar ${toast.bodyClass}" 
                        role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        `;

        $(document).Toasts('create', {
            title: toast.title || '',
            body: toastBody,
            autohide: toast.autohide,
            class: toast.bodyClass,
            delay: toast.delay,
            icon: toast.icon
        });

        $(`#${progressId}`).animate({
            width: '100%',
        }, {
            duration: toast.delay,
            easing: 'linear',
        });

        setTimeout(toastQueueManager, 500);
    }

    $(function () {
        $('#alert-container div').each(function () {
            let message = $(this).data('message');
            let type = $(this).data('type');
            let title = $(this).data('title');

            $.alert.append({
                message: message,
                type: type,
                title: title
            });
        });

        $('#alert-container').remove();
        toastQueueManager();
    });
})(jQuery);
