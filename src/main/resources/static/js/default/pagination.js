$(function () {
    let baseUrl = `${window.location.pathname}?`;
    console.log(baseUrl);

    let searchParams = new URLSearchParams(window.location.search);
    searchParams.delete('page')

    $('#pagination-list').find('.page-link').on('click', function (event) {

        event.preventDefault();
        event.stopPropagation();

        searchParams.append('page', $(this).data('page'));

        window.location.replace(baseUrl + searchParams.toString());
    });
});