//Enable tooltips
$(function () {
    moment.locale('pt-BR');
    $.fn.select2.defaults.set("theme", "bootstrap4");

    $('[data-toggle="tooltip"], [data-toggle-tooltip="tooltip"]').tooltip();

    $('.select2').each(function () {
        $(this).select2({
            allowClear: $(this).data('allowClear'),
            placeholder: $(this).data('placeholder'),
            width: '100%',
        });
    });

    $('.select2-multiple').each(function () {
        $(this).select2({
            multiple: true,
            allowClear: true,
            placeholder: $(this).data('placeholder'),
            width: '100%',
        });
    });

    $('.local-date-time').each(function () {
        const dateString = $(this).text();

        if (!dateString) {
            $(this).removeClass('local-date-time');
            return;
        }

        const date = moment.utc(dateString);
        $(this)
            .text(date.local().format('DD/MMM/YYYY HH:mm:ss'))
            .removeClass('local-date-time');
    })
});