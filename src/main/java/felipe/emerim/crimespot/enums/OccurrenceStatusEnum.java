package felipe.emerim.crimespot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OccurrenceStatusEnum {

    WAITING_ANALYSIS("occurrenceStatusEnum.waitingAnalysis"),
    IN_ANALYSIS("occurrenceStatusEnum.inAnalysis"),
    APPROVED("occurrenceStatusEnum.approved"),
    REJECTED("occurrenceStatusEnum.rejected"),
    REJECTED_BY_ADMIN("occurrenceStatusEnum.rejectedByAdmin"),
    REPORT_ACCEPTED("occurrenceStatusEnum.reportAccepted");

    private final String description;
}
