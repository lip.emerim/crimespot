package felipe.emerim.crimespot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OccurrenceTypeEnum {
    ROBBERY("occurrenceTypeEnum.robbery"),
    THEFT("occurrenceTypeEnum.theft"),
    VEHICLE_THEFT("occurrenceTypeEnum.vehicleTheft"),
    VANDALISM("occurrenceTypeEnum.vandalism"),
    DRUGS_VIOLATION("occurrenceTypeEnum.drugsViolation"),
    HOMICIDE("occurrenceTypeEnum.homicide"),
    ARSON("occurrenceTypeEnum.arson"),
    DISTURBING("occurrenceTypeEnum.disturbing"),
    SHOOTING("occurrenceTypeEnum.shooting"),
    ANIMAL_ABUSE("occurrenceTypeEnum.animalAbuse"),
    WEAPONS("occurrenceTypeEnum.weapons");

    private final String description;
}
