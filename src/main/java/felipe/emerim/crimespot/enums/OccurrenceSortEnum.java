package felipe.emerim.crimespot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OccurrenceSortEnum {

    MOST_RECENT("occurrenceSortEnum.mostRecent"),
    LEAST_RECENT("occurrenceSortEnum.leastRecent"),
    NEAREST("occurrenceSortEnum.nearest"),
    MOST_REPORTED("occurrenceSortEnum.mostReported");


    private final String description;
}
