package felipe.emerim.crimespot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleEnum {

    ROLE_USER("roleEnum.user"),
    ROLE_MODERATOR("roleEnum.moderator"),
    ROLE_ADMIN("roleEnum.admin");

    private final String description;
}


