package felipe.emerim.crimespot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FileTypeEnum {
    OCCURRENCE_DOCUMENT("fileType.occurrenceDocument");

    private final String description;
}
