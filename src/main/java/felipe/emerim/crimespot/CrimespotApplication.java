package felipe.emerim.crimespot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrimespotApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrimespotApplication.class, args);
    }

}
