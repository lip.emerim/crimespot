package felipe.emerim.crimespot.helper;

import java.net.URI;

public final class UrlHelper {

    public static String buildPath(URI uri) {
        String query = uri.getRawQuery();

        if (query == null) {
            return uri.getPath();
        }

        return uri.getPath() + '?' + query;
    }

}
