package felipe.emerim.crimespot.helper;

import lombok.AllArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CacheManagerHelper {

    private final CacheManager cacheManager;

    public void evictCache(String cacheKey, String key) {
        Cache cache = cacheManager.getCache(cacheKey);

        if (cache == null) {
            return;
        }

        if (key != null) {
            cache.evictIfPresent(key);
            return;
        }

        cache.clear();
    }
}
