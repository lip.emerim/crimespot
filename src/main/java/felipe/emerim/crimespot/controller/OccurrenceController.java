package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.CreateEditOccurrenceForm;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import felipe.emerim.crimespot.form.OccurrenceReviewForm;
import felipe.emerim.crimespot.form.OccurrenceSearchForm;
import felipe.emerim.crimespot.service.AddressService;
import felipe.emerim.crimespot.service.OccurrenceService;
import felipe.emerim.crimespot.service.OccurrenceTypeService;
import felipe.emerim.crimespot.service.ReportService;
import lombok.AllArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

@AllArgsConstructor
@Controller
@RequestMapping("/occurrence")
public class OccurrenceController {
    private final OccurrenceService occurrenceService;
    private final ReportService reportService;
    private final OccurrenceTypeService occurrenceTypeService;
    private final AddressService addressService;
    private final Messages messages;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/list")
    public ModelAndView list(
            @AuthenticationPrincipal UserImpl activeUser,
            @ModelAttribute(name = "searchForm") OccurrenceSearchForm occurrenceSearchForm
    ) {

        PaginatedEntity<Occurrence> occurrences = occurrenceService.fullSearch(
                activeUser.getUser(),
                occurrenceSearchForm,
                8
        );

        occurrenceSearchForm.setPage(1);
        ModelAndView mav = new ModelAndView("occurrence/list");
        mav.addObject("occurrences", occurrences);
        mav.addObject("searchForm", occurrenceSearchForm);
        mav.addObject("user", activeUser.getUser());

        return mav;
    }

    @PostMapping("/map-list")
    @ResponseBody
    public PaginatedEntity<Occurrence> list(
            @RequestBody OccurrenceMapSearchForm occurrenceMapSearchForm
    ) {

        return occurrenceService.mapFullSearch(occurrenceMapSearchForm);
    }

    @GetMapping("/create")
    public ModelAndView createGet(Model model) {
        CreateEditOccurrenceForm createEditOccurrenceForm = (CreateEditOccurrenceForm) model.asMap()
                .getOrDefault("form", new CreateEditOccurrenceForm());

        ModelAndView mav = new ModelAndView("occurrence/create");

        mav.addObject("form", createEditOccurrenceForm);
        mav.addObject("occurrenceTypes", occurrenceTypeService.findAll());

        return mav;
    }

    @GetMapping("/edit/{occurrenceId}")
    public ModelAndView editGet(@PathVariable("occurrenceId") Long occurrenceId,
                                @AuthenticationPrincipal UserImpl activeUser,
                                Model model) {
        CreateEditOccurrenceForm editOccurrenceForm = (CreateEditOccurrenceForm) model.asMap()
                .getOrDefault("form", new CreateEditOccurrenceForm());

        Occurrence occurrence = occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        if (!occurrenceService.canEditOccurrence(activeUser.getUser(), occurrence)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        Hibernate.initialize(occurrence.getOccurrenceDocument());

        editOccurrenceForm.setOccurrence(occurrence);
        editOccurrenceForm.setAddress(occurrence.getAddress());

        ModelAndView mav = new ModelAndView("occurrence/edit");

        mav.addObject("form", editOccurrenceForm);
        mav.addObject("occurrenceTypes", occurrenceTypeService.findAll());

        return mav;
    }

    @GetMapping("/view/{occurrenceId}")
    public ModelAndView view(@PathVariable("occurrenceId") Long occurrenceId,
                             @AuthenticationPrincipal UserImpl activeUser,
                             Model model) {
        Occurrence occurrence = occurrenceService.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        OccurrenceReviewForm occurrenceReviewForm = (OccurrenceReviewForm) model.asMap()
                .getOrDefault("form", new OccurrenceReviewForm());

        if (!occurrenceService.canViewOccurrence(activeUser.getUser(), occurrence)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        User user = activeUser.getUser();

        Hibernate.initialize(occurrence.getOccurrenceDocument());

        ModelAndView mav = new ModelAndView("occurrence/view");
        mav.addObject("occurrence", occurrence);
        mav.addObject("isModerator", user.hasRole(RoleEnum.ROLE_MODERATOR));
        mav.addObject("isInReviewByUser", occurrence.isInReviewByUser(user));
        mav.addObject("canRevokeApproval", occurrence.canRevokeApproval(user));
        mav.addObject("canEdit", occurrenceService.canEditOccurrence(user, occurrence));
        mav.addObject("isReviewableByUser", occurrence.isReviewableByUser(user));
        mav.addObject("isDeletableByUser", occurrence.canDeleteOccurrence(user));
        mav.addObject("hasPendingReportByUser", occurrence.hasPendingReportByUser(user));
        mav.addObject("isOwner", occurrence.getUser().getId().equals(user.getId()));
        mav.addObject("form", occurrenceReviewForm);
        mav.addObject("reports", reportService.getActiveReportsByOccurrenceId(occurrenceId));

        return mav;

    }

    @GetMapping("/{occurrenceId}/document/download/{documentId}")
    public ResponseEntity<Resource> downloadDocument(@AuthenticationPrincipal UserImpl activeUser,
                                                     @PathVariable Long occurrenceId, @PathVariable Long documentId) {

        File document = occurrenceService.getDocument(activeUser.getUser(), occurrenceId, documentId);

        byte[] fileContent = document.getDecodedContent();

        InputStream inputStream = new ByteArrayInputStream(fileContent);
        InputStreamResource resource = new InputStreamResource(inputStream);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(fileContent.length)
                .contentType(MediaType.parseMediaType(document.getContentType()))
                .body(resource);
    }

    @PostMapping("/review/{occurrenceId}")
    public String review(@PathVariable("occurrenceId") Long occurrenceId,
                         @AuthenticationPrincipal UserImpl activeUser,
                         RedirectAttributes redirectAttributes) {

        ArrayList<Alert> alerts = new ArrayList<>();

        try {
            occurrenceService.startReview(activeUser.getUser(), occurrenceId);
        } catch (IllegalArgumentException e) {
            alerts.add(Alert.builder()
                    .message(messages.get("occurrence.startReview.error")).type(AlertTypes.DANGER).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return "redirect:/occurrence/list";
        }

        alerts.add(Alert.builder()
                .message(messages.get("occurrence.startReview.success")).type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;
    }

    @PostMapping("/create")
    public String createPost(
            @ModelAttribute
            @Validated CreateEditOccurrenceForm form,
            BindingResult formResult,
            RedirectAttributes redirectAttributes,
            @AuthenticationPrincipal UserImpl activeUser
    ) {

        if (formResult.hasErrors() || !this.save(form, redirectAttributes, activeUser.getUser())) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    formResult
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/occurrence/create";
        }

        return "redirect:/occurrence/list";
    }

    @PostMapping("/edit")
    public String editPost(@ModelAttribute
                           @Validated CreateEditOccurrenceForm form,
                           BindingResult formResult,
                           @AuthenticationPrincipal UserImpl activeUser,
                           RedirectAttributes redirectAttributes) {

        if (!occurrenceService.canEditOccurrence(activeUser.getUser(), form.getOccurrence())) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        if (formResult.hasErrors() || !this.save(form, redirectAttributes, activeUser.getUser())) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    formResult
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/occurrence/edit/" + form.getOccurrence().getId().toString();
        }

        return "redirect:/occurrence/view/" + form.getOccurrence().getId().toString();

    }

    @PostMapping("/approve/{occurrenceId}")
    public String approve(
            @PathVariable("occurrenceId") Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes,
            @ModelAttribute @Validated OccurrenceReviewForm form,
            BindingResult result
    ) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    result
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/occurrence/view/" + occurrenceId;
        }

        occurrenceService.finishReview(activeUser.getUser(), occurrenceId,
                form.getModeratorComments(), OccurrenceStatusEnum.APPROVED);

        ArrayList<Alert> alerts = new ArrayList<>();
        alerts.add(Alert.builder()
                .message(messages.get("occurrence.approved")).type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;

    }

    @PostMapping("/reject/{occurrenceId}")
    public String reject(
            @PathVariable("occurrenceId") Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes,
            @ModelAttribute @Validated OccurrenceReviewForm form,
            BindingResult result
    ) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    result
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/occurrence/view/" + occurrenceId;
        }

        occurrenceService.finishReview(activeUser.getUser(), occurrenceId,
                form.getModeratorComments(), OccurrenceStatusEnum.REJECTED);

        ArrayList<Alert> alerts = new ArrayList<>();
        alerts.add(Alert.builder()
                .message(messages.get("occurrence.rejected")).type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;

    }

    @PostMapping("/revokeApproval/{occurrenceId}")
    public String revokeApproval(
            @PathVariable("occurrenceId") Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes
    ) {
        occurrenceService.revokeApproval(activeUser.getUser(), occurrenceId);

        ArrayList<Alert> alerts = new ArrayList<>();
        alerts.add(Alert.builder()
                .message(messages.get("occurrence.revokedApproval")).type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;
    }

    @PostMapping("/deactivate/{occurrenceId}")
    public String deactivate(
            @PathVariable("occurrenceId") Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes
    ) {
        occurrenceService.deactivate(activeUser.getUser(), occurrenceId);

        ArrayList<Alert> alerts = new ArrayList<>();
        alerts.add(Alert.builder()
                .message(messages.get("occurrence.deactivated")).type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/list";
    }

    @PostMapping("/{occurrenceId}/document/{documentId}/remove")
    public String removeDocument(
            @PathVariable Long occurrenceId,
            @PathVariable Long documentId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes
    ) {

        ArrayList<Alert> alerts = new ArrayList<>();

        occurrenceService.removeDocument(activeUser.getUser(), occurrenceId, documentId);

        alerts.add(Alert.builder()
                .type(AlertTypes.SUCCESS)
                .message(messages.get("occurrence.document.remove.success"))
                .build()
        );

        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;
    }

    @PostMapping("/reports/accept/{occurrenceId}")
    public String acceptReports(
            @PathVariable Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes
    ) {
        ArrayList<Alert> alerts = new ArrayList<>();

        occurrenceService.acceptReports(occurrenceId, activeUser.getUser());

        alerts.add(Alert.builder()
                .type(AlertTypes.SUCCESS)
                .message(messages.get("occurrence.reports.accept.success"))
                .build()
        );

        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;
    }

    @PostMapping("/reports/reject/{occurrenceId}")
    public String rejectReports(
            @PathVariable Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser,
            RedirectAttributes redirectAttributes
    ) {
        ArrayList<Alert> alerts = new ArrayList<>();

        occurrenceService.rejectReports(occurrenceId, activeUser.getUser());

        alerts.add(Alert.builder()
                .type(AlertTypes.SUCCESS)
                .message(messages.get("occurrence.reports.reject.success"))
                .build()
        );

        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/occurrence/view/" + occurrenceId;
    }

    private boolean save(CreateEditOccurrenceForm form, RedirectAttributes redirectAttributes, User activeUser) {
        ArrayList<Alert> alerts = new ArrayList<>();
        Address address = form.getAddress();

        try {
            addressService.geocode(form.getAddress());
        } catch (IllegalArgumentException e) {
            alerts.add(Alert.builder().type(AlertTypes.DANGER)
                    .message(messages.get("address.notFound")).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return false;
        }
        try {
            Occurrence occurrence = form.getOccurrence();
            occurrenceService.setDocument(occurrence, form.getOccurrenceDocument());
            occurrence.setAddress(address);
            occurrenceService.save(activeUser, occurrence);

        } catch (IllegalArgumentException e) {
            alerts.add(Alert.builder().type(AlertTypes.DANGER)
                    .message(messages.get("occurrence.timeout")).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return false;
        } catch (IOException e) {
            alerts.add(Alert.builder().type(AlertTypes.DANGER)
                    .message(messages.get("occurrence.invalidFile")).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return false;
        }

        alerts.add(Alert.builder().type(AlertTypes.SUCCESS)
                .message(messages.get("occurrence.saved")).build());
        redirectAttributes.addFlashAttribute("messages", alerts);
        return true;
    }
}
