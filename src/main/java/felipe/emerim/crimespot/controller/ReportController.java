package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.domain.Alert;
import felipe.emerim.crimespot.domain.AlertTypes;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.exception.ReportAlreadyExistsException;
import felipe.emerim.crimespot.form.ReportCreateForm;
import felipe.emerim.crimespot.service.ReportService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller()
@AllArgsConstructor
public class ReportController {
    final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
    protected ReportService reportService;
    protected Messages messages;

    @PostMapping("/occurrence/{occurrenceId}/report")
    @ResponseBody
    public Alert createReport(
            @AuthenticationPrincipal UserImpl activeUser,
            @PathVariable Long occurrenceId,
            @RequestBody @Validated ReportCreateForm form,
            BindingResult bindingResult
            ) {

        if (bindingResult.hasErrors()) {
            return Alert.builder()
                    .message(messages.get("report.create.fieldErrors"))
                    .type(AlertTypes.WARNING)
                    .build();
        }

        try {
            reportService.createReport(activeUser.getUser(), occurrenceId, form.getDescription());
            return Alert.builder()
                    .message(messages.get("report.create.success"))
                    .type(AlertTypes.SUCCESS)
                    .build();
        } catch (ReportAlreadyExistsException e) {
            return Alert.builder()
                    .message(messages.get("report.create.alreadyReported"))
                    .type(AlertTypes.WARNING)
                    .build();
        } catch (NotFoundException e) {
            return Alert.builder()
                    .message(messages.get("report.create.occurrenceDeactivated"))
                    .type(AlertTypes.WARNING)
                    .build();
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            return Alert.builder()
                    .message(messages.get("report.create.unknownError"))
                    .type(AlertTypes.DANGER)
                    .build();
        }
    }

    @RequestMapping(path = "/occurrence/{occurrenceId}/report", method = RequestMethod.HEAD)
    public ResponseEntity<?> existsByOccurrenceIdAndUserIdAndActiveIsTrue(
            @PathVariable Long occurrenceId,
            @AuthenticationPrincipal UserImpl activeUser
    ) {
        Long userId = activeUser.getUser().getId();

        if (this.reportService.existsByOccurrenceIdAndUserIdAndActiveIsTrue(occurrenceId, userId)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
