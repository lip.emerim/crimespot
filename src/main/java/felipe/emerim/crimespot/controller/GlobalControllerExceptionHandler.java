package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by Felipe Emerim on 13/05/2019
 * purpose: Converts application errors into http errors and logs error stack trace
 * - resourceNotFound, forbidden
 **/

@ControllerAdvice
public class GlobalControllerExceptionHandler extends SimpleMappingExceptionResolver {

    final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
    private final Messages messages;

    public GlobalControllerExceptionHandler(Messages messages) {
        this.messages = messages;
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    protected ModelAndView forbidden(HttpServletRequest req, Exception ex) {

        ModelAndView mav = populateCommonAttributesAndLogError(req, ex);
        mav.addObject("message", messages.get("error.forbidden"));
        mav.addObject("status", HttpStatus.FORBIDDEN);
        return mav;
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    protected ModelAndView notFound(HttpServletRequest req, Exception ex) {

        ModelAndView mav = populateCommonAttributesAndLogError(req, ex);
        mav.addObject("message", messages.get("error.notFound"));
        mav.addObject("status", HttpStatus.NOT_FOUND);
        return mav;
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    protected ModelAndView internal(HttpServletRequest req, Exception ex) {

        ModelAndView mav = populateCommonAttributesAndLogError(req, ex);
        mav.addObject("message", messages.get("error.internal"));
        mav.addObject("status", HttpStatus.INTERNAL_SERVER_ERROR);
        return mav;
    }

    private ModelAndView populateCommonAttributesAndLogError(HttpServletRequest req, Exception ex) {

        logger.error(ExceptionUtils.getStackTrace(ex));

        ModelAndView mav = new ModelAndView("error");

        mav.addObject("path", req.getRequestURL());
        mav.addObject("timestamp", new Date());
        mav.addObject("error", ex.getClass());

        return mav;
    }

}
