package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.domain.GeocodingAddress;
import felipe.emerim.crimespot.service.AddressService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@AllArgsConstructor
@Controller
@RequestMapping("/address")
public class AddressController {

    private final AddressService addressService;

    @GetMapping("search")
    @ResponseBody
    public GeocodingAddress search(@RequestParam String query) {
        if (query == null || query.length() < 5) {
            return null;
        }
        try {
            return addressService.search(query);
        } catch (IllegalArgumentException e) {
            return null;
        }

    }
}
