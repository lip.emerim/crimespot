package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by carina on 05/20/19.
 */
@AllArgsConstructor
@Controller
@RequestMapping("/")
public class HomeController {
    @GetMapping("")
    public ModelAndView dashboard(@AuthenticationPrincipal UserImpl activeUser) {

        ModelAndView mav = new ModelAndView("index");

        mav.addObject("form", new OccurrenceMapSearchForm());

        if (activeUser != null) {
            mav.addObject("userLatitude", activeUser.getUser().getAddress().getLatitude());
            mav.addObject("userLongitude", activeUser.getUser().getAddress().getLongitude());
        }

        return mav;

    }

}
