package felipe.emerim.crimespot.controller;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.ChangePasswordForm;
import felipe.emerim.crimespot.form.CreateEditUserForm;
import felipe.emerim.crimespot.form.UserSearchForm;
import felipe.emerim.crimespot.helper.UrlHelper;
import felipe.emerim.crimespot.service.AddressService;
import felipe.emerim.crimespot.service.UserService;
import felipe.emerim.crimespot.validation.group.AddressGroup;
import felipe.emerim.crimespot.validation.group.CreateUserGroup;
import felipe.emerim.crimespot.validation.group.EditUserGroup;
import lombok.AllArgsConstructor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.net.URI;
import java.util.ArrayList;

@AllArgsConstructor
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final AddressService addressService;
    private final Messages messages;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/create")
    public ModelAndView createGet(Model model) {

        CreateEditUserForm form = (CreateEditUserForm) model.asMap().getOrDefault("form", new CreateEditUserForm());

        ModelAndView mav = new ModelAndView("user/create");
        mav.addObject("form", form);

        return mav;
    }

    @GetMapping("/edit/{userId}")
    public ModelAndView editGet(@PathVariable("userId") Long userId,
                                @AuthenticationPrincipal UserImpl activeUser,
                                Model model) {
        if (!activeUser.getUser().getId().equals(userId)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        User user = userService.findById(userId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        CreateEditUserForm editUserForm = (CreateEditUserForm) model.asMap()
                .getOrDefault("form",
                        CreateEditUserForm
                                .builder()
                                .user(user)
                                .address(user.getAddress())
                                .build());

        ModelAndView mav = new ModelAndView("user/edit");

        mav.addObject("form", editUserForm);
        mav.addObject("isAdmin", editUserForm.getUser().getMainRole() == RoleEnum.ROLE_ADMIN);

        return mav;
    }

    @GetMapping("/list")
    public ModelAndView list(
            @AuthenticationPrincipal UserImpl activeUser,
            @ModelAttribute(name = "searchForm") UserSearchForm userSearchForm
    ) {
        ModelAndView mav = new ModelAndView("user/list");

        PaginatedEntity<User> results = userService.findNearestBySearchTermAndRole(
                activeUser.getUser(),
                userSearchForm);

        userSearchForm.setPage(1);
        mav.addObject("users", results);
        mav.addObject("searchForm", userSearchForm);

        return mav;
    }

    @GetMapping("/change-password")
    public ModelAndView changePassword(
            Model model,
            @AuthenticationPrincipal UserImpl activeUser
    ) {

        ChangePasswordForm changePasswordForm = (ChangePasswordForm) model.asMap()
                .getOrDefault("form", new ChangePasswordForm());

        ModelAndView mav = new ModelAndView("user/change-password");
        mav.addObject("form", changePasswordForm);
        mav.addObject("user", activeUser.getUser());

        return mav;
    }


    @PostMapping("/create")
    public String createPost(@ModelAttribute
                             @Validated({CreateUserGroup.class, AddressGroup.class}) CreateEditUserForm form,
                             BindingResult formResult,
                             RedirectAttributes redirectAttributes) {

        if (formResult.hasErrors() || !this.save(form, redirectAttributes, null)) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    formResult
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/user/create";
        }

        return "redirect:/login";
    }

    @PostMapping("/edit")
    public String editPost(@ModelAttribute
                           @Validated({EditUserGroup.class, AddressGroup.class}) CreateEditUserForm form,
                           BindingResult formResult,
                           @AuthenticationPrincipal UserImpl activeUser,
                           RedirectAttributes redirectAttributes) {

        if (formResult.hasErrors() || !this.save(form, redirectAttributes, activeUser.getUser())) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    formResult
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/user/edit/" + form.getUser().getId().toString();
        }

        return "redirect:/";

    }

    @PostMapping("/toggle")
    public String toggle(@RequestParam Long userId,
                         @RequestParam(defaultValue = "/user/list") URI originUrl,
                         RedirectAttributes redirectAttributes,
                         @AuthenticationPrincipal UserImpl activeUser) {

        ArrayList<Alert> alerts = new ArrayList<>();

        User user = userService.toggle(activeUser.getUser(), userId);

        String messageKey = "user.deactivate.success";
        if (user.isActive()) {
            messageKey = "user.activate.success";
        }

        alerts.add(Alert.builder().message(messages.get(messageKey, user.getUsername()))
                .type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);
        return "redirect:" + UrlHelper.buildPath(originUrl);

    }

    @PostMapping("/toggle-moderator")
    public String toggleModerator(@RequestParam Long userId,
                                  @RequestParam(defaultValue = "/user/list") URI originUrl,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserImpl activeUser) {

        ArrayList<Alert> alerts = new ArrayList<>();

        User user = userService.toggleModerator(activeUser.getUser(), userId);

        String messageKey = "user.deactivate.moderator.success";
        if (user.isModerator()) {
            messageKey = "user.activate.moderator.success";
        }

        alerts.add(Alert.builder().message(messages.get(messageKey, user.getUsername()))
                .type(AlertTypes.SUCCESS).build());
        redirectAttributes.addFlashAttribute("messages", alerts);
        return "redirect:" + UrlHelper.buildPath(originUrl);

    }

    @PostMapping("/change-password")
    public String createPost(@ModelAttribute
                             @Validated ChangePasswordForm form,
                             BindingResult formResult,
                             RedirectAttributes redirectAttributes,
                             @AuthenticationPrincipal UserImpl activeUser) {
        ArrayList<Alert> alerts = new ArrayList<>();

        if (formResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(
                    "org.springframework.validation.BindingResult.form",
                    formResult
            );
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/user/change-password";
        }

        try {
            this.userService.changePassword(activeUser.getUser(), form);
        } catch (IllegalArgumentException e) {
            alerts.add(Alert.builder().type(AlertTypes.DANGER)
                    .message(messages.get("user.passwordChange.invalidOriginal")).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return "redirect:/user/change-password";
        }

        alerts.add(Alert.builder().type(AlertTypes.SUCCESS)
                .message(messages.get("user.passwordChange.success")).build());
        redirectAttributes.addFlashAttribute("messages", alerts);

        return "redirect:/user/edit/" + activeUser.getUser().getId();
    }

    private boolean save(CreateEditUserForm form, RedirectAttributes redirectAttributes, User activeUser) {
        ArrayList<Alert> alerts = new ArrayList<>();
        Address address = form.getAddress();

        try {
            addressService.geocode(form.getAddress());
        } catch (IllegalArgumentException e) {
            alerts.add(Alert.builder().type(AlertTypes.DANGER)
                    .message(messages.get("address.notFound")).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return false;
        }
        try {
            User user = form.getUser();
            user.setAddress(address);
            userService.save(activeUser, user);

        } catch (IllegalArgumentException e) {
            alerts.add(Alert.builder().type(AlertTypes.DANGER)
                    .message(messages.get("user.exist")).build());
            redirectAttributes.addFlashAttribute("messages", alerts);
            return false;
        }

        alerts.add(Alert.builder().type(AlertTypes.SUCCESS)
                .message(messages.get("user.saved")).build());
        redirectAttributes.addFlashAttribute("messages", alerts);
        return true;
    }
}
