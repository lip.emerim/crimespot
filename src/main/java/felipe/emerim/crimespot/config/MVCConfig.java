package felipe.emerim.crimespot.config;

import com.google.maps.GeoApiContext;
import felipe.emerim.crimespot.components.PendingOccurrenceInterceptor;
import felipe.emerim.crimespot.config.auth.AnonymousOnlyInterceptor;
import felipe.emerim.crimespot.config.auth.VerifyAccessInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.util.Locale;

@Configuration
@EnableCaching
public class MVCConfig implements WebMvcConfigurer {

    private final VerifyAccessInterceptor verifyAccessInterceptor;
    private final PendingOccurrenceInterceptor pendingOccurrenceInterceptor;
    private final AnonymousOnlyInterceptor anonymousOnlyInterceptor;

    public MVCConfig(
            VerifyAccessInterceptor verifyAccessInterceptor,
            PendingOccurrenceInterceptor pendingOccurrenceInterceptor,
            AnonymousOnlyInterceptor anonymousOnlyInterceptor
    ) {
        this.verifyAccessInterceptor = verifyAccessInterceptor;
        this.pendingOccurrenceInterceptor = pendingOccurrenceInterceptor;
        this.anonymousOnlyInterceptor = anonymousOnlyInterceptor;
    }

    @Value("${GOOGLE_MAPS_API_KEY:dummy}")
    protected String apiKey;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/auth/securedPage");
        registry.addViewController("/auth/loginFailure");
        registry.addViewController("/login").setViewName("auth/login");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(anonymousOnlyInterceptor)
                .excludePathPatterns("/webjars/**",
                "/css/**",
                "/js/**").addPathPatterns("/**");;
        registry.addInterceptor(verifyAccessInterceptor)
                .excludePathPatterns("/webjars/**",
                        "/css/**",
                        "/js/**").addPathPatterns("/**");

        registry.addInterceptor(pendingOccurrenceInterceptor)
                .excludePathPatterns("/webjars/**",
                        "/css/**",
                        "/js/**").addPathPatterns("/**");
    }

    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean beanValidator = new LocalValidatorFactoryBean();
        beanValidator.setValidationMessageSource(messageSource());
        return beanValidator;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:messages", "classpath:ValidationMessages");
        messageSource.setDefaultEncoding("utf-8");
        return messageSource;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Locale change interceptor.
     *
     * @return the locale change interceptor
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Bean
    public LocaleResolver localeResolver() {
        LocaleContextHolder.setDefaultLocale(new Locale("pt", "BR"));
        CookieLocaleResolver slr = new CookieLocaleResolver();
        slr.setDefaultLocale(LocaleContextHolder.getLocale());
        return slr;
    }

    // spring automatically detects the shutdown method and closes the context for us
    @Bean
    public GeoApiContext geoApiContext() {
        return new GeoApiContext
                .Builder()
                .apiKey(apiKey)
                .build();
    }
}
