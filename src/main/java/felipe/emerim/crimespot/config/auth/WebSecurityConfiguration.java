package felipe.emerim.crimespot.config.auth;

import felipe.emerim.crimespot.service.UserDetailsImplService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * App auth configuration for development and testing
 */
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
@EnableWebSecurity
class WebSecurityConfiguration implements WebMvcConfigurer {

    @Bean
    public WebSecurityCustomizer configureWebSecurity() {
        return (web) -> web.ignoring().antMatchers(
                "/webjars/**",
                "/css/**",
                "/js/**",
                "/png/**"
        );
    }

    @Autowired
    public void configureAuth(AuthenticationManagerBuilder auth) {
        auth.eraseCredentials(false);
    }


    @Bean
    public SecurityFilterChain configureHttpSecurity(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        "/address/search/**",
                        "/occurrence/map-list",
                        "/user/create",
                        "/login",
                        "/").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/?logout=true")
                .invalidateHttpSession(true)
                .and()
                .csrf()
                .disable();

        return http.build();
    }
}