package felipe.emerim.crimespot.config.auth;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Component
public class AnonymousOnlyInterceptor implements HandlerInterceptor {

    protected static String[] anonymousOnlyHandlers = new String[] {
        "/login",
        "/user/create"
    };

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException, IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String endpoint = request.getRequestURI();

        boolean anonymousOnly = Arrays.stream(AnonymousOnlyInterceptor.anonymousOnlyHandlers).anyMatch(
                (h -> h.equals(endpoint))
        );

        boolean isAuthenticated = auth != null &&
                auth.isAuthenticated() &&
                !(auth instanceof AnonymousAuthenticationToken);

        if ( isAuthenticated && anonymousOnly) {
            response.sendRedirect("/");
            return false;
        }
        return true;
    }

}
