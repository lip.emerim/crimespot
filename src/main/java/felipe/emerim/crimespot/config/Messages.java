package felipe.emerim.crimespot.config;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by rodrigo on 3/16/17.
 */
@Component
public class Messages {

    private final MessageSource messageSource;
    private MessageSourceAccessor accessor;

    public Messages(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource, LocaleContextHolder.getLocale());
    }

    /**
     * Get a message without parameters
     *
     * @param code The message code
     * @return The message String
     */
    public String get(String code) {

        return accessor.getMessage(code, LocaleContextHolder.getLocale());
    }

    /**
     * Get a message with parameters
     *
     * @param code   The message code
     * @param params The message parameters
     * @return The message String
     */
    public String get(String code, String... params) {

        return accessor.getMessage(code, params, LocaleContextHolder.getLocale());
    }

}