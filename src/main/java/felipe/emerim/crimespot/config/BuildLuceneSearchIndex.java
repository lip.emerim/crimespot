package felipe.emerim.crimespot.config;

import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 * Reindex all hibernate search entities when the application starts
 */
@Configuration
public class BuildLuceneSearchIndex {

    @PersistenceUnit
    EntityManagerFactory emf;

    @PostConstruct
    public void build() throws InterruptedException {
        EntityManager em = emf.createEntityManager();
        SearchSession searchSession = Search.session(em);
        searchSession.massIndexer()
                .mergeSegmentsOnFinish(true)
                .mergeSegmentsAfterPurge(true)
                .startAndWait();
    }
}
