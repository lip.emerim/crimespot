package felipe.emerim.crimespot.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class PaginatedEntity<T> {
    List<T> data;
    Integer currentPage;
    Long totalResults;
    Integer pageLength;

    public Double getTotalPages() {
        if (totalResults == 0 || pageLength == 0) {
            return 0.0;
        }

        return Math.ceil(totalResults / (double) pageLength);
    }
}
