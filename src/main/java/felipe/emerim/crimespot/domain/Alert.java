package felipe.emerim.crimespot.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Alert {

    private AlertTypes type;

    private String message;
}
