package felipe.emerim.crimespot.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Indexed
@EqualsAndHashCode(exclude = {"user", "occurrenceDocument", "address", "reports"})
@ToString(exclude = {"reports"})
@Table(indexes = {
        @javax.persistence.Index(columnList = "moderator_id"),
        @javax.persistence.Index(columnList = "user_id"),
})
public class Occurrence implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{field.required}")
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    @Size(min = 10, max = 100, message = "{field.title}")
    private String title;

    @NotBlank(message = "{field.required}")
    @Size(min = 15, max = 1000, message = "{occurrence.description.length}")
    private String description;

    @Size(min = 15, max = 1000, message = "{occurrence.moderatorComments.length}")
    private String moderatorComments;

    @GenericField(searchable = Searchable.YES, sortable = Sortable.YES)
    @Builder.Default
    private long reportsCount = 0;

    @GenericField(searchable = Searchable.YES)
    @Builder.Default
    private boolean active = true;

    @Builder.Default
    @GenericField(searchable = Searchable.YES)
    private boolean isVerified = false;

    @CreationTimestamp
    @GenericField(searchable = Searchable.YES, sortable = Sortable.YES)
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime updatedAt;

    @ManyToOne
    @IndexedEmbedded(includeEmbeddedObjectId = true)
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.DEFAULT)
    @JsonManagedReference
    private User user;

    @ManyToOne
    @IndexedEmbedded(includeEmbeddedObjectId = true)
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    @JsonManagedReference
    private User moderator;

    @ManyToOne
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private OccurrenceStatus occurrenceStatus;

    @ManyToOne
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    @NotNull(message = "{field.required}")
    private OccurrenceType occurrenceType;

    @PastOrPresent(message = "{field.pastOrPresent}")
    @NotNull(message = "{field.required}")
    @GenericField(searchable = Searchable.YES, sortable = Sortable.YES)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate occurrenceDate;

    @OneToOne(cascade = CascadeType.ALL)
    @IndexedEmbedded
    private Address address;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private File occurrenceDocument;

    @OneToMany(mappedBy = "occurrence", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    private Set<Report> reports = new HashSet<>();

    @JsonIgnore
    public boolean isReviewable() {
        return this.getOccurrenceStatus()
                .getDescription() == OccurrenceStatusEnum.WAITING_ANALYSIS;
    }

    public boolean isReviewableByUser(User user) {
        boolean isModerator = user.hasRole(RoleEnum.ROLE_MODERATOR);
        boolean isReviewable = this.isReviewable();

        return isReviewable && isModerator;
    }

    public boolean hasPendingReportByUser(User user) {
        boolean isAdmin = user.hasRole(RoleEnum.ROLE_ADMIN);
        boolean hasReports = this.reportsCount > 0;
        boolean isApproved = this.occurrenceStatus.getDescription().equals(OccurrenceStatusEnum.APPROVED);

        return isAdmin && hasReports && isApproved;
    }

    public boolean isInReviewByUser(User user) {
        if (this.moderator == null) {
            return false;
        }

        boolean inAnalysis = this.getOccurrenceStatus().getDescription() == OccurrenceStatusEnum.IN_ANALYSIS;
        boolean isModerator = this.moderator.getId().equals(user.getId());

        return inAnalysis && isModerator;
    }

    public boolean canDeleteOccurrence(User user) {
        boolean isOwner = user.getId().equals(this.user.getId());
        boolean isAdmin = user.hasRole(RoleEnum.ROLE_ADMIN);

        return this.isActive() && (isOwner || isAdmin);
    }

    public boolean canRevokeApproval(User user) {
        boolean isAdmin = user.hasRole(RoleEnum.ROLE_ADMIN);
        boolean isApproved = this.occurrenceStatus.getDescription() == OccurrenceStatusEnum.APPROVED;

        return isAdmin && isApproved;
    }
}
