package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.enums.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by rodrigo on 3/18/17.
 */
@Entity
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Indexed
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @GenericField(searchable = Searchable.YES)
    @Enumerated(EnumType.STRING)
    private RoleEnum role;
}