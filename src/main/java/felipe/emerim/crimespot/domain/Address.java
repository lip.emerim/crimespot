package felipe.emerim.crimespot.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import felipe.emerim.crimespot.validation.group.AddressGroup;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.bridge.builtin.annotation.GeoPointBinding;
import org.hibernate.search.mapper.pojo.bridge.builtin.annotation.Latitude;
import org.hibernate.search.mapper.pojo.bridge.builtin.annotation.Longitude;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@GeoPointBinding(
        fieldName = "location",
        markerSet = "coordinates",
        projectable = Projectable.YES,
        sortable = Sortable.YES
)
@EqualsAndHashCode(exclude = {"user", "occurrence"})
@ToString(exclude = {"user", "occurrence"})
@Indexed
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{field.required}", groups = {AddressGroup.class})
    @Size(max = 1000, groups = {AddressGroup.class}, message = "{field.max}")
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String country;

    @NotBlank(message = "{field.required}", groups = {AddressGroup.class})
    @Size(max = 1000, groups = {AddressGroup.class}, message = "{field.max}")
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String state;

    @NotBlank(message = "{field.required}", groups = {AddressGroup.class})
    @Size(max = 1000, groups = {AddressGroup.class}, message = "{field.max}")
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String city;

    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String formattedAddress;

    @NotBlank(message = "{field.required}", groups = {AddressGroup.class})
    @Size(max = 1000, groups = {AddressGroup.class}, message = "{field.max}")
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String street;

    @NotBlank(message = "{field.required}", groups = {AddressGroup.class})
    @Size(max = 1000, groups = {AddressGroup.class}, message = "{field.max}")
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String neighborhood;

    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    @Size(max = 11, groups = {AddressGroup.class}, message = "{field.max}")
    @Pattern(regexp = "[\\d]{0,11}", message = "{field.invalidNumber}", groups = {AddressGroup.class})
    private String number;

    @NotNull(message = "{field.required}", groups = {AddressGroup.class})
    @Latitude(markerSet = "coordinates")
    private double latitude;

    @NotNull(message = "{field.required}", groups = {AddressGroup.class})
    @Longitude(markerSet = "coordinates")
    private double longitude;

    @NotBlank(message = "{field.required}", groups = {AddressGroup.class})
    @Pattern(regexp = "[\\d]{5}-[\\d]{3}", message = "{field.invalidPostalCode}", groups = {AddressGroup.class})
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    private String postalCode;

    @Size(max = 1000, groups = {AddressGroup.class}, message = "{field.max}")
    private String complement;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime updatedAt;

    @OneToOne(mappedBy = "address")
    @JsonBackReference
    private User user;

    @OneToOne(mappedBy = "address")
    @JsonBackReference
    private Occurrence occurrence;

    @Builder.Default
    private boolean active = true;

    /**
     * Convert the address to a google geocode API query string.
     * <p>
     * This should be used to geocode the address
     *
     * @return The query string
     */
    @JsonIgnore
    public String getAddressForGeocodeRequest() {
        return this.street +
                (this.number == null ? "" : ", " + this.number) +
                " - " +
                this.neighborhood +
                ", " +
                this.city +
                " - " +
                this.state +
                ", " +
                this.postalCode +
                ", " +
                this.country;
    }


}
