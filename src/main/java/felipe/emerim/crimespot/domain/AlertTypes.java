package felipe.emerim.crimespot.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AlertTypes {
    DANGER("danger"),
    WARNING("warning"),
    SUCCESS("success"),
    INFO("info");

    private final String description;

}
