package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.enums.FileTypeEnum;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"occurrence"})
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{field.required}")
    private String contentType;

    @NotNull(message = "{field.required}")
    @NotEmpty(message = "{field.required}")
    private byte[] fileContent;

    @Enumerated(EnumType.STRING)
    private FileTypeEnum fileType;

    @OneToOne(mappedBy = "occurrenceDocument")
    private Occurrence occurrence;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime updatedAt;

    public void setContentFromMultipartFile(MultipartFile multipartFile) throws IOException {
        this.fileContent = org.apache.commons.codec.binary.Base64.encodeBase64(multipartFile.getBytes());
    }

    public byte[] getDecodedContent() {
        return org.apache.commons.codec.binary.Base64.decodeBase64(this.fileContent);
    }

    /**
     * Update file content, type and content type from another file
     *
     * @param file The file with the data to use in the update
     */
    public void update(File file) {
        this.contentType = file.getContentType();
        this.fileType = file.getFileType();
        this.fileContent = file.getFileContent();
    }
}
