package felipe.emerim.crimespot.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Represent a google maps address that our google maps service uses.
 * <p>
 * It serves as a mean to interact with the google maps API, since we
 * cannot use our address entity directly there
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeocodingAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    private String country;
    private String state;
    private String city;
    private String formattedAddress;
    private String street;
    private String neighborhood;
    private String number;
    private String postalCode;
    private Double longitude;
    private Double latitude;
}
