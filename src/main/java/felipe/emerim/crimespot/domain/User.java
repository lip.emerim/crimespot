package felipe.emerim.crimespot.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.validation.group.CreateUserGroup;
import felipe.emerim.crimespot.validation.group.EditUserGroup;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Indexed
@EqualsAndHashCode(exclude = {"createdOccurrences", "moderatedOccurrences", "address", "reports"})
@ToString(exclude = {"createdOccurrences", "moderatedOccurrences", "reports"})
@Table(name = "`user`",
        indexes = {
                @javax.persistence.Index(columnList = "username", unique = true),
                @javax.persistence.Index(columnList = "cpf", unique = true),
                @javax.persistence.Index(columnList = "email", unique = true),
        })
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO, name = "username_data")
    @KeywordField(searchable = Searchable.YES, sortable = Sortable.YES, normalizer = "lowercase", name = "username_sort")
    @NotBlank(message = "{field.required}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @Size(min = 5, max = 30, message = "{user.username.length}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @Column(unique = true)
    @Pattern(regexp = "^[a-zA-Z0-9._-]{5,30}$", message = "{user.username.pattern}",
            groups = {CreateUserGroup.class, EditUserGroup.class})
    private String username;

    @Builder.Default
    @GenericField(searchable = Searchable.YES)
    private boolean active = true;

    @NotBlank(message = "{field.required}", groups = {CreateUserGroup.class})
    @Size(min = 5, max = 500, message = "{user.password.length}", groups = {CreateUserGroup.class})
    @JsonIgnore
    private String password;

    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.EAGER)
    @NotNull(message = "{field.required}")
    @Builder.Default
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    private Set<Occurrence> createdOccurrences = new HashSet<>();

    @OneToMany(mappedBy = "moderator", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    private Set<Occurrence> moderatedOccurrences = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @Builder.Default
    @JsonBackReference
    private Set<Report> reports = new HashSet<>();

    @NotBlank(message = "{field.required}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    @Size(min = 5, max = 50, message = "{user.name.length}", groups = {CreateUserGroup.class, EditUserGroup.class})
    private String name;

    @NotBlank(message = "{field.required}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @Email(message = "{field.email}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @FullTextField(searchable = Searchable.YES, projectable = Projectable.NO)
    @Column(unique = true)
    @Size(min = 5, max = 100, message = "{user.email.length}", groups = {CreateUserGroup.class, EditUserGroup.class})
    private String email;

    @NotNull(message = "{field.required}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @Past(message = "{field.past}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateOfBirth;

    @NotBlank(message = "{field.required}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @CPF(message = "{field.cpf}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @GenericField(searchable = Searchable.YES)
    @Column(unique = true)
    private String cpf;

    @OneToOne(cascade = CascadeType.ALL)
    @IndexedEmbedded
    @JsonManagedReference
    private Address address;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime updatedAt;

    @Min(value = 1, message = "{user.searchRadius.min}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @Max(value = 10, message = "{user.searchRadius.max}", groups = {CreateUserGroup.class, EditUserGroup.class})
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Double searchRadius;

    @Transient
    @Value("${MIN_AGE:18}")
    private int minAge;

    public boolean isOlderThan(Integer age) {
        return Period.between(dateOfBirth, LocalDate.now()).getYears() > minAge;
    }

    @JsonIgnore
    public boolean isModerator() {
        return this.hasRole(RoleEnum.ROLE_MODERATOR);
    }

    @Transient
    @GenericField(searchable = Searchable.YES)
    @IndexingDependency(derivedFrom = @ObjectPath(
            @PropertyValue(propertyName = "roles")
    ))
    public RoleEnum getMainRole() {
        if (this.hasRole(RoleEnum.ROLE_ADMIN)) {
            return RoleEnum.ROLE_ADMIN;
        }

        if (this.hasRole(RoleEnum.ROLE_MODERATOR)) {
            return RoleEnum.ROLE_MODERATOR;
        }

        return RoleEnum.ROLE_USER;
    }

    public boolean hasRole(RoleEnum role) {
        return this.roles.stream().anyMatch(r -> r.getRole() == role);
    }

    public boolean canToggleUser(User user) {
        boolean isAdmin = this.hasRole(RoleEnum.ROLE_ADMIN);
        boolean isModerator = this.hasRole(RoleEnum.ROLE_MODERATOR);
        boolean isAuthenticatedUser = user.getMainRole() == RoleEnum.ROLE_USER;
        boolean editedUserIsAdmin = user.hasRole(RoleEnum.ROLE_ADMIN);
        boolean isCurrentUser = user.equals(this);

        if (editedUserIsAdmin) {
            return false;
        }

        return isAdmin || (isModerator && isAuthenticatedUser) || isCurrentUser;
    }

    public User addRole(Role role) {
        this.getRoles().add(role);

        return this;
    }

    public User removeRole(Role role) {
        this.getRoles().remove(role);
        return this;
    }
}
