package felipe.emerim.crimespot.domain;

import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Indexed
public class OccurrenceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @GenericField(searchable = Searchable.YES)
    private OccurrenceTypeEnum description;
}
