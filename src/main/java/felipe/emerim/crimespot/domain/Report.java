package felipe.emerim.crimespot.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexingDependency;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@Indexed
@EqualsAndHashCode(exclude = {"user", "occurrence"})
@Table(indexes = {
        @javax.persistence.Index(columnList = "occurrence_id"),
        @javax.persistence.Index(columnList = "user_id"),
})
public class Report implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{field.required}")
    @Size(min = 15, max = 1000, message = "{report.description.length}")
    private String description;

    @CreationTimestamp
    @GenericField(searchable = Searchable.YES, sortable = Sortable.YES)
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private LocalDateTime updatedAt;

    @ManyToOne
    @NotNull
    @IndexedEmbedded(includeEmbeddedObjectId = true)
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.DEFAULT)
    @JsonManagedReference
    private User user;

    @ManyToOne
    @NotNull
    @IndexedEmbedded(includeEmbeddedObjectId = true)
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.DEFAULT)
    @JsonManagedReference
    private Occurrence occurrence;

    @GenericField(searchable = Searchable.YES)
    @Builder.Default
    private boolean active = true;
}
