package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.GeocodingAddress;
import felipe.emerim.crimespot.repository.AddressRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AddressService {

    private final GeocodingService geocodingService;
    private final AddressRepository addressRepository;

    /**
     * Searches the google Geocode API for an address as String
     *
     * @param query The address String
     * @return The GoogleMapsAddress object with data
     */
    public GeocodingAddress search(String query) {
        return geocodingService.geocode(query);
    }

    public Address save(Address address) {
        this.geocode(address);
        return addressRepository.save(address);
    }

    public void delete(Address address) {
        addressRepository.delete(address);
    }

    public Optional<Address> findById(Long id) {
        return addressRepository.findById(id);
    }

    /**
     * Search an address on the geocode API and update its information.
     * <p>
     * Any information that the API can find overwrites the current address information.
     * For example: The geocodeAddress has a country, we set the country of our address with
     * that value. In case it does not have a country, we leave the original value as is.
     * <p>
     * That happens because the google geocode API information is more reliable and is always stored
     * same way, without naming conflicts. However we cannot throw an Exception in case an information is not found
     * because it is rather common that the API will not find a field, especially the postal code.
     *
     * @param address The CrimeSpot address object to geocode
     */
    public void geocode(Address address) {
        GeocodingAddress geocodingAddress = this.search(address.getAddressForGeocodeRequest());

        address.setCountry(Optional.ofNullable(geocodingAddress.getCountry()).orElse(address.getCountry()));
        address.setState(Optional.ofNullable(geocodingAddress.getState()).orElse(address.getState()));
        address.setCity(Optional.ofNullable(geocodingAddress.getCity()).orElse(address.getCity()));
        address.setLatitude(geocodingAddress.getLatitude());
        address.setLongitude(geocodingAddress.getLongitude());
        address.setFormattedAddress(address.getAddressForGeocodeRequest());
    }
}
