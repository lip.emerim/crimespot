package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.OccurrenceType;
import felipe.emerim.crimespot.repository.OccurrenceTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OccurrenceTypeService {

    private final OccurrenceTypeRepository occurrenceTypeRepository;

    public List<OccurrenceType> findAll() {
        return occurrenceTypeRepository.findAll();
    }
}
