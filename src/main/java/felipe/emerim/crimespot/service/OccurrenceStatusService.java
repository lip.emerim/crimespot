package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.OccurrenceStatus;
import felipe.emerim.crimespot.repository.OccurrenceStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OccurrenceStatusService {

    private final OccurrenceStatusRepository occurrenceStatusRepository;

    public List<OccurrenceStatus> findAll() {
        return occurrenceStatusRepository.findAll();
    }

}
