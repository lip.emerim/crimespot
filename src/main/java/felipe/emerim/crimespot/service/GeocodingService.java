package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.domain.GeocodingAddress;

public interface GeocodingService {
    GeocodingAddress geocode(String address);
}
