package felipe.emerim.crimespot.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * This class is a wrapper to the GoogleMapsServices package static methods we use.
 * <p>
 * This exists so that we can mock this class and avoid making real requests
 */
@Service
public class GoogleMapsWrapperService {
    protected GeoApiContext geoApiContext;

    @Value("${GOOGLE_MAPS_API_LANGUAGE:pt-BR}")
    protected String language;

    public GoogleMapsWrapperService(GeoApiContext geoApiContext) {
        this.geoApiContext = geoApiContext;
    }

    public GeocodingResult[] geocode(String address) throws InterruptedException, ApiException, IOException {
        return GeocodingApi
                .geocode(this.geoApiContext, address)
                .language(this.language)
                .await();
    }
}
