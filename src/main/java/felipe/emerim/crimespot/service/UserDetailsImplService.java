package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

/**
 * Created by rodrigo on 2/21/17.
 */
@Service
@AllArgsConstructor
public class UserDetailsImplService implements UserDetailsService {

    private final UserRepository userRepository;

    /**
     * Load user authentication data
     *
     * @param username The name of the user
     * @return The auth user data
     * @throws UsernameNotFoundException In case the username does not exist.
     */
    @Override
    @Cacheable(value = "user_auth", key = "#username")
    public UserImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.userRepository.findByUsernameAndActiveIsTrue(username)
                .map(user -> new UserImpl(
                        user.getUsername(),
                        user.getPassword(),
                        user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getRole().name())).collect(Collectors.toList()),
                        user)
                ).orElseThrow(() -> new UsernameNotFoundException("couldn't find " + username + "!"));
    }
}