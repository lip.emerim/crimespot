package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.dao.OccurrenceDao;
import felipe.emerim.crimespot.domain.*;
import felipe.emerim.crimespot.enums.FileTypeEnum;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import felipe.emerim.crimespot.form.OccurrenceSearchForm;
import felipe.emerim.crimespot.repository.FileRepository;
import felipe.emerim.crimespot.repository.OccurrenceRepository;
import felipe.emerim.crimespot.repository.OccurrenceStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class OccurrenceService {

    private static final int OCCURRENCE_TIMEOUT = 5;
    private final OccurrenceRepository occurrenceRepository;
    private final OccurrenceStatusRepository occurrenceStatusRepository;
    private final FileRepository fileRepository;
    private final OccurrenceDao occurrenceDao;
    private final Messages messages;
    private final ReportService reportService;


    /**
     * Check if the user has created an occurrence in the last n minutes
     * <p>
     * This is to prevent occurrence spamming from the same user. If he/she created
     * an occurrence with a certain type, he/she will have to wait to create another
     *
     * @param type The occurrence type
     * @param user The user
     * @return Indicates whether an occurrence was created or not.
     */
    public boolean waitedSinceLastOccurrenceByTypeAndUser(OccurrenceType type, User user) {

        Occurrence lastOccurrence = occurrenceRepository.findByOccurrenceTypeAndUserOrderByCreatedAtDesc(type, user);

        if (lastOccurrence == null) {
            return true;
        }

        return Duration.between(lastOccurrence.getCreatedAt(), LocalDateTime.now()).toMinutes() > OCCURRENCE_TIMEOUT;
    }

    public Optional<Occurrence> findByIdAndUserActiveIsTrueAndActiveIsTrue(Long id) {
        return occurrenceRepository.findByIdAndUserActiveIsTrueAndActiveIsTrue(id);
    }

    public Occurrence save(User user, Occurrence occurrence) {

        if (occurrence.getId() == null) {
            return this.create(user, occurrence);
        }

        if (!this.canEditOccurrence(user, occurrence)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        return this.edit(user, occurrence);
    }

    /**
     * Start the review process of an occurrence
     * <p>
     * To review an occurrence the user needs to be a moderator.
     *
     * @param user         The user that will review the occurrence
     * @param occurrenceId The id of the occurrence to be reviewed
     * @return The occurrence
     */
    public Occurrence startReview(User user, Long occurrenceId) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        if (!user.hasRole(RoleEnum.ROLE_MODERATOR)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        if (!occurrence.isReviewableByUser(user)) {
            throw new IllegalArgumentException("Occurrence cannot be reviewed");
        }

        occurrence.setModerator(user);
        occurrence.setOccurrenceStatus(occurrenceStatusRepository.findByDescription(OccurrenceStatusEnum.IN_ANALYSIS));
        occurrenceRepository.save(occurrence);

        return occurrence;
    }

    /**
     * Finishes the review process of an occurrence.
     * <p>
     * Only the user that started the process can finish it
     *
     * @param user                 The user that is reviewing the occurrence
     * @param occurrenceId         The occurrence to close the review process
     * @param moderatorComments    Observations about the review
     * @param occurrenceStatusEnum The status of the review
     * @return The occurrence
     */
    public Occurrence finishReview(User user, Long occurrenceId, String moderatorComments,
                                   OccurrenceStatusEnum occurrenceStatusEnum
    ) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        if (!occurrence.isInReviewByUser(user)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        boolean hasDocument = occurrence.getOccurrenceDocument() != null &&
                occurrence.getOccurrenceDocument().getId() != null;

        boolean isApproval = occurrenceStatusEnum == OccurrenceStatusEnum.APPROVED;

        occurrence.setModeratorComments(moderatorComments);
        occurrence.setOccurrenceStatus(occurrenceStatusRepository.findByDescription(occurrenceStatusEnum));
        // Approved occurrences that have a document become verified
        occurrence.setVerified(hasDocument && isApproval);

        occurrenceRepository.save(occurrence);

        return occurrence;
    }

    /**
     * Revokes the approval of an occurrence.
     * <p>
     * Can only be performed by an ADMIN
     *
     * @param user         An ADMIN user
     * @param occurrenceId The id of the occurrence to be revoked
     * @return The occurrence
     */
    public Occurrence revokeApproval(User user, Long occurrenceId) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        if (!occurrence.canRevokeApproval(user)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        occurrence.setOccurrenceStatus(occurrenceStatusRepository
                .findByDescription(OccurrenceStatusEnum.REJECTED_BY_ADMIN));

        occurrenceRepository.save(occurrence);

        return occurrence;
    }

    public Occurrence deactivate(User user, Long occurrenceId) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        if (!occurrence.canDeleteOccurrence(user)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        occurrence.setActive(false);

        occurrenceRepository.save(occurrence);

        return occurrence;
    }

    public boolean canEditOccurrence(User user, Occurrence occurrence) {

        Occurrence persistedOccurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrence.getId())
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        boolean isEditable = persistedOccurrence.getOccurrenceStatus()
                .getDescription() == OccurrenceStatusEnum.WAITING_ANALYSIS;

        return user.getId().equals(persistedOccurrence.getUser().getId()) && isEditable;
    }

    public boolean canViewOccurrence(User user, Occurrence occurrence) {
        boolean isModerator = user.hasRole(RoleEnum.ROLE_MODERATOR);
        boolean isOwner = user.getId().equals(occurrence.getUser().getId());

        return isModerator || isOwner;
    }

    public PaginatedEntity<Occurrence> fullSearch(User user, OccurrenceSearchForm occurrenceSearchForm,
                                                  int pageLength
    ) {
        return occurrenceDao.fullSearch(user, occurrenceSearchForm, pageLength);
    }

    public PaginatedEntity<Occurrence> mapFullSearch(OccurrenceMapSearchForm occurrenceMapSearchForm) {
        return occurrenceDao.mapFullSearch(occurrenceMapSearchForm);
    }

    /**
     * Make sure the user editing is the current user and that the id of the address belongs to this occurrence.
     *
     * @param activeUser The currently logged in user
     * @param occurrence The occurrence with edit parameter
     * @param address    The address of the persisted occurrence
     */
    public void checkOccurrenceOwnership(User activeUser, Occurrence occurrence, Address address) {
        boolean isOwnerOfOccurrence = activeUser.getId().equals(occurrence.getUser().getId());
        boolean isOwnerOfAddress = address.getId().equals(occurrence.getAddress().getId());

        if (!isOwnerOfOccurrence || !isOwnerOfAddress) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }
    }

    private Occurrence create(User user, Occurrence occurrence) {

        if (!waitedSinceLastOccurrenceByTypeAndUser(occurrence.getOccurrenceType(), user)) {
            throw new IllegalArgumentException("An occurrence with this type was recently created");
        }

        OccurrenceStatus status = occurrenceStatusRepository
                .findByDescription(OccurrenceStatusEnum.WAITING_ANALYSIS);

        occurrence.setOccurrenceStatus(status);
        occurrence.setVerified(false);
        occurrence.setUser(user);

        return occurrenceRepository.save(occurrence);
    }

    public void setDocument(Occurrence occurrence, MultipartFile file)
            throws IOException {

        File occurrenceDocument = occurrence.getOccurrenceDocument();

        if (occurrenceDocument == null) {
            occurrenceDocument = new File();
        }

        if (file == null || file.isEmpty()) {
            return;
        }

        occurrenceDocument.setFileType(FileTypeEnum.OCCURRENCE_DOCUMENT);
        occurrenceDocument.setContentType(file.getContentType());
        occurrenceDocument.setContentFromMultipartFile(file);
        occurrence.setOccurrenceDocument(occurrenceDocument);

    }

    public File getDocument(User user, Long occurrenceId, Long documentId) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        boolean isModerator = user.isModerator();
        boolean isOwner = occurrence.getUser().getId().equals(user.getId());
        boolean hasFile = occurrence.getOccurrenceDocument() != null &&
                occurrence.getOccurrenceDocument().getId().equals(documentId);

        if (!hasFile) {
            throw new NotFoundException(messages.get("error.notFound"));
        }

        if (!isModerator && !isOwner) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        return occurrence.getOccurrenceDocument();
    }

    @Transactional(rollbackFor = {Exception.class})
    public void removeDocument(User user, Long occurrenceId, Long documentId) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        boolean hasFile = occurrence.getOccurrenceDocument() != null &&
                occurrence.getOccurrenceDocument().getId().equals(documentId);

        boolean isEditable = occurrence.getOccurrenceStatus().getDescription() == OccurrenceStatusEnum.WAITING_ANALYSIS;

        boolean isOwner = occurrence.getUser().getId().equals(user.getId());

        if (!hasFile) {
            throw new NotFoundException(messages.get("error.notFound"));
        }

        if (!isOwner || !isEditable) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        File file = occurrence.getOccurrenceDocument();
        occurrence.setOccurrenceDocument(null);
        occurrenceRepository.save(occurrence);
        fileRepository.delete(file);
    }

    private Occurrence edit(User user, Occurrence occurrence) {

        Occurrence persistedOccurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrence.getId())
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        occurrence.setUser(persistedOccurrence.getUser());
        this.checkOccurrenceOwnership(user, occurrence, persistedOccurrence.getAddress());

        persistedOccurrence.setAddress(occurrence.getAddress());
        persistedOccurrence.setTitle(occurrence.getTitle());
        persistedOccurrence.setDescription(occurrence.getDescription());
        persistedOccurrence.setOccurrenceType(occurrence.getOccurrenceType());
        persistedOccurrence.setOccurrenceDate(occurrence.getOccurrenceDate());

        if (occurrence.getOccurrenceDocument() != null && persistedOccurrence.getOccurrenceDocument() != null) {
            persistedOccurrence.getOccurrenceDocument().update(occurrence.getOccurrenceDocument());
        }

        if (occurrence.getOccurrenceDocument() != null && persistedOccurrence.getOccurrenceDocument() == null) {
            persistedOccurrence.setOccurrenceDocument(occurrence.getOccurrenceDocument());
        }

        return occurrenceRepository.save(persistedOccurrence);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void acceptReports(Long occurrenceId, User user) {
        Occurrence occurrence = this.getReportedOccurrence(occurrenceId, user);
        occurrence.setOccurrenceStatus(occurrenceStatusRepository
                .findByDescription(OccurrenceStatusEnum.REPORT_ACCEPTED));
        occurrenceRepository.save(occurrence);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void rejectReports(Long occurrenceId, User user) {
        Occurrence occurrence = this.getReportedOccurrence(occurrenceId, user);
        this.reportService.deactivateAllByOccurrenceId(occurrenceId);
        occurrence.setReportsCount(0L);
        occurrenceRepository.save(occurrence);
    }

    public Occurrence getReportedOccurrence(Long occurrenceId, User user) {
        Occurrence occurrence = this.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

       if (!occurrence.hasPendingReportByUser(user)) {
           throw new ForbiddenException(messages.get("error.forbidden"));
       }

        return occurrence;
    }
}
