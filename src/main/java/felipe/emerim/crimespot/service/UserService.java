package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.dao.UserDao;
import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.PaginatedEntity;
import felipe.emerim.crimespot.domain.Role;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.exception.ForbiddenException;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.form.ChangePasswordForm;
import felipe.emerim.crimespot.form.UserSearchForm;
import felipe.emerim.crimespot.helper.CacheManagerHelper;
import felipe.emerim.crimespot.repository.OccurrenceRepository;
import felipe.emerim.crimespot.repository.OccurrenceStatusRepository;
import felipe.emerim.crimespot.repository.RoleRepository;
import felipe.emerim.crimespot.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {


    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final Messages messages;
    private final PasswordEncoder passwordEncoder;
    private final CacheManagerHelper cacheManagerHelper;
    private final UserDao userDao;
    private final OccurrenceRepository occurrenceRepository;
    private final OccurrenceStatusRepository occurrenceStatusRepository;

    public void encryptPassword(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
    }

    public User save(User activeUser, User user) {

        // if new user we should encrypt the password and check if a user already exists
        if (user.getId() == null) {
            this.encryptPassword(user);
            this.checkUserExistsByNameOrCpfOrEmail(user);
            this.setRoleUser(user);
        }

        if (user.getId() != null) {
            User persistedUser = this.findById(user.getId())
                    .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));
            this.checkUpdateUserIsValid(user, persistedUser);
            this.checkUserOwnership(activeUser, user);
            user.setPassword(persistedUser.getPassword());
            user.setRoles(persistedUser.getRoles());
            user.setCreatedAt(persistedUser.getCreatedAt());
            cacheManagerHelper.evictCache("user_auth", activeUser.getUsername());
        }

        cacheManagerHelper.evictCache("user_auth", user.getUsername());
        return userRepository.save(user);
    }

    /**
     * Toggle a user on the system
     * <p>
     * An ADMIN can toggle every user but other ADMINS
     * A moderator can toggle regular users only
     * <p>
     * If we try to deactivate a moderator and that moderator
     * is analysing occurrences, those occurrences must be
     * reset to the pending analysis status
     *
     * @param activeUser The currently logged in user
     * @param userId     The id of the user to be toggled
     * @return The toggled user
     */
    @Transactional(rollbackFor = {Exception.class})
    public User toggle(User activeUser, Long userId) {
        User user = this.findById(userId).orElseThrow(
                () -> new NotFoundException(messages.get("error.notFound")));

        if (!activeUser.canToggleUser(user)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }
        user.setActive(!user.isActive());
        userRepository.save(user);

        if (user.hasRole(RoleEnum.ROLE_MODERATOR) && !user.isActive()) {
            this.resetInAnalysisOccurrencesByUser(user);
        }

        cacheManagerHelper.evictCache("user_auth", user.getUsername());

        return user;
    }

    /**
     * Add or remove moderator status from an user.
     * <p>
     * Only the admin is allowed to perform this action. Admins cannot toggle
     * moderator of other admins
     *
     * @param activeUser The currently logged in user
     * @param userId     The id of the user to toggle
     * @return The user with the changes
     */
    public User toggleModerator(User activeUser, Long userId) {
        User user = this.findById(userId).orElseThrow(
                () -> new NotFoundException(messages.get("error.notFound")));

        if (!activeUser.hasRole(RoleEnum.ROLE_ADMIN) || user.hasRole(RoleEnum.ROLE_ADMIN)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        this.toggleModeratorTransactional(user);

        cacheManagerHelper.evictCache("user_auth", user.getUsername());

        return user;
    }

    public void checkUserExistsByNameOrCpfOrEmail(User user) {
        if (user.getId() == null) {
            if (userRepository.existsByUsernameOrCpfOrEmail(user.getUsername(), user.getCpf(), user.getEmail())) {
                throw new IllegalArgumentException("User already exists");
            }
            return;
        }

        Long exists = userRepository.existsByIdNotAndUsernameOrCpfOrEmail(
                user.getId(), user.getUsername(), user.getCpf(), user.getEmail());

        if (exists != null) {
            throw new IllegalArgumentException("User already exists");
        }
    }

    public Optional<User> findById(Long userId) {
        return userRepository.findById(userId);
    }

    public void checkUserOwnership(User activeUser, User user) {
        boolean isActiveUser = activeUser.getId().equals(user.getId());
        boolean isOwnerOfAddress = activeUser.getAddress().getId().equals(user.getAddress().getId());

        if (!isActiveUser || !isOwnerOfAddress) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }
    }

    public PaginatedEntity<User> findNearestBySearchTermAndRole(User user, UserSearchForm userSearchForm) {

        if (!user.hasRole(RoleEnum.ROLE_MODERATOR) && !user.hasRole(RoleEnum.ROLE_ADMIN)) {
            throw new ForbiddenException(messages.get("error.forbidden"));
        }

        return userDao.findNearestBySearchTermAndRole(user, userSearchForm);
    }

    public User changePassword(User user, ChangePasswordForm changePasswordForm) {
        User persistedUser = this.findById(user.getId())
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        boolean passwordMatch = this.passwordEncoder.matches(changePasswordForm.getOriginalPassword(),
                persistedUser.getPassword());

        if (!passwordMatch) {
            throw new IllegalArgumentException("Invalid password supplied");
        }

        persistedUser.setPassword(this.passwordEncoder.encode(changePasswordForm.getNewPassword()));

        userRepository.save(persistedUser);
        cacheManagerHelper.evictCache("user_auth", user.getUsername());

        return persistedUser;
    }

    protected void setRoleUser(User user) {

        user.getRoles().clear();

        Role role = roleRepository.findByRole(RoleEnum.ROLE_USER)
                .orElseThrow(() -> new NotFoundException(messages.get("error.notFound")));

        user.addRole(role);

    }

    /**
     * Check if a user can be updated with the given data.
     * <p>
     * Since username, CPF and email are unique, we need to check if the new data for those
     * fields will not conflict with data from another user.
     *
     * @param user The user to be updated
     */
    protected void checkUpdateUserIsValid(User user, User persistedUser) {
        boolean changedUsername = !user.getUsername().equals(persistedUser.getUsername());
        boolean changedCpf = !user.getCpf().equals(persistedUser.getCpf());
        boolean changedEmail = !user.getEmail().equals(persistedUser.getEmail());

        if (changedUsername || changedCpf || changedEmail) {
            this.checkUserExistsByNameOrCpfOrEmail(user);
        }
    }

    /**
     * Toggle moderator privileges for a user.
     * <p>
     * If the moderator is to be deactivated and has occurrences in analysis
     * those occurrences must be reset to the pending analysis state.
     *
     * @param user The moderator to be toggled.
     */
    @Transactional(rollbackFor = {Exception.class})
    protected void toggleModeratorTransactional(User user) {
        Role roleModerator = roleRepository.findByRole(RoleEnum.ROLE_MODERATOR)
                .orElseThrow(() -> new NotFoundException("error.notFound"));

        if (user.hasRole(RoleEnum.ROLE_MODERATOR)) {
            user.removeRole(roleModerator);
            this.resetInAnalysisOccurrencesByUser(user);
            userRepository.save(user);
            return;
        }

        user.addRole(roleModerator);
        userRepository.save(user);
    }

    /**
     * Given a moderator, reset in analysis occurrences to waiting analysis.
     * <p>
     * An inactive moderator cannot be analysing occurrences, otherwise those
     * occurrences would be in a limbo state. They are being analysed by a user
     * that is inactive. But the user cannot complete the analysis because
     * he is inactive. The solution to that problem is resetting the status
     * of the occurrences that user is analysis to waiting analysis.
     *
     * @param user The user to check for occurrences
     */
    @Transactional(rollbackFor = {Exception.class})
    protected void resetInAnalysisOccurrencesByUser(User user) {
        List<Occurrence> occurrences = occurrenceRepository
                .findAllByModeratorIdAndOccurrenceStatusDescription(user.getId(), OccurrenceStatusEnum.IN_ANALYSIS);

        for (Occurrence occurrence: occurrences) {
            occurrence.setModerator(null);
            occurrence.setOccurrenceStatus(occurrenceStatusRepository
                    .findByDescription(OccurrenceStatusEnum.WAITING_ANALYSIS));
        }

        occurrenceRepository.saveAll(occurrences);
    }
}
