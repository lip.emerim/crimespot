package felipe.emerim.crimespot.service;

import felipe.emerim.crimespot.dao.ReportDao;
import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.Report;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.exception.NotFoundException;
import felipe.emerim.crimespot.exception.ReportAlreadyExistsException;
import felipe.emerim.crimespot.repository.OccurrenceRepository;
import felipe.emerim.crimespot.repository.ReportRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@AllArgsConstructor
@Service
public class ReportService {
    protected ReportDao reportDao;
    protected ReportRepository reportRepository;
    protected OccurrenceRepository occurrenceRepository;

    public List<Report> getActiveReportsByOccurrenceId(Long occurrenceId) {
        return reportDao.getActiveReportsByOccurrenceId(occurrenceId);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void deactivateAllByOccurrenceId(Long occurrenceId) {
        List<Report> reports = reportDao.getActiveReportsByOccurrenceId(occurrenceId);

        for(Report report: reports) {
            report.setActive(false);
        }

        reportRepository.saveAll(reports);
    }

    @Transactional
    public Report createReport(User user, Long occurrenceId, String description) {
        if (this.existsByOccurrenceIdAndUserIdAndActiveIsTrue(
                occurrenceId,
                user.getId()
        )) {
            throw new ReportAlreadyExistsException("");
        }

        Occurrence occurrence = occurrenceRepository.findByIdAndUserActiveIsTrueAndActiveIsTrue(occurrenceId).orElseThrow(
                () -> new NotFoundException("")
        );

        occurrence.setReportsCount(occurrence.getReportsCount() + 1);
        occurrenceRepository.save(occurrence);

        Report report = Report
                .builder()
                .user(user)
                .description(description)
                .occurrence(occurrence)
                .build();

        return reportRepository.save(report);
    }

    public boolean existsByOccurrenceIdAndUserIdAndActiveIsTrue(Long occurrenceId, Long userId) {
        return this.reportRepository.existsByOccurrenceIdAndUserIdAndActiveIsTrue(occurrenceId, userId);
    }
}
