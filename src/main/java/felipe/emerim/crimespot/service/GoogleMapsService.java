package felipe.emerim.crimespot.service;

import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import felipe.emerim.crimespot.domain.GeocodingAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Class that should wrap google maps service.
 * <p>
 * This should do all json parsing and loading logic. Our services must not deal with that complexity
 * on each maps request. We MUST use this as a component because some attributes, like the context
 * require a singleton implementation. If singleton is not respected we will start seeing memory leaks
 * and in the end the server will die.
 * <p>
 * For the reasons above we always destroy the context when this class is not necessary anymore
 */
@Service
public class GoogleMapsService implements GeocodingService {
    protected GoogleMapsWrapperService googleMapsWrapperService;

    @Value("${GOOGLE_MAPS_API_LANGUAGE:pt-BR}")
    protected String language;

    public GoogleMapsService(GoogleMapsWrapperService googleMapsWrapperService) {
        this.googleMapsWrapperService = googleMapsWrapperService;
    }

    /**
     * Given an address query, fetches the address from the google maps api.
     *
     * @param address The search query.
     * @return The address with its information.
     * @throws RuntimeException When the API cannot be reached
     */
    @Cacheable(value = "address_geocode", key = "#address")
    public GeocodingAddress geocode(String address) throws RuntimeException {

        GeocodingResult[] results;
        try {
            results = this.googleMapsWrapperService.geocode(address);
        } catch (ApiException | InterruptedException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException("There was an error fetching the address");
        }
        if (results.length == 0) {
            throw new IllegalArgumentException("Could not resolve address");
        }

        return getAddressFromRequest(results);
    }

    /**
     * Convert the first API result to an Address object we can use.
     *
     * @param results The api response results
     * @return The address object
     */
    protected GeocodingAddress getAddressFromRequest(GeocodingResult[] results) {
        // Get closest match
        GeocodingResult result = results[0];

        return GeocodingAddress
                .builder()
                .city(this.getCity(result))
                .country(this.getCountry(result))
                .state(this.getState(result))
                .latitude(result.geometry.location.lat)
                .longitude(result.geometry.location.lng)
                .formattedAddress(result.formattedAddress)
                .neighborhood(this.getNeighborhood(result))
                .number(this.getNumber(result))
                .street(this.getStreet(result))
                .postalCode(this.getPostalCode(result))
                .build();
    }

    /**
     * Get the city of the address from the result
     * <p>
     * The city is a special case, sometimes it is in the locality component, other times in the
     * administrative area level 2 component. We first search the locality, in case that is null
     * we try the admin level 2.
     * <p>
     *
     * @param result A result of the API request.
     * @return The city component
     */
    protected String getCity(GeocodingResult result) {
        try {
            // In some places city is locality, in other it is an admin level
            return this.getAddressComponent(result, AddressComponentType.LOCALITY, true);
        } catch (IllegalArgumentException e) {
            // IF no locality then try to search for admin level
            // It is totally ok for this to happen here
        }

        return this.getAddressComponent(result, AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2, false);
    }

    protected String getCountry(GeocodingResult result) {
        return this.getAddressComponent(result, AddressComponentType.COUNTRY, false);
    }

    protected String getState(GeocodingResult result) {
        return this.getAddressComponent(result, AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1, false);
    }

    protected String getPostalCode(GeocodingResult result) {
        return this.getAddressComponent(result, AddressComponentType.POSTAL_CODE, false);
    }

    protected String getNeighborhood(GeocodingResult result) {
        return Optional.ofNullable(this.getAddressComponent(result, AddressComponentType.NEIGHBORHOOD, false))
                .orElse(this.getAddressComponent(result, AddressComponentType.SUBLOCALITY_LEVEL_1, false));
    }

    protected String getStreet(GeocodingResult result) {
        return this.getAddressComponent(result, AddressComponentType.ROUTE, false);
    }

    protected String getNumber(GeocodingResult result) {
        return this.getAddressComponent(result, AddressComponentType.STREET_NUMBER, false);
    }

    /**
     * Return an address component from a google maps API result
     *
     * @param result   a google maps API result
     * @param type     The type of the component
     * @param required Whether the component is required
     * @return The component data
     * @throws IllegalArgumentException If the component is required but cannot be found.
     */
    protected String getAddressComponent(GeocodingResult result, AddressComponentType type, Boolean required) {
        for (AddressComponent addressComponent : result.addressComponents) {
            if (Arrays.stream(addressComponent.types).anyMatch(t -> t == type)) {
                return addressComponent.longName;
            }
        }

        if (!required) {
            return null;
        }

        throw new IllegalArgumentException("Could not resolve address component: " + type.toString());
    }

}
