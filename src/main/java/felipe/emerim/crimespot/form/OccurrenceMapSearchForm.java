package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
public class OccurrenceMapSearchForm {
    private String searchTerm = "";
    private List<OccurrenceTypeEnum> occurrenceTypeEnums;
    private Double searchRadius;
    private Double latitude;
    private Double longitude;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;
    private Boolean onlyVerified = Boolean.FALSE;

}
