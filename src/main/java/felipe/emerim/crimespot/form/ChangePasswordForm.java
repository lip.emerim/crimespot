package felipe.emerim.crimespot.form;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ChangePasswordForm {
    @NotBlank(message = "{field.required}")
    private String originalPassword;
    @Size(min = 5, max = 500, message = "{user.password.length}")
    @NotBlank(message = "{field.required}")
    private String newPassword;
    private String newPasswordConfirm;

    private boolean passwordConfirmMatch;

    @AssertTrue(message = "{user.passwordConfirmMismatch}")
    public boolean isPasswordConfirmMatch() {

        if (this.newPassword == null) {
            return true;
        }

        return this.newPassword.equals(this.newPasswordConfirm);
    }

}
