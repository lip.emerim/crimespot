package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.validator.OccurrenceDocumentValidator;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class CreateEditOccurrenceForm {
    @Valid
    @NotNull
    private Occurrence occurrence;

    @Valid
    @NotNull
    private Address address;

    @OccurrenceDocumentValidator(message = "{field.invalidFile}")
    private MultipartFile occurrenceDocument;

}
