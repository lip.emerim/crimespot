package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.Occurrence;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportCreateForm {
    @NotBlank(message = "{field.required}")
    @Size(min = 15, max = 1000, message = "{report.description.length}")
    protected String description;
}
