package felipe.emerim.crimespot.form;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class OccurrenceReviewForm {

    @Size(min = 15, max = 1000, message = "{occurrence.moderatorComments.length}")
    private String moderatorComments;
}
