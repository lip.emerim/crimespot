package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.enums.RoleEnum;
import lombok.Data;

@Data
public class UserSearchForm {
    private int page = 1;
    private int pageLength = 8;
    private String searchTerm = "";
    private RoleEnum role;
    private Boolean active;
}
