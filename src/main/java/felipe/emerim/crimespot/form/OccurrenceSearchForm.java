package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.enums.OccurrenceSortEnum;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import lombok.Data;

import java.util.List;

@Data
public class OccurrenceSearchForm {
    private int page = 1;
    private String searchTerm = "";
    private OccurrenceStatusEnum occurrenceStatusEnum;
    private List<OccurrenceTypeEnum> occurrenceTypeEnums;
    private OccurrenceSortEnum occurrenceSortEnum = OccurrenceSortEnum.MOST_RECENT;
    private Boolean onlyRelatedToMe = false;
}
