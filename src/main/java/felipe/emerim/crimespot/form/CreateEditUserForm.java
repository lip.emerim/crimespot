package felipe.emerim.crimespot.form;

import felipe.emerim.crimespot.domain.Address;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.validation.group.CreateUserGroup;
import felipe.emerim.crimespot.validation.group.EditUserGroup;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateEditUserForm {
    @Valid
    @NotNull(groups = {CreateUserGroup.class, EditUserGroup.class})
    private User user;

    @Valid
    @NotNull(groups = {CreateUserGroup.class, EditUserGroup.class})
    private Address address;

    @NotBlank(message = "{field.required}", groups = {CreateUserGroup.class})
    private String passwordConfirm;

    private boolean passwordConfirmMatch;

    @AssertTrue(message = "{user.passwordConfirmMismatch}", groups = {CreateUserGroup.class})
    public boolean isPasswordConfirmMatch() {

        if (this.user.getPassword() == null) {
            return true;
        }

        return this.user.getPassword().equals(this.passwordConfirm);
    }
}
