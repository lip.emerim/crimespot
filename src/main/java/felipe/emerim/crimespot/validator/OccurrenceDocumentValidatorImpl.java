package felipe.emerim.crimespot.validator;

import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

/**
 * Occurrence document file validator.
 */
public class OccurrenceDocumentValidatorImpl implements ConstraintValidator<OccurrenceDocumentValidator, MultipartFile> {
    private static final long MAX_SIZE = 5 * 1024 * 1024;
    private static final Set<MediaType> ALLOWED_MEDIA_TYPES = Set.of(
            MediaType.APPLICATION_PDF,
            MediaType.IMAGE_JPEG,
            MediaType.IMAGE_PNG
    );

    @Override
    public void initialize(OccurrenceDocumentValidator constraintAnnotation) {

    }

    /**
     * Validator implementation
     * <p>
     * A file is only considered valid if it is a PDF, PNG or JPEG, has a fileName
     * and does not exceed 5MB.
     * <p>
     * If no file is sent, the validation passes since the file is not required.
     */
    @Override
    public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
        //File is not required
        if (value == null || value.isEmpty()) {
            return true;
        }

        // does not have a media type
        if (value.getContentType() == null) {
            return false;
        }

        MediaType mediaType;

        try {
            mediaType = MediaType.parseMediaType(value.getContentType());
        } catch (InvalidMediaTypeException e) {
            return false;
        }

        // Invalid mime
        if (!ALLOWED_MEDIA_TYPES.contains(mediaType)) {
            return false;
        }

        // Invalid fileName
        if (value.getOriginalFilename() == null || value.getOriginalFilename().isBlank()) {
            return false;
        }

        // max size exceeded
        return value.getSize() <= MAX_SIZE;
    }
}
