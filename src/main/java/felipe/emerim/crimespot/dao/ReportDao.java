package felipe.emerim.crimespot.dao;

import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.Report;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import lombok.AllArgsConstructor;
import org.hibernate.search.engine.search.predicate.SearchPredicate;
import org.hibernate.search.engine.search.predicate.dsl.BooleanPredicateClausesStep;
import org.hibernate.search.engine.search.query.SearchQuery;
import org.hibernate.search.engine.search.sort.SearchSort;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.scope.SearchScope;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Component
@AllArgsConstructor
public class ReportDao {
    @PersistenceUnit
    private final EntityManagerFactory emf;

    public List<Report> getActiveReportsByOccurrenceId(Long occurrenceId) {
        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession session = Search.session(em);
            SearchQuery<Report> searchQuery = this.buildReportsQuery(session, occurrenceId);

            return searchQuery.fetchAllHits();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public SearchQuery<Report> buildReportsQuery(
            SearchSession session,
            Long occurrenceId
    ) {
        SearchPredicate basePredicate = this.getReportsActivePredicate(session);
        SearchScope<Report> scope = session.scope(Report.class);

        SearchSort sort = scope.sort().field("createdAt").desc().toSort();

        return session.search(Report.class).where(f -> f.bool(b -> {
                    b.must(basePredicate);
                    b.must(scope.predicate().match().field("occurrence.id").matching(occurrenceId));
                })
        ).sort(sort).toQuery();
    }

    public Long countReportedOccurrences() {
        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession session = Search.session(em);
            SearchScope<Occurrence> scope = session.scope(Occurrence.class);

            BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();
            booleanJunction.must(this.getReportedOccurrencesPredicate(session));

            return session.search(Occurrence.class).where(f -> f.bool(b -> b.must(booleanJunction.toPredicate()))
            ).fetchTotalHitCount();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public SearchPredicate getReportedOccurrencesPredicate(SearchSession session) {
        SearchScope<Occurrence> scope = session.scope(Occurrence.class);
        BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();

        booleanJunction.must(
                scope.predicate().match().field("user.active").matching(true)
        );

        booleanJunction.must(
                scope.predicate().match().field("active").matching(true)
        );

        booleanJunction.must(
                scope.predicate().range().field("reportsCount").greaterThan(0L)
        );

        booleanJunction.must(
                scope.predicate()
                        .match()
                        .fields("occurrenceStatus.description")
                        .matching(OccurrenceStatusEnum.APPROVED)
        );

        return booleanJunction.toPredicate();
    }

    public SearchPredicate getReportsActivePredicate(SearchSession session) {
        SearchScope<Report> scope = session.scope(Report.class);
        BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();

        booleanJunction.must(
                scope.predicate().match().field("active").matching(true)
        );

        booleanJunction.must(
                scope.predicate().match().field("user.active").matching(true)
        );

        booleanJunction.must(
                scope.predicate().match().field("occurrence.active").matching(true)
        );

        return booleanJunction.toPredicate();
    }
}
