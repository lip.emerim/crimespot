package felipe.emerim.crimespot.dao;

import felipe.emerim.crimespot.domain.PaginatedEntity;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.form.UserSearchForm;
import lombok.AllArgsConstructor;
import org.hibernate.search.engine.search.common.BooleanOperator;
import org.hibernate.search.engine.search.predicate.SearchPredicate;
import org.hibernate.search.engine.search.predicate.dsl.BooleanPredicateClausesStep;
import org.hibernate.search.engine.search.query.SearchQuery;
import org.hibernate.search.engine.spatial.GeoPoint;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.scope.SearchScope;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Component
@AllArgsConstructor
public class UserDao {
    private static final int MAX_PAGE_LENGTH = 15;
    @PersistenceUnit
    private final EntityManagerFactory emf;

    public PaginatedEntity<User> findNearestBySearchTermAndRole(User user, UserSearchForm userSearchForm) {

        if (userSearchForm.getPageLength() > MAX_PAGE_LENGTH) {
            userSearchForm.setPageLength(MAX_PAGE_LENGTH);
        }

        int start = (userSearchForm.getPage() - 1) * userSearchForm.getPageLength();

        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession searchSession = Search.session(em);
            SearchQuery<User> searchQuery = this.buildSearchQuery(searchSession, user, userSearchForm);
            List<User> result = searchQuery.fetchHits(start, userSearchForm.getPageLength());

            PaginatedEntity<User> paginatedEntity = PaginatedEntity.<User>builder()
                    .data(result)
                    .currentPage(userSearchForm.getPage())
                    .totalResults(searchQuery.fetchTotalHitCount())
                    .pageLength(userSearchForm.getPageLength())
                    .build();

            em.close();

            return paginatedEntity;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public SearchQuery<User> buildSearchQuery(
            SearchSession session,
            User user,
            UserSearchForm userSearchForm
    ) {
        SearchPredicate basePredicate = this.getBaseFilters(userSearchForm, session);

        return session.search(User.class).where(f -> f.bool(b -> {
                    b.must(basePredicate);
                })
        ).sort(f -> f.distance("address.location",
                GeoPoint.of(user.getAddress().getLatitude(), user.getAddress().getLongitude())
                ).then().field("username_sort")
        ).toQuery();
    }

    private SearchPredicate getBaseFilters(UserSearchForm userSearchForm, SearchSession session) {
        SearchScope<User> scope = session.scope(User.class);
        BooleanPredicateClausesStep<?> bool = scope.predicate().bool();

        bool.should(scope.predicate().matchAll());
        if (userSearchForm.getSearchTerm() != null && userSearchForm.getSearchTerm().length() >= 3) {
            bool.must(scope.predicate().simpleQueryString()
                    .fields("username_data", "name", "email", "cpf")
                    .matching(userSearchForm.getSearchTerm())
                    .defaultOperator(BooleanOperator.OR)
            );
        }


        if (userSearchForm.getRole() != null) {
            bool.must(scope.predicate().match()
                    .field("mainRole")
                    .matching(userSearchForm.getRole())
            );
        }

        if (userSearchForm.getActive() != null) {
            bool.must(scope.predicate()
                    .match()
                    .field("active")
                    .matching(userSearchForm.getActive())
            );
        }

        return bool.toPredicate();
    }
}
