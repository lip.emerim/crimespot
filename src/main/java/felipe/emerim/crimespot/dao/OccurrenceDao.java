package felipe.emerim.crimespot.dao;

import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.PaginatedEntity;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.OccurrenceSortEnum;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.OccurrenceTypeEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import felipe.emerim.crimespot.form.OccurrenceMapSearchForm;
import felipe.emerim.crimespot.form.OccurrenceSearchForm;
import lombok.AllArgsConstructor;
import org.hibernate.search.engine.search.common.BooleanOperator;
import org.hibernate.search.engine.search.predicate.SearchPredicate;
import org.hibernate.search.engine.search.predicate.dsl.BooleanPredicateClausesStep;
import org.hibernate.search.engine.search.query.SearchQuery;
import org.hibernate.search.engine.search.sort.SearchSort;
import org.hibernate.search.engine.spatial.DistanceUnit;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.scope.SearchScope;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class OccurrenceDao {
    private static final int MAX_PAGE_LENGTH = 10;
    private static final int MAP_MAX_RESULTS = 200;
    @PersistenceUnit
    private final EntityManagerFactory emf;

    public PaginatedEntity<Occurrence> fullSearch(User user, OccurrenceSearchForm occurrenceSearchForm,
                                                  int pageLength
    ) {
        if (occurrenceSearchForm.getOccurrenceSortEnum() == null) {
            occurrenceSearchForm.setOccurrenceSortEnum(OccurrenceSortEnum.MOST_RECENT);
        }

        if (pageLength > MAX_PAGE_LENGTH) {
            pageLength = MAX_PAGE_LENGTH;
        }

        if (user.getMainRole() == RoleEnum.ROLE_USER) {
            occurrenceSearchForm.setOnlyRelatedToMe(false);
        }

        int start = (occurrenceSearchForm.getPage() - 1) * pageLength;

        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession session = Search.session(em);
            SearchQuery<Occurrence> searchQuery = this.buildSearchQuery(session, user, occurrenceSearchForm);
            List<Occurrence> result = searchQuery.fetchHits(start, pageLength);

            PaginatedEntity<Occurrence> paginatedEntity = PaginatedEntity.<Occurrence>builder()
                    .data(result)
                    .currentPage(occurrenceSearchForm.getPage())
                    .totalResults(searchQuery.fetchTotalHitCount())
                    .pageLength(pageLength)
                    .build();

            em.close();

            return paginatedEntity;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public PaginatedEntity<Occurrence> mapFullSearch(OccurrenceMapSearchForm occurrenceMapSearchForm) {

        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession session = Search.session(em);
            SearchQuery<Occurrence> searchQuery = this.buildMapSearchQuery(
                    session,
                    occurrenceMapSearchForm
            );
            List<Occurrence> result = searchQuery.fetchHits(MAP_MAX_RESULTS);

            // user that created occurrence must not appear on map search
            for (Occurrence occurrence : result) {
                occurrence.setUser(null);
                occurrence.setModerator(null);
            }

            PaginatedEntity<Occurrence> paginatedEntity = PaginatedEntity.<Occurrence>builder()
                    .data(result)
                    .currentPage(1)
                    .totalResults(searchQuery.fetchTotalHitCount())
                    .pageLength(MAP_MAX_RESULTS)
                    .build();

            em.close();

            return paginatedEntity;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public SearchQuery<Occurrence> buildSearchQuery(
            SearchSession session,
            User user,
            OccurrenceSearchForm occurrenceSearchForm
    ) {
        SearchPredicate basePredicate = this.buildBasePredicates(user, occurrenceSearchForm, session);

        SearchSort sort = this.buildSort(session, occurrenceSearchForm.getOccurrenceSortEnum(), user);

        return session.search(Occurrence.class).where(f -> f.bool(b -> {
                    b.must(basePredicate);
                })
        ).sort(sort).toQuery();
    }

    public SearchQuery<Occurrence> buildMapSearchQuery(
            SearchSession session,
            OccurrenceMapSearchForm occurrenceMapSearchForm
    ) {
        SearchPredicate basePredicate = this.buildBaseMapPredicates(occurrenceMapSearchForm, session);

        SearchSort sort = session.scope(Occurrence.class).sort()
                .field("occurrenceDate").desc().then()
                .distance("address.location", occurrenceMapSearchForm.getLatitude(),
                        occurrenceMapSearchForm.getLongitude()).asc().toSort();

        return session.search(Occurrence.class).where(f -> f.bool(b -> {
                    b.must(basePredicate);
                })
        ).sort(sort).toQuery();
    }

    public Long countByStatus(OccurrenceStatusEnum occurrenceStatusEnum) {

        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession session = Search.session(em);
            SearchScope<Occurrence> scope = session.scope(Occurrence.class);

            BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();
            booleanJunction.must(this.getActiveOccurrencesPredicate(session));

            booleanJunction.must(scope.predicate()
                    .match()
                    .field("occurrenceStatus.description")
                    .matching(occurrenceStatusEnum)
            );

            return session.search(Occurrence.class).where(f -> f.bool(b -> {
                        b.must(f.matchAll());
                        b.must(booleanJunction);
                    })
            ).fetchTotalHitCount();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Long countInAnalysisByUser(User user) {
        EntityManager em = null;

        try {
            em = emf.createEntityManager();
            SearchSession session = Search.session(em);
            SearchScope<Occurrence> scope = session.scope(Occurrence.class);

            BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();
            booleanJunction.must(this.getActiveOccurrencesPredicate(session));

            booleanJunction.must(
                    scope.predicate()
                            .match()
                            .fields("occurrenceStatus.description")
                            .matching(OccurrenceStatusEnum.IN_ANALYSIS)
            );

            booleanJunction.must(
                    scope.predicate()
                            .match()
                            .field("moderator.id")
                            .matching(user.getId())
            );

            return session.search(Occurrence.class).where(f -> f.bool(b -> {
                        b.must(f.matchAll());
                        b.must(booleanJunction.toPredicate());
                    })
            ).fetchTotalHitCount();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private SearchSort buildSort(SearchSession session, OccurrenceSortEnum occurrenceSortEnum, User user) {
        SearchScope<Occurrence> scope = session.scope(Occurrence.class);

        switch (occurrenceSortEnum) {
            case MOST_RECENT:
            default:
                return scope.sort().field("createdAt").desc().toSort();
            case NEAREST:
                return scope.sort().distance("address.location",
                        user.getAddress().getLatitude(), user.getAddress().getLongitude()).toSort();
            case LEAST_RECENT:
                return scope.sort().field("createdAt").asc().toSort();
            case MOST_REPORTED:
                return scope.sort().field("reportsCount").desc().toSort();
        }
    }

    private SearchPredicate buildBasePredicates(
            User user,
            OccurrenceSearchForm occurrenceSearchForm,
            SearchSession session
    ) {
        SearchScope<Occurrence> scope = session.scope(Occurrence.class);
        BooleanPredicateClausesStep<?> bool = session.scope(Occurrence.class).predicate().bool();

        bool.must(scope.predicate().matchAll());
        bool.must(this.getActiveOccurrencesPredicate(session));

        if (occurrenceSearchForm.getSearchTerm() != null && occurrenceSearchForm.getSearchTerm().length() >= 3) {
            bool.must(scope.predicate()
                    .simpleQueryString()
                    .fields("user.username_data", "user.name", "user.email", "user.cpf", "title",
                            "moderator.username_data", "moderator.name", "moderator.cpf",
                            "address.country", "address.state", "address.city", "address.street",
                            "address.postalCode")
                    .matching(occurrenceSearchForm.getSearchTerm())
                    .defaultOperator(BooleanOperator.OR));
        }

        if (occurrenceSearchForm.getOccurrenceStatusEnum() != null) {
            bool.must(scope.predicate()
                    .match()
                    .fields("occurrenceStatus.description")
                    .matching(occurrenceSearchForm.getOccurrenceStatusEnum()));
        }


        Optional<SearchPredicate> predicate = this.getOccurrenceTypesPredicate(
                occurrenceSearchForm.getOccurrenceTypeEnums(), session);

        predicate.ifPresent(bool::must);


        if (occurrenceSearchForm.getOnlyRelatedToMe()) {
            bool.must(scope.predicate()
                    .match()
                    .fields("user.id", "moderator.id")
                    .matching(user.getId()));
        }

        if (user.getMainRole() == RoleEnum.ROLE_USER) {
            bool.must(scope.predicate()
                    .match()
                    .fields("user.id")
                    .matching(user.getId()));
        }

        return bool.toPredicate();
    }

    private SearchPredicate buildBaseMapPredicates(
            OccurrenceMapSearchForm occurrenceMapSearchForm,
            SearchSession session
    ) {
        SearchScope<Occurrence> scope = session.scope(Occurrence.class);

        BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();

        booleanJunction.must(scope.predicate().matchAll());
        booleanJunction.must(this.getActiveOccurrencesPredicate(session));

        if (occurrenceMapSearchForm.getSearchTerm() != null && occurrenceMapSearchForm.getSearchTerm().length() >= 3) {
            booleanJunction.must(scope.predicate()
                    .simpleQueryString()
                    .fields("title",
                            "moderator.username_data", "moderator.name", "moderator.cpf",
                            "address.country", "address.state", "address.city", "address.street",
                            "address.postalCode")
                    .matching(occurrenceMapSearchForm.getSearchTerm())
                    .defaultOperator(BooleanOperator.OR));
        }

        booleanJunction.must(scope.predicate()
                .match()
                .fields("occurrenceStatus.description")
                .matching(OccurrenceStatusEnum.APPROVED));


        Optional<SearchPredicate> predicate = this.getOccurrenceTypesPredicate(
                occurrenceMapSearchForm.getOccurrenceTypeEnums(), session
        );
        predicate.ifPresent(booleanJunction::must);

        booleanJunction.must(scope.predicate()
                .spatial()
                .within()
                .field("address.location")
                .circle(
                        occurrenceMapSearchForm.getLatitude(),
                        occurrenceMapSearchForm.getLongitude(),
                        occurrenceMapSearchForm.getSearchRadius(),
                        DistanceUnit.KILOMETERS
                )
        );

        if (occurrenceMapSearchForm.getStartDate() != null && occurrenceMapSearchForm.getEndDate() != null) {
            booleanJunction.must(scope.predicate()
                    .range()
                    .field("occurrenceDate")
                    .between(
                            occurrenceMapSearchForm.getStartDate(),
                            occurrenceMapSearchForm.getEndDate()
                    )
            );
        }

        if (occurrenceMapSearchForm.getOnlyVerified()) {
            booleanJunction.must(
                    scope.predicate().match().field("isVerified").matching(true)
            );
        }

        return booleanJunction.toPredicate();
    }

    private Optional<SearchPredicate> getOccurrenceTypesPredicate(
            List<OccurrenceTypeEnum> occurrenceTypes,
            SearchSession session
    ) {

        SearchScope<Occurrence> scope = session.scope(Occurrence.class);

        if (occurrenceTypes == null || occurrenceTypes.isEmpty()) {
            return Optional.empty();
        }

        BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();

        for (OccurrenceTypeEnum occurrenceType : occurrenceTypes) {
            booleanJunction.should(scope.predicate()
                    .match()
                    .fields("occurrenceType.description")
                    .matching(occurrenceType));
        }

        return Optional.of(booleanJunction.toPredicate());
    }

    public SearchPredicate getActiveOccurrencesPredicate(SearchSession session) {
        SearchScope<Occurrence> scope = session.scope(Occurrence.class);
        BooleanPredicateClausesStep<?> booleanJunction = scope.predicate().bool();

        booleanJunction.must(
                scope.predicate().match().field("user.active").matching(true)
        );

        booleanJunction.must(
                scope.predicate().match().field("active").matching(true)
        );

        return booleanJunction.toPredicate();
    }
}
