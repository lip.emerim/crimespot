package felipe.emerim.crimespot.components;

import felipe.emerim.crimespot.config.Messages;
import felipe.emerim.crimespot.domain.Alert;
import felipe.emerim.crimespot.domain.AlertTypes;
import lombok.AllArgsConstructor;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@AllArgsConstructor
public class FileUploadExceptionAdvice extends ResponseEntityExceptionHandler {

    protected Messages messages;

    @ExceptionHandler({ MaxUploadSizeExceededException.class, FileSizeLimitExceededException.class })
    public String handler(RedirectAttributes redirectAttributes, HttpServletRequest request) {
        List<Alert> alerts = new ArrayList<>();

        alerts.add(Alert.builder()
                .type(AlertTypes.DANGER)
                .message(messages.get("upload.maxSize"))
                .build());

        redirectAttributes.addFlashAttribute("messages", alerts);
        return "redirect:" + request.getRequestURI();
    }
}
