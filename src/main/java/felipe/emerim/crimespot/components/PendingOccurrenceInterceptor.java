package felipe.emerim.crimespot.components;

import felipe.emerim.crimespot.config.auth.UserImpl;
import felipe.emerim.crimespot.dao.OccurrenceDao;
import felipe.emerim.crimespot.dao.ReportDao;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import felipe.emerim.crimespot.enums.RoleEnum;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * Intercept every request to add pending and in analysis occurrence counters.
 * <p>
 * In case the user is not logged in, or the user does not have the moderator role,
 * this does nothing.
 */
@Component
@AllArgsConstructor
public class PendingOccurrenceInterceptor implements HandlerInterceptor {

    private final OccurrenceDao occurrenceDao;
    private final ReportDao reportDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView model) {

        if (model == null || model.getView() instanceof RedirectView ||
                Objects.requireNonNull(model.getViewName()).startsWith("redirect:")) {
            return;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return;
        }

        UserImpl activeUser = (UserImpl) authentication.getPrincipal();

        if (activeUser == null || !activeUser.getUser().hasRole(RoleEnum.ROLE_MODERATOR)) {
            model.addObject("pendingOccurrencesCounter", 0L);
            model.addObject("inAnalysisOccurrencesCounter", 0L);
            model.addObject("reportedOccurrencesCounter", 0L);
            return;
        }

        Long pendingOccurrencesCounter = occurrenceDao
                .countByStatus(OccurrenceStatusEnum.WAITING_ANALYSIS);

        Long inAnalysisOccurrencesCounter = occurrenceDao
                .countInAnalysisByUser(activeUser.getUser());

        Long reportedOccurrenceCounter = reportDao.countReportedOccurrences();

        model.addObject("pendingOccurrencesCounter", pendingOccurrencesCounter);
        model.addObject("inAnalysisOccurrencesCounter", inAnalysisOccurrencesCounter);
        model.addObject("reportedOccurrencesCounter", reportedOccurrenceCounter);
    }
}
