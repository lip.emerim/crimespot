package felipe.emerim.crimespot.repository;

import felipe.emerim.crimespot.domain.Occurrence;
import felipe.emerim.crimespot.domain.OccurrenceType;
import felipe.emerim.crimespot.domain.User;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OccurrenceRepository extends JpaRepository<Occurrence, Long> {

    Occurrence findByOccurrenceTypeAndUserOrderByCreatedAtDesc(OccurrenceType type, User user);

    Optional<Occurrence> findByIdAndUserActiveIsTrueAndActiveIsTrue(Long id);

    List<Occurrence> findAllByModeratorIdAndOccurrenceStatusDescription(Long id, OccurrenceStatusEnum status);

}
