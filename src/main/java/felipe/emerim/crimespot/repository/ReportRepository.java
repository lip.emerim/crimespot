package felipe.emerim.crimespot.repository;

import felipe.emerim.crimespot.domain.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findAllByOccurrenceIdAndActiveIsTrue(Long occurrenceId);

    boolean existsByOccurrenceIdAndUserIdAndActiveIsTrue(Long occurrenceId, Long userId);
}
