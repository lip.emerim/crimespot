package felipe.emerim.crimespot.repository;

import felipe.emerim.crimespot.domain.OccurrenceStatus;
import felipe.emerim.crimespot.enums.OccurrenceStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OccurrenceStatusRepository extends JpaRepository<OccurrenceStatus, Long> {
    OccurrenceStatus findByDescription(OccurrenceStatusEnum description);
}
