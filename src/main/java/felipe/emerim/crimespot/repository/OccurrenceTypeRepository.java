package felipe.emerim.crimespot.repository;

import felipe.emerim.crimespot.domain.OccurrenceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OccurrenceTypeRepository extends JpaRepository<OccurrenceType, Long> {
}
