package felipe.emerim.crimespot.repository;

import felipe.emerim.crimespot.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameAndActiveIsTrue(String username);

    boolean existsByUsernameOrCpfOrEmail(String username, String cpf, String email);

    @Query("SELECT max(1) FROM User u WHERE u.id <> ?1 AND (u.username = ?2 or u.cpf = ?3 or u.email = ?4)")
    Long existsByIdNotAndUsernameOrCpfOrEmail(Long id, String username, String cpf, String email);
}
