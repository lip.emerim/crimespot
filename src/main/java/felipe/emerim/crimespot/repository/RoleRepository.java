package felipe.emerim.crimespot.repository;

/*
  Created by Níkolas Daroit on 14/04/2019
  purpose: implements part of the business rules to Role, permission related
  methods: findByName
 */

import felipe.emerim.crimespot.domain.Role;
import felipe.emerim.crimespot.enums.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRole(RoleEnum role);
}